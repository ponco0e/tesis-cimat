#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 16 14:56:04 2019

@author: ponco
"""

from __future__ import print_function

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import pickle

import seaborn as sns
sns.set()
#sns.set(style="ticks", color_codes=True)

#%%

class IndexTracker(object):
    def __init__(self, ax, X):
        self.ax = ax
        ax.set_title('use scroll wheel to navigate images')

        self.X = X
        rows, cols, self.slices = X.shape
        self.ind = self.slices//2

        self.im = ax.imshow(self.X[:, :, self.ind])
        self.update()

    def onscroll(self, event):
        print("%s %s" % (event.button, event.step))
        if event.button == 'up':
            self.ind = (self.ind + 1) % self.slices
        else:
            self.ind = (self.ind - 1) % self.slices
        self.update()

    def update(self):
        self.im.set_data(self.X[:, :, self.ind])
        ax.set_ylabel('slice %s' % self.ind)
        self.im.axes.figure.canvas.draw()



#%%


"""

       ÉSTE GATO ESTÁ PUESTO EN HONOR A SAB. TE AMO. GRACIAS POR TODO <3
       
                             /.   |                             .-""-.__
                             ', J'            _.--///--._____.-'        `-._
                             `-''          .-'                              `.
                                         .'                        _.._       `.
                                        /                       J""    `-.      \
                                       /                         \        `-.    `.
                                     .'          |               F           \     \
                                     F           |              /             `.    \
                               _.---<_           J             J                `-._/
                             ./`.     `.          \          J F
                          __/ F  )                 \          \
                        <     |                \    >.         L
                         `-.                    |  J  `-.      )
                           J         |          | /      `-    F
                            \  o     /         (|/        //  /
                             `.    )'.          |        /'  /
                              `---'`\ `.        |        /  /
                              '|\\ \ `. `.      |       /( /
                                 `` `  \  \     |       \_'
                                        L  L    |     .' /
                                        |  |    |    (_.'
                                        J  J    F
                                        J  J   J
                                        J  J   J                                 
                                        J  J   F                               
                                        |  |  J                                
                                        |  F  F                              
                                        F /L  |                               
                                        `' \_)'                    
"""

#%%
#X = np.random.rand(20, 20, 40)
for modName, fsuf in [('Zeppelin', ''), ('Zeppelin+Stick', '_zs')]:
    
    with open('results/crossings/alpha' + fsuf + '/final.pickle', 'rb') as f:
    #with open('results/crossings/alpha/final.pickle', 'rb') as f:
        alpDF=pickle.load(f).reset_index()
        alpDF.gt_icvfLvl = pd.Series(alpDF.gt_icvfLvl, dtype="category")
    
    with open('results/crossings/rotAng' + fsuf + '/final.pickle', 'rb') as f:    
    #with open('results/crossings/rotAng/final.pickle', 'rb') as f:
        rotDF=pickle.load(f).reset_index()
        rotDF.gt_icvfLvl = pd.Series(rotDF.gt_icvfLvl, dtype="category")
        # 4DOF normal es sólo 3DOF (rotAng, cOrt, alpha)
    with open('results/crossings/4DOF' + fsuf + '/final.pickle', 'rb') as f:
        dof4DF=pickle.load(f).reset_index()
        dof4DF.gt_icvfLvl = pd.Series(dof4DF.gt_icvfLvl, dtype="category")
        #dof4DF.distanceSurface = None #para evitar consumir tanta RAM
        
    #%%
    fig, ax = plt.subplots(1, 1)
    X = dof4DF.iloc[0,12][:,:,25,:]
    tracker = IndexTracker(ax, X)
    
    
    fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
    plt.show()
    
    
    
    #%% 4DOF en 3D
    for nlvl in [0, 5, 10, 15, 16, 23]:
        
        Z = dof4DF.iloc[nlvl,12][:,25,25,:]/np.linalg.norm(dof4DF.iloc[nlvl,11], 1)
        
        x = np.linspace(-6, 6, Z.shape[1])
        y = np.linspace(-6, 6, Z.shape[0])
        
        X, Y = np.meshgrid(x, y)
        
        fig = plt.figure()
        #ax = plt.axes()#projection='3d')
        cnt=plt.contour(X, Y, Z, 10, colors='black')
        plt.clabel(cnt, inline=True, fontsize=8)
        plt.imshow(Z, extent=[-6,6,-6,6], origin='lower', cmap='RdGy', alpha=0.5)
        plt.colorbar()
        plt.xlabel('$\hat{f}$')
        plt.ylabel('$\hat{D}_{\perp}$')
        #ax.set_zlabel('z')
        plt.axes().set_aspect('equal')
        plt.axes().get_xaxis().set_ticklabels([])
        plt.axes().get_yaxis().set_ticklabels([])
        plt.title('$f='+ dof4DF.iloc[nlvl,0] + '$, $\\theta=' + 
                  str(dof4DF.iloc[nlvl,4]) +'°$');
        plt.savefig('results/crossings/' + 'distSurf' + str(nlvl) + '.eps', 
                format='eps', dpi=1000, bbox_inches='tight', pad_inches=0)
        
    #dof4DF.distanceSurface = None
    #%% histogramas
    #bins = np.arange(-32, -22, 1)
    #
    #g = sns.FacetGrid(alpDF, col='gt_rotAng',  row='gt_icvfLvl', hue='gt_icvfLvl')
    #
    ##g.map(plt.axvline, x=0, ls=":", c=".7")
    #g = g.map(plt.hist, 'cOrt_err', bins=bins)
    #g.fig.tight_layout(w_pad=1, h_pad=3)
    #
    #plt.subplots_adjust(top=0.95)
    #g.fig.suptitle('d.o.f.: $\\alpha, D_{ort}, D_{par}$') 
    
    #%%
    #bins = np.arange(-32, -22, 1)
    #
    #g = sns.FacetGrid(rotDF, col='gt_rotAng',  row='gt_icvfLvl', hue='gt_icvfLvl')
    #
    ##g.map(plt.axvline, x=0, ls=":", c=".7")
    #g = g.map(plt.hist, 'cOrt_err', bins=bins)
    #g.fig.tight_layout(w_pad=1, h_pad=3)
    #
    #plt.subplots_adjust(top=0.95)
    #g.fig.suptitle('$\\theta, D_{ort}, D_{par}$') 
    
    
    #%% boxplots por ICVF
    plt.figure()
    #ax = sns.boxplot(x="gt_icvfLvl", y="cOrt_err", data=rotDF, showfliers = False)
    ax = sns.swarmplot(x="gt_icvfLvl", y="cOrt_err", data=rotDF, hue="gt_rotAng")
    plt.title(modName +' using $\\theta, D_{\parallel}, D_{\perp}$')
    plt.xlabel('$f$')
    plt.ylabel('$\epsilon (\hat{D}_{\perp})$, percentage')
    ax.legend_.set_title("$\\theta$")
    plt.savefig('results/crossings/' + 'rotAng' + fsuf +'.eps', 
                format='eps', dpi=1000, bbox_inches='tight', pad_inches=0)
    plt.show()
    #ax = rotDF.boxplot('cOrt_err', by='gt_icvfLvl') 
    #ax.get_figure().suptitle('$\\theta, D_{ort}, D_{par}$')
    
    
    plt.figure()
    #ax = sns.boxplot(x="gt_icvfLvl", y="cOrt_err", data=alpDF, showfliers = False)
    ax = sns.swarmplot(x="gt_icvfLvl", y="cOrt_err", data=alpDF, hue="gt_rotAng")
    plt.title(modName +' using $\\alpha, D_{\parallel}, D_{\perp}$')
    plt.xlabel('$f$')
    plt.ylabel('$\epsilon (\hat{D}_{\perp})$, percentage')
    ax.legend_.set_title("$\\theta$")
    plt.savefig('results/crossings/' + 'alpha' + fsuf +'.eps', 
                format='eps', dpi=1000, bbox_inches='tight', pad_inches=0)
    plt.show()
    
    
    #ax = alpDF.boxplot('cOrt_err', by='gt_icvfLvl') 
    #ax.get_figure().suptitle('$\\alpha, D_{ort}, D_{par}$')
    
    
    
    
    #%% boxplots por ángulo
    #plt.figure()
    ##ax = sns.boxplot(x="gt_rotAng", y="cOrt_err", data=rotDF, showfliers = False)
    #ax = sns.swarmplot(x="gt_rotAng", y="cOrt_err", data=rotDF, color=".25")
    #plt.title('Degrees of Freedom: $\\theta, D_{\perp}, D_{\parallel}$')
    #plt.show()
    ##ax = rotDF.boxplot('cOrt_err', by='gt_rotAng') 
    ##ax.get_figure().suptitle('$\\theta, D_{ort}, D_{par}$')
    #
    #plt.figure()
    ##ax = sns.boxplot(x="gt_rotAng", y="cOrt_err", data=alpDF, showfliers = False)
    #ax = sns.swarmplot(x="gt_rotAng", y="cOrt_err", data=alpDF, color=".25")
    #plt.title('Degrees of Freedom: $\\alpha, D_{\perp}, D_{\parallel}$')
    #plt.show()
    ##ax = alpDF.boxplot('cOrt_err', by='gt_rotAng') 
    ##ax.get_figure().suptitle('$\\alpha, D_{ort}, D_{par}$')
    
    
    
    #%% para 4DOF
    
    ### ICVF
    
    ##### Por angulo
    #plt.figure()
    ##ax = sns.boxplot(x="gt_rotAng", y="icvf_err", data=dof4DF, showfliers = False)
    #ax = sns.swarmplot(x="gt_rotAng", y="icvf_err", data=dof4DF, color=".25")
    #plt.title('Degrees of Freedom: $\\alpha, D_{\perp}, f, \\theta$')
    #plt.show()
    
    ###### Por icvf
    plt.figure()
    #ax = sns.boxplot(x="gt_icvfLvl", y="icvf_err", data=dof4DF, showfliers = False)
    ax = sns.swarmplot(x="gt_icvfLvl", y="icvf_err", data=dof4DF, hue="gt_rotAng")
    plt.title(modName +' using $f, \\theta, \\alpha, D_{\perp}$')
    plt.xlabel('$f$')
    plt.ylabel('$\epsilon (\hat{f})$, percentage')
    ax.legend_.set_title("$\\theta$")
    plt.savefig('results/crossings/' + '4DOF_icvf' + fsuf +'.eps', 
                format='eps', dpi=1000, bbox_inches='tight', pad_inches=0)
    plt.show()
    
    
    
    ###################################
    
    ### cOrt
    
    ##### Por angulo
    #plt.figure()
    ##ax = sns.boxplot(x="gt_rotAng", y="cOrt_err", data=dof4DF, showfliers = False)
    #ax = sns.swarmplot(x="gt_rotAng", y="cOrt_err", data=dof4DF, color=".25")
    #plt.title('Degrees of Freedom: $\\alpha, D_{\perp}, f, \\theta$')
    #plt.show()rotAng
    
    ##### Por icvf
    plt.figure()
    #ax = sns.boxplot(x="gt_icvfLvl", y="cOrt_err", data=dof4DF, showfliers = False)
    ax = sns.swarmplot(x="gt_icvfLvl", y="cOrt_err", data=dof4DF, hue="gt_rotAng")
    plt.title(modName +' using  $f, \\theta, \\alpha, D_{\perp}$')
    plt.xlabel('$f$')
    plt.ylabel('$\epsilon (\hat{D}_{\perp})$, percentage')
    ax.legend_.set_title("$\\theta$")
    plt.savefig('results/crossings/' + '4DOF_cOrt' + fsuf +'.eps', 
                format='eps', dpi=1000, bbox_inches='tight', pad_inches=0)
    plt.show()
    
    #%% tablas
    outTab = rotDF[['gt_icvfLvl', 
                    'gt_rotAng', 
                    'cPar_err', 
                    'cOrt_err', 
                    'rotAng_err']].copy()
    for col in outTab[['cPar_err', 'cOrt_err', 'rotAng_err']]:
        outTab[col] = outTab[col].map('{:.2f}%'.format)
    outTab.to_latex('results/crossings/rotAng{}.tex'.format(fsuf), 
                    float_format='%.2f',
                    index=False)
    
    outTab = alpDF[['gt_icvfLvl', 
                    'gt_rotAng', 
                    'cPar_err', 
                    'cOrt_err', 
                    'alpha_err']].copy()
    for col in outTab[['cPar_err', 'cOrt_err', 'alpha_err']]:
        outTab[col] = outTab[col].map('{:.2f}%'.format)
    outTab.to_latex('results/crossings/alpha{}.tex'.format(fsuf), 
                    float_format='%.2f',
                    index=False)
    
    outTab = dof4DF[['gt_icvfLvl', 
                    'gt_rotAng', 
                    'icvf_err', 
                    'cOrt_err', 
                    'alpha_err',
                    'rotAng_err']].copy()
    for col in outTab[['icvf_err', 'cOrt_err', 'alpha_err', 'rotAng_err']]:
        outTab[col] = outTab[col].map('{:.2f}%'.format)
    outTab.to_latex('results/crossings/4DOF{}.tex'.format(fsuf), 
                    float_format='%.2f',
                    index=False)
