// by ponco
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

inline double to_degrees(double radians){
    return radians * (180.0 / M_PI);
}

void changeEndianess(void *data, size_t size){
    u_char *p1, *p2, buf;
    p1=data; p2=data+size-1;
    for(size_t i=0; i<(size/2);i++){
        buf = *p1;
        *p1 = *p2;
        *p2 = buf;
        p1++;
        p2--;
    }
}

int main(int argc, char* argv[]){
    if(argc < 3) {fputs("Uso: trajfile angle_degrees\n", stderr); exit(4);}

    const size_t bytesPerPHeader = 8 + 4;
    const size_t bytesPerPoint = bytesPerPHeader + 8*3;
    const size_t bytesPerHeader = 8*3;

    char fPath[200];

    size_t res; //salida de fread/fwrite

    double rotAng = strtod(argv[2], NULL) * (M_PI/180.0);

    //abrir archivo
    strcpy(fPath, argv[1]);
    FILE* fp = fopen(fPath, "rb");

    if (fp == NULL) {fputs("No se pudo abrir archivo de entrada\n", stderr); exit(1);}

    //leer header
    double duration;
    size_t N, T;

    double *headBuff = calloc(bytesPerHeader, 1);

    double ceBuff;

    res = fread(headBuff, bytesPerHeader, 1, fp);
    if (res != 1) {fputs("Error de lectura\n", stderr); exit(2);}

    duration = headBuff[0];
    changeEndianess(&duration, sizeof(double));

    ceBuff = headBuff[1];
    changeEndianess(&ceBuff, sizeof(double));
    N = (size_t)ceBuff;

    //Son en realidad T+1 pasos
    ceBuff = headBuff[2];
    changeEndianess(&ceBuff, sizeof(double));
    T = (size_t)ceBuff;

    printf("N: %lu, T: %lu, duration: %f, rotAng: %f\n", N, T, duration, rotAng);

    //buffer de entrada
    u_char *ibuff = calloc(N*bytesPerPoint, 1);
    if (ibuff == NULL) {fputs("No hay suficiente memoria\n", stderr); exit(4);}

    //salida
    //strncpy(fPath,argv[1],strlen(argv[1])-5);
    fPath[strlen(argv[1])-4] = '\0';
    strcat(fPath, argv[2]);
    strcat(fPath, "rotAng.traj");

    FILE *ofp = fopen(fPath, "wb");
    if (ofp == NULL) {fputs("No se pudo abrir archivo de salida\n", stderr); exit(1);}

    //escribir header
    res = fwrite(headBuff, bytesPerHeader, 1, ofp);
    if (res != 1) {fputs("Error de escritura\n", stderr); exit(3);}

    u_char *iptr;
    double x,xOld,z;

    //por cada paso de tiempo
    for(size_t currTstep = 0; currTstep<T+1; currTstep++){
        //leer chunk a RAM
        res = fread(ibuff, N*bytesPerPoint, 1, fp);
        if (res != 1) {fputs("Error de lectura\n", stderr); exit(2);}

        //por cada caminante
        for(size_t k=0; k<N; k++){
            //saltarse subheader actual
            iptr = ibuff + k*bytesPerPoint + bytesPerPHeader;

            //leer coordenadas
            //si, está horrible el casting de pointers, meperd0nas?
            x = *((double*)iptr);
            changeEndianess(&x, sizeof(double));

            //y = *((double*)(iptr + sizeof(double)));
            //changeEndianess(&y, sizeof(double));

            z = *((double*)(iptr + 2*sizeof(double)));
            changeEndianess(&z, sizeof(double));

            //rotar alrededor de y
            xOld = x;
            x = x*cos(rotAng) + z*sin(rotAng);
            z = -xOld*sin(rotAng) + z*cos(rotAng);

            //sobreescribir
            changeEndianess(&x, sizeof(double));
            changeEndianess(&z, sizeof(double));
            *((double*)iptr) = x;
            *((double*)(iptr + 2*sizeof(double))) = z;


        }
        //escribir chunk rotado
        res = fwrite(ibuff, N*bytesPerPoint, 1, ofp);
        if (res != 1) {fputs("Error de escritura\n", stderr); exit(3);}
    }

    fclose(fp);
    fclose(ofp);

    free(ibuff);

    return 0;

}
