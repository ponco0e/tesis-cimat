#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  7 14:18:34 2019

@author: ponco
"""

#ejecutarse despues de fitTensorRoMRI.py

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D

import seaborn as sns
sns.set()

#%%

for signalNo, lvl in {1:'lvl2', 
               4:'lvl3',
               8:'lvl4', 
               12:'lvl5', 
               16:'lvl6', 
               20:'lvl7'
               }.items():
    
    currPar = 2.1e-9
    
    currOrt = dof4DF.cOrt1[signalNo]
    currAlpha = dof4DF.alpha[signalNo]
    currRotAng = dof4DF.rotAng[signalNo]
    currIcvf =  dof4DF.icvf[signalNo]
    
    
    bValue = 1000 * 1e6
    
    y = dof4DF.signal[signalNo]
    S_0 = y[0]
    
    s = np.sin((np.pi/180)*currRotAng)
    c = np.cos((np.pi/180)*currRotAng)
    rotM = np.array([
                    [c,   0.0,   s],
                    [0.0, 1.0, 0.0],
                    [-s,  0.0,   c]
                    ])
    D_t1 = np.zeros((3,3))
    D_t1[2,2] = currPar
    y_stick1 = np.exp(-bValue * currPar * (G @ np.array([0, 0, 1]))**2)
    y_stick2 = np.exp(-bValue * currPar * (G @ rotM @ np.array([0, 0, 1]))**2)
    D_t1[0,0] = D_t1[1,1] = currOrt
    D_t2 = rotM @ D_t1 @ rotM.transpose()
    y_est = currAlpha * np.exp(-bValue * np.einsum('ij,ji->i', G, D_t1 @ G.transpose()))
    y_est += (1-currAlpha) * np.exp(-bValue * np.einsum('ij,ji->i', G, D_t2 @ G.transpose()))
    y_est *= S_0
    
    dirCols= ['g_x','g_y','g_z']
    
    dotProdG = np.abs(shell1Schm[dirCols]@ np.array([0, 0, 1])).to_numpy()
    plotIdx = np.argsort(dotProdG)[1:]
    
    fig = plt.figure()
    plt.plot(dotProdG[plotIdx], y[plotIdx]/y[0], label = "$S_{sim}$")
    plt.plot(dotProdG[plotIdx], y_est[plotIdx]/y_est[0], label = "$S_{model}$")
    
    plt.title(lvl +", L1: " + str(np.linalg.norm(y-y_est)))
    plt.legend()
    
    plt.xlabel("$|g_i^T e_z|$")
    plt.ylabel("$S/S_0$")
    
    plt.ylim(0,1)