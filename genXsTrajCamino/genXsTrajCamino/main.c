// by ponco
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

void changeEndianess(void *data, size_t size){
    u_char *p1, *p2, buf;
    p1=data; p2=data+size-1;
    for(size_t i=0; i<(size/2);i++){
        buf = *p1;
        *p1 = *p2;
        *p2 = buf;
        p1++;
        p2--;
    }
}

int main(int argc, char* argv[]){
    if(argc < 2) {fputs("Necesario path a archivo .traj\n", stderr); exit(4);}

    const size_t bytesPerPHeader = 8 + 4;
    const size_t bytesPerPoint = bytesPerPHeader + 8*3;
    const size_t bytesPerHeader = 8*3;

    char fPath[100];

    size_t res;

    //abrir archivo
    strcpy(fPath, argv[1]);
    FILE* fp = fopen(fPath, "rb");

    if (fp == NULL) {fputs("No se pudo abrir archivo de entrada\n", stderr); exit(1);}

    //leer header
    double duration, N, T;

    fread(&duration, sizeof(double), 1, fp);
    changeEndianess(&duration, sizeof(double));

    fread(&N, sizeof(double), 1, fp);
    changeEndianess(&N, sizeof(double));

    //Son en realidad T+1 pasos
    res = fread(&T, sizeof(double), 1, fp);
    changeEndianess(&T, sizeof(double));


    if (res != 1) {fputs("Error de lectura\n", stderr); exit(2);}
    printf("N: %f, T: %f, duration: %f", N, T, duration);


    //leer delta deseado
    int nSteps;
    if(argc == 3){
        double delta = strtod(argv[2], NULL);
        double tStep = duration/T;
        nSteps = round(delta/tStep);
        printf(", delta: %f\n", delta);
        printf("tStep: %f, nSteps: %d\n", tStep, nSteps); //DBG
    }else{
        nSteps = 1;
        printf("\n");
    }

    //leer traj
    double *iniTraj = (double*)calloc(3*N, sizeof(double));
    double *finTraj = (double*)calloc(3*N, sizeof(double));
    double *xs = (double*)calloc(3*N, sizeof(double));
    double buf;

    double *memPtr = NULL;

    double time;
    int sIndex;

    //paso 0: inicio, paso 1: final
    for(int step=0; step<2; step++){
        //por cada paso de tiempo en intervalo de longitud delta
        for(int currTstep = 0; currTstep<nSteps; currTstep++){
            //resetear punteros a buffers de entrada
            if(step==0) {memPtr=iniTraj;}
            else{memPtr=finTraj;}

            //por cada caminante
            for(int k=0; k<N; k++){
                //leer subheader
                fread(&time, sizeof(double), 1, fp);
                changeEndianess(&time, sizeof(double));

                fread(&sIndex, sizeof(int), 1, fp);
                changeEndianess(&sIndex, sizeof(int));

                //leer coordenadas
                for(int coord =0; coord<3; coord++){
                    res=fread(&buf, sizeof(double), 1, fp);
                    if (res != 1) {fputs("Error de lectura\n", stderr); exit(2);}
                    changeEndianess(&buf, sizeof(double));
                    *memPtr += buf;
                    memPtr++;
                }
            }
        }
        fseek(fp, (T+1-nSteps)*N*bytesPerPoint + bytesPerHeader, SEEK_SET);
    }

    fclose(fp);
    //obtener y escalar los desplazamientos
    double *iniPtr, *finPtr;

    iniPtr = iniTraj;
    finPtr = finTraj;
    memPtr = xs;
    for(uint i=0; i<3*N; i++){
        *memPtr = 1e6*(*iniPtr/nSteps - *finPtr/nSteps);
        iniPtr++; finPtr++;
        memPtr++;
    }

    //escribir a disco
    strcat(fPath, "_delta");
    if(argc == 3) strcat(fPath, argv[2]);
    else strcat(fPath, "0");
    strcat(fPath, ".xs");

    fp = fopen(fPath, "wb");
    if (fp == NULL) {fputs("No se pudo abrir archivo de salida\n", stderr); exit(1);}

    fwrite(&N, sizeof(double), 1, fp);
    fwrite(&T, sizeof(double), 1, fp);
    res=fwrite(xs, sizeof(double), 3*N, fp);

    if (res != 3*((size_t)N)) {fputs("Error de escritura\n", stderr); exit(3);}

    fclose(fp);

    free(iniTraj); free(finTraj); free(xs);

    return 0;

}
