function MatLS = createMat_2_Ds_free(gs,alpha1)
    nMeas = length(gs);
    
    for i=1:nMeas
        MatLS(i,1) = gs(i,1) ^2;
        MatLS(i,2) = gs(i,2) ^2;
        MatLS(i,3) = gs(i,3) ^2;
        MatLS(i,4) = 2*gs(i,1)*gs(i,2);
        MatLS(i,5) = 2*gs(i,1)*gs(i,3);
        MatLS(i,6) = 2*gs(i,2)*gs(i,3);

        MatLS(i,1+6) = gs(i,1) ^2;
        MatLS(i,2+6) = gs(i,2) ^2;
        MatLS(i,3+6) = gs(i,3) ^2;
        MatLS(i,4+6) = 2*gs(i,1)*gs(i,2);
        MatLS(i,5+6) = 2*gs(i,1)*gs(i,3);
        MatLS(i,6+6) = 2*gs(i,2)*gs(i,3);
    
    
    end
    
    alpha2 = 1 - alpha1;
    
    MatLS(:,1:6) =  alpha1*MatLS(:,1:6);
    MatLS(:,7:12) = alpha2*MatLS(:,7:12);

end
