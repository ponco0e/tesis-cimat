function [bw_opt,rad_opt] =optBandwidthDensity(N,maxTime)

    %T,idxSubstrate,strSubstrates,generic_names_data,my_path,process
    %xs = load3DPointsMC_in_um(N,T,idxSubstrate,strSubstrates,generic_names_data,my_path,process);

    [DTeX,DTiX,DTeZ,DTiZ,~,~,~] = load_DTs_GT();
    
    DT1 = DTiX;
    DT2 = DTiZ;
    
    %xs = [ mvnrnd([0;0;0],DT1*2*maxTime,N*1000) ; mvnrnd([0;0;0],DT2*2*maxTime,N*1000)];  alpha1 = 0.5;
    xs =  mvnrnd([0;0;0],DT1*2*maxTime,2*N*1000);  alpha1 = 1.0;

    
    bw_opt = -1;
    dist_opt = 1e30;
    rad_opt = -1;
    
    rads = [0.5 1.5 3 5 7.5 9 ; 1 3 6 10 16 30 ; 2 6 12 20 32 60 ];

    k=1;
    for nRads = 1:size(rads,1)
    
        pts = cargaPnts3Dsample(rads(nRads,:));
        
        px_analit = MMG_grid(DT1*2*maxTime,DT2*2*maxTime,alpha1,pts(1,:)'*1e-6,pts(2,:)'*1e-6,pts(3,:)'*1e-6);
        px_analit = px_analit /sum(px_analit);
    
        for bw = 0.5:0.2:12.0;    
            px_kernel = mvksdensity(xs',pts,bw);
            dist(k) =KLdi3v(px_analit,px_kernel');
            if dist(k) < dist_opt
                dist_opt = dist(k);
                bw_opt = bw;
                rad_opt = nRads;
            end
            k = k+1;
        end

    end
    
    
    display(dist_opt)
    display(bw_opt)
    display(rad_opt)
    
    %scalePerBin = 1.0;
    %maxAvgDisplacement = sqrt(2*coeff_diff*maxTime) * 1e6; % en um
    %nBins = int32(maxAvgDisplacement*5);
    %hist3D = zeros(nBins,2*nBins,2*nBins);
            
    
end
