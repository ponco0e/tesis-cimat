function Ds = computeDFromAnalytic(gs,l1_1,l1_23,l2_1,l2_23,alpha1)

    MatLS = createMat_2_Ds_free(gs',alpha1);
    
    tens = [ l1_1,l1_23,l1_23,0,0,0, l2_23,l2_23,l2_1,0,0,0 ]';
    
    Ds = MatLS * tens;

    gSc =[];
    gSc(1,:) = gs(1,:) .* Ds';
    gSc(2,:) = gs(2,:) .* Ds';
    gSc(3,:) = gs(3,:) .* Ds';
    
    figure; showPointCloud([gSc';-gSc'],'MarkerSize',20);
    xlabel('X (m)'); ylabel('Y (m)'); zlabel('Z (m)');    
    
end
