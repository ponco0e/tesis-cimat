function plotSubstrateCamino

close all

figure; hold on;

machinePath = '/Users/alram';
dataPath = '/Dropbox/datosEPFL/MC_camino_simulations';
filename = 'rad_coord';


dat = csvread([machinePath dataPath '/' filename '.txt']);
dat = dat*1e6;

radc = dat(:,1);
xc = dat(:,2);
yc = dat(:,3);
zc = dat(:,4);


for i=1:length(xc)
    h = circle2(xc(i),yc(i),radc(i));
end

min(radc)
max(radc)

svx=35;
svy=35;
rectangle('Position',[0 0 svx svy]);


nPs =1000;
x = rand(nPs,1)*svx ;
y = rand(nPs,1)*svy;
indIntra = zeros(nPs,1) > 1;



end


function h = circle2(x,y,r)
d = r*2;
px = x-r;
py = y-r;
h = rectangle('Position',[px py d d],'Curvature',[1,1]);
daspect([1,1,1])
end




%[xc,yc,zc,radc] = textread('/Users/alram/tempPrograms/MC-DC-Simulator-master/test/centers.txt','%f %f %f %f','headerlines',1);
%[xc,yc,zc,radc] = textread([machinePath dataPath filename],'%f %f %f %f','headerlines',1);

%[xc,yc,zc,radc] = textread('/Users/alram/Downloads/centers_e.txt','%f %f %f %f','headerlines',1);

%[xc,yc,zc,ox,oy,oz,radc] = textread('/Users/alram/Dropbox/datosEPFL/simulations/analysis_traj/analytic/confs/centers_z_orientation.txt','%f %f %f %f %f %f %f','headerlines',1);
%[xc,yc,zc,ox,oy,oz,radc,dummy1,dummy2,dummy3] = textread('/Users/alram/Dropbox/datosEPFL/simulations/axonPackUndulation/voxel2/plys/cylinders_gamma_ondulation_3D_Amp0.3_voxelSize5_6.27_0.1225_23.txt','%f %f %f %f %f %f %f %f %f %f');


%[xw,yw,zw] = textread('/Users/alram/tempPrograms/MC-DC-Simulator-master/test/ini_walker_pos.txt','%f %f %f','headerlines',1);
%[xw,yw,zw] = textread('/Users/alram/Dropbox/datosEPFL/simulations/analysis_traj/analytic/confs/ini_walker_extra_pos.txt');

%posIntra=[];
%nPosIntra = 0;
