function [l1s,l2s,errorOpt] = optl1l2_2T_3D(histsMC,nBins,maxTime,alpha1)

    e1=[1;0;0]; e2=[0;1;0]; e3 = [0;0;1];   
    %e2 = [0.0636;0.9932;-0.0977]; e3=[-0.0744;-0.0929;-0.9929]; e1=[-0.9952;0.0704;0.0680];
    
    
    
    rl1 = 1e-9:0.1e-9:2.5e-9;
    rl2 = 0.01e-9:0.005e-9:0.9e-9;
    
    matVals = combvec(rl1,rl2);
    
    
    errors = zeros(size(matVals,2),1);
    

    parfor i=1:size(matVals,2)
        
        DT1 = espectrum2DT(e1,e2,e3,matVals(1,i),matVals(2,i),matVals(2,i))*2*maxTime;
        DT2 = espectrum2DT(e3,e2,e1,matVals(1,i),matVals(2,i),matVals(2,i))*2*maxTime;


        hist3DAnaly = computeHistAnaly3D(DT1,DT2,alpha1,nBins);
        %errors(i) = computeErrorHists3D(histsMC,hist3DAnaly,nBins);
        errors(i) = KLdi3v(histsMC,hist3DAnaly);
    end

    [errorOpt, idxMin] = min(errors);

    l1s = matVals(1,idxMin);
    l2s = matVals(2,idxMin);
    
    DT1 = espectrum2DT(e1,e2,e3,l1s,l2s,l2s)*2*maxTime;
    DT2 = espectrum2DT(e3,e2,e1,l1s,l2s,l2s)*2*maxTime;
    hist3D_s = computeHistAnaly3D(DT1,DT2,alpha1,nBins);

    plotHist3D(hist3D_s,'Result optimization');
    
end
