function e = computeErrorHists(histsMC,histAnaly)
    hDif = (histsMC - histAnaly).^2;
    e = sqrt(sum(hDif,2))/size(histsMC,2);
    e = sum(e)/size(histsMC,1);
    
end
