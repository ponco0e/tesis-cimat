function [ l1Dist ] = cross0TargetFunc( x_cort, x_cpar, hist3D_comb_s , maxTime, bins, scalepBin, scaledKernCM)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
T1 =  MakeDT_Matrix(x_cort, 0, 0, x_cort, 0, x_cpar);

%generar histograma analitico suavizado
hist3D_an_s = computeHistAnaly3D(T1*2*maxTime + scaledKernCM, ...
    T1, ... %dummy
    1.0, ...
    bins, ...
    scalepBin);

%comparar
l1Dist = double(gather(computeErrorHists3DL1(hist3D_comb_s, hist3D_an_s)));

end

