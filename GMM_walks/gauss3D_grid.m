function v = gauss3D_grid(Sig,X,Y,Z)

    iSig = inv(Sig);
    
    v = (1/( ((2*pi)^(3/2))*(det(Sig)^(1/2)))) * exp(-0.5*( iSig(1,1)*(X.*X) + iSig(2,2)*(Y.*Y) + iSig(3,3)*(Z.*Z) ...
                                                           +2*(iSig(1,2)*(X.*Y) + iSig(1,3)*(X.*Z) + iSig(2,3)*(Y.*Z))  ));
%    v =                                           exp(-0.5*( iSig(1,1)*(X.*X) + iSig(2,2)*(Y.*Y) + iSig(3,3)*(Z.*Z) ...
%                                                            +2*(iSig(1,2)*(X.*Y) + iSig(1,3)*(X.*Z) + iSig(2,3)*(Y.*Z))  ));
end
