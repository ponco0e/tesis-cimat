function Ds = computeDFromSamples(samples,gs,maxTime)

    

    dq = zeros(length(gs),1);

    samples = samples * 1e3; % convertir a mm
    
    for k=1:length(gs)
        dq(k) = accumulateDQM(dq(k),gs(:,k),samples');
    end

    
    Ds = (dq./(length(samples))) / (2*maxTime);

    
    gSc =[];
    gSc(1,:) = gs(1,:) .* Ds';
    gSc(2,:) = gs(2,:) .* Ds';
    gSc(3,:) = gs(3,:) .* Ds';
    
    figure; showPointCloud([gSc';-gSc'],'MarkerSize',20);
    xlabel('X (m)'); ylabel('Y (m)'); zlabel('Z (m)');
    
end
