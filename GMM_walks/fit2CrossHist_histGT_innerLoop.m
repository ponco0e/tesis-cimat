function [ subdistances ] = fit2CrossHist_histGT_innerLoop( hist3D_comb_s, rotAng, GKern, bins, scalepBin, maxTime , diff_ort, diff_par, scaledKernCM)
%fit2CrossHist_histGT_innerLoop loop interno para intentar usar multiples
%gpus
%   by ponco

%crear subtensor para cada proceso
subdistances = zeros(length(diff_ort), ...
    length(diff_par), length(diff_par), 'gpuArray');

j = 1;
for c_ort = diff_ort %por cada coeff ortogonal compartido
    k = 1;
    for c_par1 = diff_par  %por cada coeff paralelo (T1)
        l = 1;
        for c_par2 = diff_par %por cada coeff paralelo (T2)
            %disp(['Buscando en ang=' num2str(rotAng) '°, c_ort=' num2str(c_ort) ', c_par1= ' num2str(c_par1) ', cpar2=' num2str(c_par2) ])
            %generar tensores
            T1 =  MakeDT_Matrix(c_ort, 0, 0, c_ort, 0, c_par1);
            T2 =  MakeDT_Matrix(c_ort, 0, 0, c_ort, 0, c_par2);
            
            %rotar T2
            rotang_r = deg2rad(rotAng); %a radianes
            orient2 = [-sin(rotang_r); 0 ; cos(rotang_r)];
            R = fromToRotation([0; 0; 1], orient2);
            T2 = R*T2*R';
            
            %generar histograma analitico suavizado
            %hist3D_an = gpuArray(computeHistAnaly3D(T1*2*maxTime,T2*2*maxTime,0.5,bins,scalepBin));
            %hist3D_an_s = SmoothHistogram(hist3D_an, GKern);
            hist3D_an_s = computeHistAnaly3D(T1*2*maxTime + scaledKernCM, ...
                T2*2*maxTime + scaledKernCM, ...
                0.5, ...
                bins, ...
                scalepBin);
            
            %comparar
            subdistances(j,k,l) = computeErrorHists3DL1(hist3D_comb_s, hist3D_an_s);
            
            l = l+1;
        end
        k = k+1;
    end
    j = j+1;
end


end

