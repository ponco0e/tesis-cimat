function [result] = fit2CrossHist_histGT(hist3D, GKernRAM, bins, scalepBin, maxTime, scaledKernCM, groundTruthParams)
%by ponco

%coeficientes a barrer
%diff_rotang = 55.0 : 5.0 : 95.0;
%diff_ort = 1e-9*(0.1 : 0.2 : 1.5);
%diff_par = 1e-9*(1.6 : 0.2 : 2.5);

%NOTA: usar pasos impares (para incluir al valor central en cuestión)
angTol = groundTruthParams.gtRotang*0.15;
angSteps = 13;

ortCDTol = groundTruthParams.gtCDOrt*0.999;
ortCDSteps = 13;

parCDTol = str2double(groundTruthParams.gtCD)*0.15;
parCDSteps = 13;



diff_rotang = groundTruthParams.gtRotang-angTol  :  2*angTol/(angSteps-1)     : groundTruthParams.gtRotang+angTol;
diff_ort =    groundTruthParams.gtCDOrt-ortCDTol :  2*ortCDTol/(ortCDSteps-1) : groundTruthParams.gtCDOrt+ortCDTol;
diff_par =    str2double(groundTruthParams.gtCD)-parCDTol    :  2*parCDTol/(parCDSteps-1) : str2double(groundTruthParams.gtCD)+parCDTol;

%de prueba
%diff_rotang = 55.0 : 20.0 : 95.0;
%diff_ort = 1e-9*(0.1 : 0.5 : 1.5);
%diff_par = 1e-9*(1.5 : 0.5 : 2.5);


%angulo, coeficientes del T1, coeficientes del T2
distances = zeros(...
    length(diff_rotang), ... %angulo
    length(diff_ort), ... %coeficiente ortogonal compartido
    length(diff_par), length(diff_par),  ... %coeficientes paralelos (T1, T2)
    'gpuArray');

%para evitar warning de overhead
%GKernRAM = FGs(:,:,:,s);

%GKern = gpuArray(GKernRAM);
    
%cada GPU tiene su copia del ground truth
%hist3Dgpu = gpuArray(hist3D);
%hist3D_comb_s = SmoothHistogram(hist3Dgpu, GKern);
hist3D_comb_s = SmoothHistogram(hist3D, GKernRAM);

%par
for i = 1 : length(diff_rotang)  %por cada angulo (T2)
    rotAng = diff_rotang(i);
    
    %copiar para evitar cache miss (?)
    cp_ort = diff_ort;
    cp_par = diff_par;
    
    distances(i,:,:,:) = fit2CrossHist_histGT_innerLoop(hist3D_comb_s, rotAng, GKernRAM, bins, scalepBin, maxTime, cp_ort, cp_par, scaledKernCM);
end


%conseguir minimo
[minL1, flatIdx] = min(distances(:));
%minL1
%distances(flatIdx)

%obtener del indice flettened los coeficientes de las tablas de busqueda

[minRotangIdx, minOrtIdx, minParIdx1, minParIdx2] = ind2sub(size(distances),flatIdx);
%distances(minOrtIdx, minParIdx, minRotangIdx)

rotang_min = diff_rotang(minRotangIdx);
coef_ort_min = diff_ort(minOrtIdx);
coef_par_min1 = diff_par(minParIdx1);
coef_par_min2 = diff_par(minParIdx2);


disp([ 'min L1: ', num2str(minL1), ...
    ', RotAng: ', num2str(rotang_min), '°', ...
    ', C_ort: ', num2str(coef_ort_min), ...
    ', C_par1: ', num2str(coef_par_min1), ...
    ', C_par2: ', num2str(coef_par_min2)
    ])


result = struct('minL1', gather(minL1), ...
    'rotAng', rotang_min, ...
    'cOrt', coef_ort_min, ...
    'cPar1', coef_par_min1, ...
    'cPar2', coef_par_min2, ...
    'distanceSurface', gather(distances));

end
