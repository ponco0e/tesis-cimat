function [params, DB,vecindades,nRotAlea,RotAlea] = createDB_single_Crossing()
% Generate several configurations of analytical distributions for axon
% crossing.
% params:
% DB:
% vecindades:
% nRotAlea:
% RotAlea
    DB =[];

    nRotAlea = 1;

    %generate 3x3 random rotation matrices
    RotAlea = zeros(nRotAlea,3,3);
    for r=1:nRotAlea
%         A = rand(3,3); A = A*A'; 
%         [RotAlea(r,:,:), ~ ] = eig(A);
        vecRand = rand(3,1); vecRand = vecRand ./ norm(vecRand);
        RotAlea(r,:,:) = fromToRotation([1; 0; 0], vecRand);
    end
        
    % difusion perpendicular y paralela
    
    %l1 = (2)*1e-9;
    %l2 = 3.125e-11 ; %(0.25)*1e-9;  
    
    l1 = (1.8:0.1:2.1)*1e-9;
    l2 = (0.1:0.1:0.5)*1e-9;
    
    
    %l1 = 1.8*1e-9;
    %l2 = 0.5*1e-9;
    
    
    %single bundle
    params1 = combvec(l1,l2);
    nParams = size(params1,2);
    
    nExperi = 0;
    for i=1:nParams
        T1 = diag([params1(1,i) params1(2,i) params1(2,i)]);    
        for r=1:nRotAlea
            T1 = squeeze(RotAlea(r,:,:))*T1*squeeze(RotAlea(r,:,:))';  
            
            nExperi = nExperi + 1;
            DB(nExperi,1:6 ) = [T1(1,1), T1(1,2), T1(1,3), T1(2,2), T1(2,3), T1(3,3)];
            DB(nExperi,7:12) = zeros(1,6);
            DB(nExperi,13) = 1.0;
            
        end
        
    end
    
    
%     idxs = (1:length(params1))';
%     
%     
%     [salto, dosVecs]=compVecinos(length(params1),length(l2),idxs,length(params1));
%     vecs1(:,1:2) = dosVecs;
%     [~, dosVecs]=compVecinos(salto,length(l1),idxs,length(params1));
%     vecs1(:,3:4) = dosVecs;
%         
%     vecs1(vecs1 < repmat(idxs,1,4)) = 0;
%     
%     vecs1 = fliplr(vecs1);
%     
%     vecs1 = [zeros(size(vecs1,1),2) vecs1 zeros(size(vecs1,1),6) ];
    
    
    
    params1 = [zeros(nParams,1) params1' zeros(nParams,2) ones(nParams,1) ];
    

    % 2 crossing bundles
    angles = [60 70 80 90]*pi/180; % crossing angle in rads
    alphas = [0.4 0.45 0.5];
    %angles = 80*pi/180; % crossing angle in rads
    %alphas= [0.4 0.5];
    
    params2 = combvec(angles,l1,l2,l1,l2,alphas);
    
    nParams = size(params2,2);
    
    for i=1:nParams
        
        T1 = diag([params2(2,i) params2(3,i) params2(3,i)]);    
        T2 = diag([params2(4,i) params2(5,i) params2(5,i)]);    
        orient2 = [cos(params2(1,i)); sin(params2(1,i)); 0];
        R = fromToRotation([1; 0; 0], orient2);
        T2 = R*T2*R';
        
        for r=1:nRotAlea
            
            T1 =  squeeze(RotAlea(r,:,:))*T1*squeeze(RotAlea(r,:,:))';  T2 =  squeeze(RotAlea(r,:,:))*T2*squeeze(RotAlea(r,:,:))';
            
            nExperi = nExperi +1;
            DB(nExperi,1:6 ) = [T1(1,1), T1(1,2), T1(1,3), T1(2,2), T1(2,3), T1(3,3)];
            DB(nExperi,7:12) = [T2(1,1), T2(1,2), T2(1,3), T2(2,2), T2(2,3), T2(3,3)];
            DB(nExperi,13) = params2(6,i);
            
        end
        
        
    end

    vecindades = [];
    
    
%     idxs = (1:length(params2))';
%     
%     [salto, dosVecs]=compVecinos(length(params2),length(alphas),idxs,length(params2));
%     vecs2(:,1:2) = dosVecs;
%     [salto, dosVecs]=compVecinos(salto,length(l2),idxs,length(params2));
%     vecs2(:,3:4) = dosVecs;
%     [salto, dosVecs]=compVecinos(salto,length(l1),idxs,length(params2));
%     vecs2(:,5:6) = dosVecs;
%     [salto, dosVecs]=compVecinos(salto,length(l2),idxs,length(params2));
%     vecs2(:,7:8) = dosVecs;
%     [salto, dosVecs]=compVecinos(salto,length(l1),idxs,length(params2));
%     vecs2(:,9:10) = dosVecs;
%     [~, dosVecs]=compVecinos(salto,length(angles),idxs,length(params2));
%     vecs2(:,11:12) = dosVecs;
%     
%     vecs2(vecs2 < repmat(idxs,1,12)) = 0;
%     
%     vecs2(vecs2 ~= 0) = vecs2(vecs2 ~= 0) + size(vecs1,1); % sumarle a los indices la dimension de vecs1
%     
%     
%     vecs2 = fliplr(vecs2);
%     
%     vecindades = [vecs1;vecs2];

    
    params = [ params1 ;params2'];
    
end
