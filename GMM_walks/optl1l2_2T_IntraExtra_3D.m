function [l1s_intra,l2s_intra,l1s_extra,l2s_extra, errorOpt] = optl1l2_2T_IntraExtra_3D(histsMC,nBins,maxTime,alpha1)

    e1=[1;0;0]; e2=[0;1;0]; e3 = [0;0;1];   
    %e2 = [0.0636;0.9932;-0.0977]; e3=[-0.0744;-0.0929;-0.9929]; e1=[-0.9952;0.0704;0.0680];
    
    
    
    rl1_intra = 1.5e-9:0.1e-9:2.3e-9;
    %rl2_intra = 0.01e-9:0.005e-9:0.8e-9;
    rl2_intra = 0.01e-9:0.03e-9:0.8e-9;

    rl1_extra = rl1_intra;
    rl2_extra = rl2_intra;
    
    
    matVals = combvec(rl1_intra,rl2_intra,rl1_extra,rl2_extra);
    
    
    errors = zeros(size(matVals,2),1);
    

    parfor i=1:size(matVals,2)
        
        DT1_intra = espectrum2DT(e1,e2,e3,matVals(1,i),matVals(2,i),matVals(2,i))*2*maxTime;
        DT1_extra = espectrum2DT(e1,e2,e3,matVals(3,i),matVals(4,i),matVals(4,i))*2*maxTime;

        DT2_intra = espectrum2DT(e3,e2,e1,matVals(1,i),matVals(2,i),matVals(2,i))*2*maxTime;
        DT2_extra = espectrum2DT(e3,e2,e1,matVals(3,i),matVals(4,i),matVals(4,i))*2*maxTime;

        hist3DAnaly = computeHistAnaly3D(DT1_intra,DT2_intra,alpha1,nBins) + computeHistAnaly3D(DT1_extra,DT2_extra,alpha1,nBins);
        
        hist3DAnaly = hist3DAnaly / sum(hist3DAnaly(:));
        

        %errors(i) = computeErrorHists3D(histsMC,hist3DAnaly,nBins);
        errors(i) = KLdi3v(histsMC,hist3DAnaly);
    end

    [errorOpt, idxMin] = min(errors);

    l1s_intra = matVals(1,idxMin);
    l2s_intra = matVals(2,idxMin);

    l1s_extra = matVals(3,idxMin);
    l2s_extra = matVals(4,idxMin);
    
    DT1_intra = espectrum2DT(e1,e2,e3,l1s_intra,l2s_intra,l2s_intra)*2*maxTime;
    DT1_extra = espectrum2DT(e1,e2,e3,l1s_extra,l2s_extra,l2s_extra)*2*maxTime;
    
    DT2_intra = espectrum2DT(e3,e2,e1,l1s_intra,l2s_intra,l2s_intra)*2*maxTime;
    DT2_extra = espectrum2DT(e3,e2,e1,l1s_extra,l2s_extra,l2s_extra)*2*maxTime;
    
    
    hist3D_s = computeHistAnaly3D(DT1_intra,DT2_intra,alpha1,nBins) + computeHistAnaly3D(DT1_extra,DT2_extra,alpha1,nBins);
    hist3DAnaly = hist3DAnaly / sum(hist3DAnaly(:));


    plotHist3D(hist3D_s,'Result optimization');
    
end
