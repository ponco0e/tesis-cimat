%function fit_GMM_walks

%inicializar

clc , clear, close all


maxTime = 0.047; %max(TE); %

coeff_diff = 2.1e-9;

rng(100);

%[params, DB, vecindades, nRotAlea, RotAlea] = createDB_single_Crossing();

%inspect = 1;
%params = params(inspect,:);
%DB = DB(inspect,:);


% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
% parametros del los histogramas
scalePerBin = 3; % escala que define el tamanio de los hisotgramas
display(scalePerBin)

maxAvgDisplacement = sqrt(2*coeff_diff*maxTime) * 1e6; % desplazamisnt maximo en um
nBins = int32(maxAvgDisplacement*8);

tamHist = double([2*nBins,2*nBins,2*nBins])
sigmas = 6.6; % sigmas de suavizado
display(sigmas)

%%

% crear kernels de suavizado
FGs = zeros([2*nBins, 2*nBins, 2*nBins, length(sigmas)]);
sepFGs = zeros([length(sigmas), 3, 2*nBins]);
for s=1:length(sigmas)
    [FGs(:,:,:,s), sepFGs(s, :, :)] = makeKernel(tamHist,sigmas(s),nBins);
end

scaledKernCM = eye(3,3) * (sigmas(s) * (1/scalePerBin) * 1e-6)^2;

%% estadisticas de vectores de desplazamiento
%MD = mean( (xs*1e-6))
%MSD = mean( (xs*1e-6) .^2)
%estimated_cdiffus = MSD/(2*maxTime)
%relError_perComponent = 100*abs( estimated_cdiffus - coeff_diff)/coeff_diff



%% explorar parámetros con dos cruces

cPath = '/home/ponco/devel/cimat/tesis-cimat/recorrersimuladormc/xss/';

icvfs = [0.37179500637303714 0.5228519024512746 0.7207069831677195];

%parpool(gpuDeviceCount);
for gt_rotang=60.0 : 10.0 : 90.0
%for gt_rotang=80.0 : 10.0 : 90.0 %SE TRABÓ
    for gt_cd={'1.9e-9' '2e-9' '2.1e-9'}
        currICVF = 1;
        for gt_icvfLvl={'low' 'mid' 'high'}
            gt_icvf = icvfs(currICVF);
            
            groundTruthParams = struct('gtRotang', gt_rotang, ...
                'gtCD', gt_cd{1}, ...
                'gtICVFLvl', gt_icvfLvl{1}, ...
                'gtCDOrt', 0.0);
            
            %cargar matrices de desplazamiento para los casos intra y extra
            pathIntra = strjoin({cPath gt_icvfLvl{1} 'ICVF_cd' gt_cd{1} '_gammac_' 'intra' '.traj.xs'}, '');
            pathExtra = strjoin({cPath gt_icvfLvl{1} 'ICVF_cd' gt_cd{1} '_gammac_' 'extra' '.traj.xs'}, '');
            
            [xs_intra, N, T] = readCaminoxs(pathIntra);
            [xs_extra, N, T] = readCaminoxs(pathExtra);
            
            %calcular groundTruth de C. ortogonal
            eigvals_intra = sort(eig(cov(1e-6*xs_intra)/(2*maxTime)));
            eigvals_extra = sort(eig(cov(1e-6*xs_extra)/(2*maxTime)));
            aproxOCD_intra = (eigvals_intra(1) + eigvals_intra(2))/2;
            aproxOCD_extra = (eigvals_extra(1) + eigvals_extra(2))/2;cPath = '/home/ponco/devel/cimat/tesis-cimat/recorrersimuladormc/xss/';
            groundTruthParams.gtCDOrt = gt_icvf*aproxOCD_intra + ...
                (1-gt_icvf)*aproxOCD_extra;
            
            %generar histograma del manojo 1
            hist3D_intra = computeHistogram3DFromSamples(xs_intra,nBins,scalePerBin);
            hist3D_extra = computeHistogram3DFromSamples(xs_extra,nBins,scalePerBin);
            hist3D_1 = gt_icvf*hist3D_intra + (1-gt_icvf)*hist3D_extra;
            
            
            %rotar vectores de desplazamiento
            rotang_r = deg2rad(gt_rotang); %a radianes
            orient2 = [-sin(rotang_r); 0 ; cos(rotang_r)];
            R = fromToRotation([0; 0; 1], orient2);
            %xs_intra = (R*xs_intra'*R')';
            xs_intra = (R*xs_intra')';
            %xs_extra = (R*xs_extra'*R')';
            xs_extra = (R*xs_extra')';
            
            %generar histograma del manojo 2 (rotado)
            hist3D_intra = computeHistogram3DFromSamples(xs_intra,nBins,scalePerBin);
            hist3D_extra = computeHistogram3DFromSamples(xs_extra,nBins,scalePerBin);
            hist3D_2 = gt_icvf*hist3D_intra + (1-gt_icvf)*hist3D_extra;
            
            
            %generar histograma ground truth
            hist3D = 0.5*hist3D_1 + 0.5*hist3D_2;
            
            %plotHist3D(hist3D,2,'From simulator: y' );
            %break
            
            %realizar búsqueda 1 cruce
            disp(['Corriendo ahora 1Cross: gt_rotang:' num2str(gt_rotang) ', gt_cdPar:' gt_cd{1} ...
                ', gt_cdOrt:' num2str(groundTruthParams.gtCDOrt) ', gt_icvfLvl:' gt_icvfLvl{1}])
            tic
            res = fit2CrossHist_histGT(hist3D, FGs(:,:,:,s), nBins, scalePerBin, maxTime, scaledKernCM, groundTruthParams);
            toc
            %serializar a json y guardar
            finalRes = mergestruct(groundTruthParams, res);
            rfName = strjoin({cPath 'Cross1_' 'rotAng' num2str(gt_rotang) '_' gt_cd{1} 'CD_' gt_icvfLvl{1} 'ICVF' '.json' }, '');
            savejson('', finalRes, rfName);
            
            disp('')
            
            currICVF = currICVF + 1;
        end
    end
end


%% hacer experimentos con un cruce

for gt_cd={'1.9e-9' '2e-9' '2.1e-9'}
    currICVF = 1;
    for gt_icvfLvl={'low' 'mid' 'high'}
        gt_icvf = icvfs(currICVF);
        
        groundTruthParams = struct('gtCD', gt_cd{1}, ...
            'gtICVFLvl', gt_icvfLvl{1}, ...
            'gtCDOrt', 0.0);
        
        %cargar matrices de desplazamiento para los casos intra y extra
        pathIntra = strjoin({cPath gt_icvfLvl{1} 'ICVF_cd' gt_cd{1} '_gammac_' 'intra' '.traj.xs'}, '');
        pathExtra = strjoin({cPath gt_icvfLvl{1} 'ICVF_cd' gt_cd{1} '_gammac_' 'extra' '.traj.xs'}, '');
        
        [xs_intra, N, T] = readCaminoxs(pathIntra);
        [xs_extra, N, T] = readCaminoxs(pathExtra);
        
        %calcular groundTruth de C. ortogonal
        eigvals_intra = sort(eig(cov(1e-6*xs_intra)/(2*maxTime)));
        eigvals_extra = sort(eig(cov(1e-6*xs_extra)/(2*maxTime)));
        aproxOCD_intra = (eigvals_intra(1) + eigvals_intra(2))/2;
        aproxOCD_extra = (eigvals_extra(1) + eigvals_extra(2))/2;
        groundTruthParams.gtCDOrt = gt_icvf*aproxOCD_intra + ...
            (1-gt_icvf)*aproxOCD_extra;
        
        %generar histograma del manojo 1
        hist3D_intra = computeHistogram3DFromSamples(xs_intra,nBins,scalePerBin);
        hist3D_extra = computeHistogram3DFromSamples(xs_extra,nBins,scalePerBin);
        hist3D_1 = gt_icvf*hist3D_intra + (1-gt_icvf)*hist3D_extra;
        
        %hacer lo mismo para el experimento sin cruces
        disp(['Corriendo ahora 0Cross: gt_cdPar:' gt_cd{1} ...
                ', gt_cdOrt:' num2str(groundTruthParams.gtCDOrt) ', gt_icvfLvl:' gt_icvfLvl{1}])
        tic
        res = fit1CrossHist_histGT(hist3D_1, FGs(:,:,:,s), nBins, scalePerBin, maxTime, scaledKernCM, groundTruthParams);
        toc
        %serializar a json y guardar
        finalRes = mergestruct(groundTruthParams, res);
        rfName = strjoin({cPath 'Cross0_' 'rotAng' num2str(gt_rotang) '_' gt_cd{1} 'CD_' gt_icvfLvl{1} 'ICVF' '.json' }, '');
        savejson('', finalRes, rfName);
        
        disp('')
        
        currICVF = currICVF + 1;
    end
end

