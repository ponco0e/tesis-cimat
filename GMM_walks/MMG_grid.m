function v = MMG_grid(DT1,DT2,a1,X,Y,Z)
v = a1*gauss3D_grid(DT1,X,Y,Z);

%v = mvnpdf([X(:) Y(:) Z(:)],[0,0,0],DT1);
%v = reshape(v,length(Y),length(X),length(Z));
%v = a1*v/sum(v(:));
if (1-a1) > 0
    v = v + (1-a1)*gauss3D_grid(DT2,X,Y,Z);
    
    %v = mvnpdf([X(:) Y(:) Z(:)],[0,0,0],DT2);
    %v = reshape(v,length(Y),length(X),length(Z));
    %v = v + (1-a1)*v/sum(v(:));
end
end