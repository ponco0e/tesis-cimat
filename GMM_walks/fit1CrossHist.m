function [coeffs] = fit1CrossHist(xs_intra, xs_extra, FGs, s, bins, scalepBin, icvf, maxTime)
%by ponco

%generar histogramas
hist3D_sim_intra = computeHistogram3DFromSamples(xs_intra,bins,scalepBin);
hist3D_sim_extra = computeHistogram3DFromSamples(xs_extra,bins,scalepBin);

%histograma combinado
hist3D_comb = icvf*hist3D_sim_intra + (1-icvf)*hist3D_sim_extra;
%plotHist3D(hist3D_comb,1,'From simulator (complete)');
%suavizar
hist3D_comb_s = SmoothHistogram(hist3D_comb, FGs(:,:,:,s));
%plotHist3D(hist3D_comb_s,1,'From simulator (smoothed)');


%coeficientes a barrer
diff_ort = 1e-9*(0.01 : 0.005 : 0.6);
diff_par = 1e-9*(0.2 : 0.01 : 1.4);

distances = zeros(length(diff_ort), length(diff_par));


i = 1;
for c_ort = diff_ort %por cada coeff ortogonal a z candidato
    j = 1;
    for c_par = diff_par %por cada coeff paralelo a z candidato
        %generar tensor
        T_samp =  MakeDT_Matrix(c_ort, 0, 0, c_ort, 0, c_par);
        
        %xs_gauss = mvnrnd([0;0;0],T_samp*2*maxTime,N);
        %xs_gauss = xs_gauss * 1e6; %el muestreo los genera en m, se converten a um
        %hist3D_samples_g = computeHistogram3DFromSamples(xs_gauss,nBins,scalePerBin);
        
        %generar histograma analitico suavizado
        hist3D_an = computeHistAnaly3D(T_samp*2*maxTime,T_samp,1.0,bins,scalepBin);
        hist3D_an_s = SmoothHistogram(hist3D_an, FGs(:,:,:,s));
        
        %comparar
        distances(i,j) = computeErrorHists3DL1(hist3D_comb_s, hist3D_an_s);
        
        j = j + 1;
    end
    i = i + 1;
end

[minL1perCol, minL1Rows] = min(distances); %encontrar el menor elemento
[minL1, minL1ColIdx] = min(minL1perCol);
minL1RowIdx = minL1Rows(minL1ColIdx);

coef_ort_min = diff_ort(minL1RowIdx);
coef_par_min = diff_par(minL1ColIdx);

disp([ 'min L1: ', num2str(minL1), ', C_ort: ', num2str(coef_ort_min), ', C_par: ', num2str(coef_par_min) ])

%figure;
%%heatmap(diff_par, diff_ort, distances)
%[X, Y] = meshgrid(diff_par, diff_ort);
%surf(X, Y, distances)
%xlabel('Coeff. Paralelo');
%ylabel('Coeff. Ortogonal');
%zlabel('Distancia L1');
%colorbar

coeffs = [coef_ort_min, coef_par_min];

end