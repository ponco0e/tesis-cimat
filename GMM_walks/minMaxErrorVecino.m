function minErrPerExp = minMaxErrorVecino(params,DB,nRotAlea,RotAlea,porcError,maxTime,nBins,scalePerBin,FGs)

    RotAleaLoc = squeeze(RotAlea);

    %uno=1;
    %errores = [];
    %nErrores = zeros(size(params,2),1);
    
    nSigmas = size(FGs,4);
    
    minErrPerExp = zeros(size(DB,1),nSigmas);
    tic
    parfor k=1:size(DB,1)
        
        
        
        %crear histograma de este parametro

        DBv = DB(k,:); 
        % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        %para sacar las matrices de covarianza del, por ejemplo, renglon 10,000
        T1 = MakeDT_Matrix(DBv(1), DBv(2), DBv(3), DBv(4 ), DBv(5 ), DBv(6 ));
        T2 = MakeDT_Matrix(DBv(7), DBv(8), DBv(9), DBv(10), DBv(11), DBv(12));
        alpha = DBv(13); % porcentaje de mezcla
        % calcular histograma analitico para esas 2 matrices de covarianza
        histAnalytic = computeHistAnaly3D(T1*2*maxTime,T2*2*maxTime,alpha,nBins,scalePerBin);
        
        paramsO = params(k,:);
        
        erroresLocalesMin = ones(nSigmas,1)*inf;
        
        for sig=-1:2:1
            if paramsO(6) == 1 % 1 tensor
                valsParam = [paramsO(1:end-1) + sig*paramsO(1:end-1)*porcError , 1];
                idxParams = [2,3];
            else                % 2 tensores
                valsParam = paramsO + sig*paramsO*porcError;
                idxParams = 1:size(paramsO,2);
            end
            
            for nVec =idxParams
                %%%%%
                newParams = paramsO;
                newParams(nVec) = valsParam(nVec);
                
                %crear histograma de c/u de estos vecinos de parametro
                
                %erroresLocales = zeros(nRotAlea,1);
                
                if paramsO(6) == 1 % 1 tensor
                    T1 = diag([newParams(2) newParams(3) newParams(3)]);    
                    T2 = zeros(3,3);    
                    for r=1:nRotAlea
                        T1 = RotAleaLoc*T1*RotAleaLoc';  
                        % calcular histograma analitico para esas 2 matrices de covarianza
                        histAnalyticVeci = computeHistAnaly3D(T1*2*maxTime,T2*2*maxTime,newParams(6),nBins,scalePerBin);
                    end                
                else                % 2 tensores
                    %uno=0;
                    T1 = diag([newParams(2) newParams(3) newParams(3)]);    
                    T2 = diag([newParams(4) newParams(5) newParams(5)]);    
                    orient2 = [cos(newParams(1)); sin(newParams(1)); 0];
                    R = fromToRotation([1; 0; 0], orient2);
                    T2 = R*T2*R';
        
                    for r=1:nRotAlea
                        T1 =  RotAleaLoc*T1*RotAleaLoc';  
                        T2 =  RotAleaLoc*T2*RotAleaLoc';
                        % calcular histograma analiticoidxPrec para esas 2 matrices de covarianza
                        histAnalyticVeci = computeHistAnaly3D(T1*2*maxTime,T2*2*maxTime,newParams(6),nBins,scalePerBin);
                        
                    end            
                end
                
               for s=1:nSigmas
                    %display([s idx])            
                    % - - - - - 
                    %Smooth histograma Samples
                    histAnalytic_s =  SmoothHistogram( histAnalytic, FGs(:,:,:,s)) ;
                    histAnalyticVeci_s =  SmoothHistogram( histAnalyticVeci, FGs(:,:,:,s)) ;

                    %erroresLocales= computeErrorHistsProyecciones(histAnalytic_s,histAnalyticVeci_s,T1,T2,newParams(6),nBins,scalePerBin); 
                    %erroresLocales(s) = computeErrorHistsLineas(histAnalytic_s,histAnalyticVeci_s,T1,T2,newParams(6),nBins,scalePerBin);
                    %erroresLocales (s)= KLdiv(histAnalidxPrecytic_s,histAnalyticVeci_s);
                    %erroresLocales (s)= computeErrorHists3DL2(histAnalytic_s,histAnalyticVeci_s);
                    erroresLocaless = computeErrorHists3DL1(histAnalytic_s,histAnalyticVeci_s);

                    %nErrores(nVec) = nErrores(nVec) +1;
                    %errores(nVec,nErrores(nVec)) = erroresLocales;
                    %fprintf('\n%d %.8f',nVec,erroresLocales);

                    if erroresLocaless < erroresLocalesMin(s)
                        erroresLocalesMin(s) = erroresLocaless;
                    end
               end
                
                
            end
                
        end
        
        minErrPerExp(k,:) = erroresLocalesMin;
        
    end
    toc
%  if uno == 1    
%         %para un solo manojo
%         minErrorL1  = min( errores(2,1:nErrores(2)))
%         minErrorL2  = min( errores(3,1:nErrores(3)))
%         maxErrorL1  = max( errores(2,1:nErrores(2)))
%         maxErrorL2  = max( errores(3,1:nErrores(3)))
% 
% 
%         minErrTot = min([ minErrorL1 minErrorL2 ])
%         maxErrTot = max([ maxErrorL1 maxErrorL2 ])
% 
% 
%         figure; hist(errores(2,1:nErrores(2)) ); title('L1');
%         figure; hist(errores(3,1:nErrores(3)) ); title('L2');
% 
% 
%         return
%     
%  else  
%     %compute the minimal
% 
%     minErrorAngle = min(errores(1,1:nErrores(1)))
%     minErrorL1  = min( [errores(2,1:nErrores(2)) errores(4,1:nErrores(4)) ])
%     minErrorL2  = min( [errores(3,1:nErrores(3)) errores(5,1:nErrores(5)) ])
%     minErrorAlpha = min(errores(6,1:nErrores(6)))
% 
%     %maximos
%     
%     maxErrorAngle = max(errores(1,1:nErrores(1)))
%     maxErrorL1  = max( [errores(2,1:nErrores(2)) errores(4,1:nErrores(4)) ])
%     maxErrorL2  = max( [errores(3,1:nErrores(3)) errores(5,1:nErrores(5)) ])
%     maxErrorAlpha = max(errores(6,1:nErrores(6)))
%     
%     minErrTot = min([minErrorAngle minErrorL1 minErrorL2 minErrorAlpha])
%     maxErrTot = max([maxErrorAngle maxErrorL1 maxErrorL2 maxErrorAlpha])
%     
%     figure; hist(errores(1,1:nErrores(1))); title('angle');
%     figure; hist([errores(2,1:nErrores(2)) errores(4,1:nErrores(4)) ]); title('L1');
%     figure; hist([errores(3,1:nErrores(3)) errores(5,1:nErrores(5)) ]); title('L2');
%     figure; hist(errores(6,1:nErrores(6))); title('alpha');
%  end

end
