function [ ps ] = mvksdensity( x, pts, sigma )
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    dim=size(x,1);
    nxs=size(x,2);
    nys=size(pts,2);
    ps=zeros(1,nys);
    for n=1:nys
        mu=repmat(pts(:,n),1,nxs);
        Pos=((x-mu).^2);
        Pos=sum(Pos,1)/(2*sigma.^2);
        ps(n)=sum((1/(sqrt(sigma.^2*(2*pi).^dim))*exp(-Pos)));

    end
    ps=ps/sum(ps);
end
