function [FG , sepFG]= makeKernel(tamker,sigma,nBins)

    %tamker(1)=tamker(1)*2;
    mu=[0,0,0];
    S=eye(3,3)*sigma^2;
    x=(-floor(tamker(1)/2)+1:floor(tamker(1)/2));
    y=(-floor(tamker(1)/2)+1:floor(tamker(1)/2));
    z=(-floor(tamker(1)/2)+1:floor(tamker(1)/2));
    [Y,X,Z]=meshgrid(y,x,z);


    G = mvnpdf([X(:) Y(:) Z(:)],mu,S);
    G = reshape(G,length(y),length(x),length(z));
    G=G/sum(G(:));
    %figure; imshow(G(:,:,72),[])
    
    %plotHist3D(G,'analytic')
    
    G = circshift(G,[-(nBins-1),-(nBins-1),-(nBins-1)]);


    FG=fftn(G);
    
    sepFG(1,:) = normpdf(x, 0, sigma);
    sepFG(2,:) = normpdf(y, 0, sigma);
    sepFG(3,:) = normpdf(z, 0, sigma);

end
