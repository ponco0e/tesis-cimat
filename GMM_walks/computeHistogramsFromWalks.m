function [hists,nBins] = computeHistogramsFromWalks(T,N,idxSubstrate,strSubstrates,generic_names_data,gs,my_path,maxTime,coeff_diff,process)

    maxAvgDisplacement = sqrt(2*coeff_diff*maxTime) * 1e6; % en um

    nBins = int32(maxAvgDisplacement*5);
   
    %nSubstrates = length(idxSubstrate);
    
    
    %traj files
    T = T*1000; % in tousands for the computations
    
    hists = zeros(length(gs),nBins);

    for nSub = idxSubstrate
        strSubstrate = strSubstrates(nSub,:);
        generic_name_data = generic_names_data(nSub,:);
        
        for np = process
            my_file = [generic_name_data strSubstrate '_' num2str(np-1) '.traj'];
            fprintf('\n%s',my_file)

            fileID = fopen([my_path 'outputs/' my_file],'rb');
            traj = fread(fileID,'float32');
            fclose(fileID);        

            N_p = length(traj)/(3*(T+1));
            traj = reshape(traj,3,N_p*(T+1))';

            %posFinCentered = zeros(3,N_p);


%             figure(100); hold on, axis equal
%             for i=1:500
%                 block = (1:T+1)+(i-1)*(T+1);
%                 pos = traj(block,:);
%                 %display(pos(1,:))
%                 %pos(:,3) = pos(:,3) - repmat(pos(1,3),T+1,1 );
%                 scatter3(pos(:,1), pos(:,2), pos(:,3),'.')
%                 %scatter3(traj( [block(1);block(end)] ,1) , traj( [block(1);block(end)] ,2), traj( [block(1);block(end)] ,3),'.')
%             end        

            for i=1:N_p
                block = (1:T+1)+(i-1)*(T+1);
                posFinCentered = (traj( block(end) ,:)  -  traj( block(1) ,:)) * 1e3; % en um            

                for k=1:length(gs)                    
                    pos = abs(posFinCentered*gs(:,k));

                    nPos = floor(pos)+1;
                    %nPos
                    hists(k,nPos) = hists(k,nPos) + 1;
                end
            end

        end 

    end
    
    for k=1:length(gs) 
        hists(k,:) = hists(k,:) / sum(hists(k,:));
    end
    
    
end
