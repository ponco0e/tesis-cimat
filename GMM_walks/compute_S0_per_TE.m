function [S0_per_TE, sigmaEmpiric_per_TE] = compute_S0_per_TE(protocol,meas)

S0_per_TE = zeros(protocol.totalmeas,1);
sigmaEmpiric_per_TE = zeros(protocol.totalmeas,1);

for i=1:length(protocol.uTE)
    idxB0 = intersect ( find(protocol.TE == protocol.uTE(i)),protocol.b0_Indices);

    %S0_TE = mean(meas(idxB0));
    S0_TE = median(meas(idxB0));
    
    sigmaEmp = std(meas(idxB0));
    idxDW = find(protocol.TE == protocol.uTE(i));
    S0_per_TE(idxDW) = S0_TE;
    sigmaEmpiric_per_TE(idxDW) = sigmaEmp;
end


end