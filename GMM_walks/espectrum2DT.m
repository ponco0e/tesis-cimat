function DT = espectrum2DT(e1,e2,e3,l1,l2,l3)
    DT = l1*(e1*e1') + l2*(e2*e2') + l3*(e3*e3');
end


function v = MMG_grid(DT1,DT2,a1,X,Y,Z)
    v = a1*gauss3D_grid(DT1,X,Y,Z);
    if (1-a1) > 0
        v = v + (1-a1)*gauss3D_grid(DT2,X,Y,Z);
    end
end
