function v = MMG(DT1,DT2,a1,x)
    v = a1*gauss3D(DT1,x)+(1-a1)*gauss3D(DT2,x);
end
