function plotHist3D(hist3D,red,myTitle)
%     hist3D = hist3D + 1; % evitar log(0)
%     v = log(log(log(squeeze(hist3D(:,1,:)))+1)+1);
%     maxV = max(v(:));
%     scale = 255/maxV;
%     figure; imshow(v*scale,jet(255))
    
%     v = squeeze(hist3D(:,1,:));
%     figure; imshow(log(v),[]); colormap(jet)

    switch(red)
        case 1
            nBins = size(hist3D,1);
            v = squeeze(hist3D(int32(nBins/2)+1,:,:));
        case 2
            nBins = size(hist3D,2);
            v = squeeze(hist3D(:,int32(nBins/2)+1,:));
        case 3
            nBins = size(hist3D,3);
            v = squeeze(hist3D(:,:,int32(nBins/2)+1));
    end
    figure;
    %hold on;

    imshow(v,[]);
    colormap(gca,jet)
    title(myTitle)

end
