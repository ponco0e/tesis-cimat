function [T1,T2] = fitGMM_2_Ds_free(Ds,gs,alpha1)


    MatLS = createMat_2_Ds_free(gs,alpha1);
    
    %sol = MatLS\Ds;
    sol = inv(MatLS'*MatLS+1e-4*eye(12))*MatLS'*Ds;
    %sol = lsqnonneg(MatLS,Ds) %esto no siempre se puede, pero en el caso de difusi?n en los ejes can?nicos si.
    
    figure, plot(Ds,'r')   , hold on; 
    plot(MatLS*sol,'b+')    
    
    
    T1 = [sol(1)   sol(4)   sol(5);   sol(4)   sol(2)   sol(6);   sol(5)   sol(6)   sol(3)];
    T2 = [sol(1+6) sol(4+6) sol(5+6); sol(4+6) sol(2+6) sol(6+6); sol(5+6) sol(6+6) sol(3+6)];
    
    [E1, L1] = eig(T1);
    [E2, L2] = eig(T2);
    
    
end
