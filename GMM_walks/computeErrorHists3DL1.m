function e = computeErrorHists3DL1(histsMC,histAnaly)
    [dx,dy,dz] = size(histsMC);
    
    %hist_dif3D = abs(histsMC-histAnaly);
    %e = (1/(dx*dy*dz))*sum(hist_dif3D(:)); %diferencia en masa
    
    e = (1/(dx*dy*dz))*sum( abs(histsMC(:) - histAnaly(:)) );
    %e = sum( abs(histsMC(:) - histAnaly(:)) );
end
