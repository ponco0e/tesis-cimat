function [hist3D,nBins] = computeHistogram3DFromWalks(T,idxSubstrate,strSubstrates,generic_names_data,my_path,maxTime,coeff_diff,process)

    scalePerBin = 1.0;
    
    maxAvgDisplacement = sqrt(2*coeff_diff*maxTime) * 1e6; % en um

    nBins = int32(maxAvgDisplacement*5);
   
    hist3D = zeros(nBins,2*nBins,2*nBins);
        
    %traj files
    T = T*1000; % in tousands for the computations
    

    for nSub = idxSubstrate
        strSubstrate = strSubstrates(nSub,:);
        generic_name_data = generic_names_data(nSub,:);
        
        for np = process
            my_file = [generic_name_data strSubstrate '_' num2str(np-1) '.traj'];
            fprintf('\n%s',my_file)

            fileID = fopen([my_path 'outputs/' my_file],'rb');
            traj = fread(fileID,'float32');
            fclose(fileID);        

            N_p = length(traj)/(3*(T+1));
            traj = reshape(traj,3,N_p*(T+1))';

 
            for i=1:N_p
                block = (1:T+1)+(i-1)*(T+1);
                posFinCentered = (traj( block(end) ,:)  -  traj( block(1) ,:)) * 1e3*scalePerBin; % en um, * 4 hace que cada bin sea de 0.25 um            
                %posFinCentered = floor(posFinCentered)+0.5;
                %posFinCentered = [3.5e-3; 3.5e-3; 3.5e-3]*1e3*scalePerBin;

                if posFinCentered(1)<0 % trabajar son en el semiespacion x>0
                    posFinCentered = -posFinCentered;
                end
                
                idx3D = floor(posFinCentered) + 1 ; %sumar [1,1,1] por logica 1
                idx3D(2) = idx3D(2) + nBins;
                idx3D(3) = idx3D(3) + nBins;
                
                if idx3D(1) <= nBins && idx3D(1) <= 2*nBins && idx3D(3) <= 2*nBins
                    hist3D(idx3D(1),idx3D(2),idx3D(3)) = hist3D(idx3D(1),idx3D(2),idx3D(3)) + 1; % contar
                else
                    dislpay('fuera de bines!!!')
                end
            end

        end 

    end
    
    hist3D = hist3D /sum(hist3D(:));
    
    
end
