function [strSubstrates, generic_names_data,idxSubstrate ] = selectInputData(N,T) 
%En esta función defines todos los experimentos que vas a leer, por ejemplo para 2 manojos cada uno con su extra y su intra (4 compartimientos)
    

    % el nombre del compartimiento y las propiedades del experimento de los archivos que son la salida del simular MC

    strSubstrates = [ ...
         '_extra_' num2str(N) 'N_' num2str(T) 'T'; ...
         '_intra_' num2str(N) 'N_' num2str(T) 'T'; ...
         '_extra_' num2str(N) 'N_' num2str(T) 'T'; ...
         '_intra_' num2str(N) 'N_' num2str(T) 'T'; ...

    ];

 

    % el nombre del experimento de los archivos que son la salida del simular MC

    generic_names_data=[...
         'bundle_z'; ...
         'bundle_z'; ...
         'bundle_x'; ...
         'bundle_x'; ...

    ];

 

    % AQUI SELECCIONAS QUE CONjUNTOS (indices) DE ARCHVIVOS QUIERES LEER: una intra en X, las 2 intra, la extra en Z, etc.


    %idxSubstrate = 1:2; %fibra en Z
    %idxSubstrate = 3:4; %fibra en X
    %idxSubstrate = 1:4; %cruce X Z
 

    %idxSubstrate = [2,4]; % 2 intras
    %idxSubstrate = [1,3]; % 2 extras
    

    %idxSubstrate = 3; %extra en X
    %idxSubstrate = 4; %intra en X
    idxSubstrate = 1; %extra en Z
    %idxSubstrate = 2; %intra en Z

    

end
