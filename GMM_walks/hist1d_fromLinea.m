function [h_vals,centers] =  hist1d_fromLinea(X,Y,Z,i_idx,j_idx,k_idx,hist3D,vrecta,minXH,maxXH,scalePerBin1D)


    nP = length(i_idx);
    dimXH = (maxXH - minXH)*scalePerBin1D + 1;
    h_vals = zeros(dimXH,1);

    centers = minXH:1/scalePerBin1D:maxXH;
    
    %figure; hold on; axis equal;
    
    for k=1:nP
        pp = X(i_idx(k),j_idx(k),k_idx(k))*vrecta(1) + Y(i_idx(k),j_idx(k),k_idx(k))*vrecta(2) + Z(i_idx(k),j_idx(k),k_idx(k))*vrecta(3);
        
        %plot3(X(i_idx(k),j_idx(k),k_idx(k)), Y(i_idx(k),j_idx(k),k_idx(k)), Z(i_idx(k),j_idx(k),k_idx(k)),'+');
        %display([X(i_idx(k),j_idx(k),k_idx(k)), Y(i_idx(k),j_idx(k),k_idx(k)), Z(i_idx(k),j_idx(k),k_idx(k))])
        pp = round(pp*scalePerBin1D)/scalePerBin1D;
        
        idx = (pp - minXH)*scalePerBin1D + 1; %ROUND?
        if idx >0 && idx <= dimXH
            h_vals(idx) = h_vals(idx) + hist3D(i_idx(k),j_idx(k),k_idx(k));
        end
        
    end
    %hold off;
    
    h_vals = h_vals / sum(h_vals);    
end
