

%set fibre direction
fibredir = [0;0;1];

cpath = '/home/ponco/devel/cimat/tesis-cimat/recorrersimuladormc/'

%read protocol
my_scheme_file = 'schemes/moddedDyrby_1shell_b1000.0.txt';
[bval , bvec ,G ,delta ,DELTA , TE  ]=readChallenge2015ProtocolData(cpath,my_scheme_file);
protocol = Challenge2015_2_Protocol(bval , bvec ,G ,delta ,DELTA ,TE );

%read Signal file
my_file = 'crossings_camino/highICVF_cd2.1e-9_gammac_extra.traj.bfloat.DWI.txt';
S_MC = load([cpath '' my_file]);

% get the mean S0 value
S0 = mean(S_MC(protocol.b0_Indices));

%normalize signal
S_MC = S_MC ./ S0;
% see the signal (the same (unique signal 2 times)
%seeSignalQlty(S_MC,S_MC,protocol,fibredir,S0);

% Fit a tensor
D = FitLinearDT(S_MC, protocol);

% get eigen values
eigsVals = getEigenVals(D)
