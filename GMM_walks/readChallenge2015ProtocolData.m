function [bval, bvec,G,delta,DELTA, TE ]=readChallenge2015ProtocolData(my_path,myFile)

    %cd(my_path); %wtf
    myFile = strjoin({my_path myFile}, '');
    [gx, gy ,gz, G, DELTA, delta, TE] = textread(myFile,'%f %f %f %f %f %f %f','headerlines', 1);

    %compute b values
    gmr = 2*pi*42.576*1e6; %de MHZ/T ->  rad/(sec T)
    %G in  T/m
    %delta  in   sec
    %DELTA in   sec

    bval = gmr^2 * G.^2 .* delta.^2 .* (DELTA-delta/3) ; % de s/m^2 -> s/mm^2
    bvec = [gx,gy,gz];
    

end