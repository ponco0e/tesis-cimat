function [ HS ] = SmoothHistogram( Hist, FFTKer )
%     %UNTITLED2 Summary of this function goes here
%     %   Detailed explanation goes here
%     tam=size(Hist);
%     %tam(1)=tam(1)*2;
%     %se hace el histograma con valores negativos de x?s, se construye con el
%     %espejo
%     HA=zeros(tam);
%     HA(floor(tam(1)/2)+1:end,:,:)=Hist;
%     HA(1:floor(tam(1)/2),:,:)=flip(Hist,1);
%     
    
    FHA=fftn(Hist);
    FHSA=FHA.*FFTKer;
    HS=ifftn(FHSA);
    %HS=HSA(floor(tam(1)/2)+1:end,:,:);
end
