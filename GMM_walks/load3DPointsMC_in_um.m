function xs = load3DPointsMC_in_um(N,T,idxSubstrate,strSubstrates,generic_names_data,my_path,process)

%traj files

%T = T*1000; % in tousands for the computations



%xs = zeros(3,N*1000*length(idxSubstrate));
%xs = zeros(3,N*length(idxSubstrate));
%idxXs = 1;


xs = []
for nSub = idxSubstrate
    strSubstrate = strSubstrates(nSub,:);
    generic_name_data = generic_names_data(nSub,:);
    
    
    for np = process
        my_file = [generic_name_data strSubstrate '_' num2str(np-1) '.traj'];    
        fprintf('\n%s',my_file)
        
        
        fileID = fopen([my_path 'outputs\' my_file],'rb');
        traj = fread(fileID,'float32');
        fclose(fileID);
        
        N_p = length(traj)/(3*(T+1));
        traj = reshape(traj,3,N_p*(T+1))';
        
        idxs = 1 : (T+1) : (N_p*(T+1));
        
        xsp = (traj( idxs+T ,:)  -  traj( idxs ,:)) * 1e3; % en um, () en el archivo viene en mm.
        xs = vertcat(xs, xsp);
        
        %for i=1:N_p    
            %block = (1:T+1)+(i-1)*(T+1);
            %posFinCentered = (traj( block(end) ,:)  -  traj( block(1) ,:)) * 1e3; % en um, () en el archivo viene en mm.
            %xs(:,idxXs) = posFinCentered';
            %idxXs = idxXs+1;
        %end
        clearvars xsp idxs traj
    end
    
end



end
