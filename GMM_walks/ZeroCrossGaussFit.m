clc , clear, close all


maxTime = 0.047; %max(TE); %

coeff_diff = 2.1e-9;

rng(100);

%[params, DB, vecindades, nRotAlea, RotAlea] = createDB_single_Crossing();

%inspect = 1;
%params = params(inspect,:);
%DB = DB(inspect,:);


% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
% parametros del los histogramas
scalePerBin = 3; % escala que define el tamanio de los hisotgramas
display(scalePerBin)

maxAvgDisplacement = sqrt(2*coeff_diff*maxTime) * 1e6; % desplazamisnt maximo en um
nBins = int32(maxAvgDisplacement*8);

tamHist = double([2*nBins,2*nBins,2*nBins])
sigmas = 6.6; % sigmas de suavizado
display(sigmas)

%%

% crear kernels de suavizado
FGs = zeros([2*nBins, 2*nBins, 2*nBins, length(sigmas)]);
sepFGs = zeros([length(sigmas), 3, 2*nBins]);
for s=1:length(sigmas)
    [FGs(:,:,:,s), sepFGs(s, :, :)] = makeKernel(tamHist,sigmas(s),nBins);
end

scaledKernCM = eye(3,3) * (sigmas(s) * (1/scalePerBin) * 1e-6)^2;



%% hacer los experimentos
cPath = '../recorrersimuladormc/crossings_camino/';

%icvfs = [0.37179500637303714 0.5228519024512746 0.7207069831677195];
%icvfLvls = {'low' 'mid' 'high'};
icvfs = [0.20284519076726346 0.30812232342739665 0.4014456195874796 ... 
    0.5016271405754553 0.5016271405754553 0.7064550523980454];
icvfLvls = {'lvl2' 'lvl3' 'lvl4' 'lvl5' 'lvl6' 'lvl7'};
gt_cd = {'2.1e-9'};

lMetric = 1;

fig1 = figure();
set(gcf,'PaperUnits','points','PaperPosition',[0,0,2000,530])
fig2 = figure();
set(gcf,'PaperUnits','points','PaperPosition',[0,0,2000,530])

currICVF = 1;
for gt_icvfLvl=icvfLvls
    gt_icvf = icvfs(currICVF);
    
    % groundtruth
    groundTruthParams = struct('gtCD', gt_cd{1}, ...
        'gtICVFLvl', gt_icvfLvl{1}, ...
        'gtCDOrt', 0.0, ...
        'lMetric', lMetric);
    
    %cargar matrices de desplazamiento para los casos intra y extra
    pathIntra = strjoin({cPath gt_icvfLvl{1} 'ICVF_cd' gt_cd{1} '_gammac_' 'intra' '.traj_delta0.xs'}, '');
    pathExtra = strjoin({cPath gt_icvfLvl{1} 'ICVF_cd' gt_cd{1} '_gammac_' 'extra' '.traj_delta0.xs'}, '');
    
    [xs_intra, N, T] = readCaminoxs(pathIntra);
    [xs_extra, N, T] = readCaminoxs(pathExtra);
    
    %calcular groundTruth de C. ortogonal
    eigvals_intra = sort(eig(cov(1e-6*xs_intra)/(2*maxTime)));
    eigvals_extra = sort(eig(cov(1e-6*xs_extra)/(2*maxTime)));
    aproxOCD_intra = (eigvals_intra(1) + eigvals_intra(2))/2;
    aproxOCD_extra = (eigvals_extra(1) + eigvals_extra(2))/2;
    groundTruthParams.gtCDOrt = gt_icvf*aproxOCD_intra + ...
        (1-gt_icvf)*aproxOCD_extra;
    
    
    %generar histograma del manojo 1
    hist3D_intra = computeHistogram3DFromSamples(xs_intra,nBins,scalePerBin);
    hist3D_extra = computeHistogram3DFromSamples(xs_extra,nBins,scalePerBin);
    hist3D_1 = gt_icvf*hist3D_intra + (1-gt_icvf)*hist3D_extra;
    hist3D_1_smooth = SmoothHistogram(hist3D_1, FGs(:,:,:,s));
    
    
    
    %----------------------------------------hist3D_intra-------------
    % calcular marginales
    binSize = 1e-6*(1/scalePerBin);
    
    margX = sum(squeeze(sum(hist3D_1, 2)), 2);
    margX = margX(:)/binSize;
    margX_s = sum(squeeze(sum(hist3D_1_smooth, 2)), 2);
    margX_s = margX_s(:)/binSize;
    
    margY = sum(squeeze(sum(hist3D_1, 1)), 2);
    margY = margY(:)/binSize;
    margY_s = sum(squeeze(sum(hist3D_1_smooth, 1)), 2);
    margY_s = margY_s(:)/binSize;
    
    margZ= sum(squeeze(sum(hist3D_1, 1)), 1);
    margZ= margZ(:)/binSize;
    margZ_s = sum(squeeze(sum(hist3D_1_smooth, 1)), 1);
    margZ_s = margZ_s(:)/binSize;
    
    % marginales de intra y extra
    margI = sum(squeeze(sum(hist3D_intra, 2)), 2);
    margI = margI(:)/binSize;
    margE = sum(squeeze(sum(hist3D_extra, 2)), 2);
    margE = margE(:)/binSize;
    
    %----------------------------------------------
    % calcular estimados del histograma
    
    xgrid = 1e-6*(  ((-double(nBins)+0.5)/scalePerBin):(1/scalePerBin):(double(nBins)-0.5)/scalePerBin );
    xgrid = xgrid(:);
    
    estMeanX = sum(xgrid .* margX * binSize);
    estVarX = sum( margX* binSize .* (xgrid.^2) ) - estMeanX^2;
    estMeanX_s = sum(xgrid .* margX_s* binSize);
    estVarX_s = sum( margX_s* binSize .* (xgrid.^2) ) - estMeanX_s^2;
    
    estMeanY = sum(xgrid .* margY* binSize);
    estVarY = sum( margY* binSize .* (xgrid.^2) ) - estMeanY^2;
    estMeanY_s = sum(xgrid .* margY_s* binSize);
    estVarY_s = sum( margY_s* binSize .* (xgrid.^2) ) - estMeanY_s^2;
    
    estMeanZ = sum(xgrid .* margZ* binSize);
    estVarZ = sum( margZ* binSize .* (xgrid.^2) ) - estMeanZ^2;
    estMeanZ_s = sum(xgrid .* margZ_s* binSize);
    estVarZ_s = sum( margZ_s* binSize .* (xgrid.^2) ) - estMeanZ_s^2;
    
    estMeanI = sum(xgrid .* margI* binSize);
    estVarI = sum( margI* binSize .* (xgrid.^2) ) - estMeanI^2;
    estMeanE = sum(xgrid .* margE* binSize);
    estVarE = sum( margE* binSize .* (xgrid.^2) ) - estMeanE^2;
    
    
    %------------------------------------------------------------
    % ajustar gaussiana
    
    %norm_func = @(p,x) p(1) * exp(-((x-p(2)).^2)/(2*p(3)));
    norm_func = @(p,x) (1/sqrt(2*pi*p(3))) * exp(-((x-p(2)).^2)/(2*p(3)));
    
    if(isa(lMetric, 'numeric'))
        coefEstsX = gridSearch1DGaussFit( xgrid, margX, estVarX, lMetric, binSize);
        
        coefEstsI = gridSearch1DGaussFit( xgrid, margI, estVarI, lMetric, binSize);
        coefEstsE = gridSearch1DGaussFit( xgrid, margE, estVarE, lMetric, binSize);
        
        coefEstsX_s = gridSearch1DGaussFit( xgrid, margX_s, estVarX_s, lMetric, binSize);
    else
        coefEstsX = gridSearch1DGaussFit( margX, 0, estVarX, lMetric, binSize);
        
        coefEstsI = gridSearch1DGaussFit( 1e-6*xs_intra(:,1), 0, estVarI, lMetric, binSize);
        coefEstsE = gridSearch1DGaussFit( 1e-6*xs_extra(:,1), 0, estVarE, lMetric, binSize);
        
        coefEstsX_s = gridSearch1DGaussFit( xgrid, 0, estVarX_s, lMetric, binSize);
    end
    
    
%     xgrid = (  ((-double(nBins)+0.5)/scalePerBin):(1/scalePerBin):(double(nBins)-0.5)/scalePerBin );
%     xgrid = xgrid(:);
%     
%     %ajustar gaussiana
%     initGuess(1) = max(margX);  % amplitude
%     initGuess(2) = 0;       % media
%     initGuess(3) = 0.5;      %  varianza
%     
%     
%     norm_func = @(p,x) p(1) .* exp(-((x-p(2)).^2)/(2*p(3)));
%     norm_func2 = @(p,x) p(1) .* exp(-((x - p(1))/p(2)).^2);
%     
%     coefEstsX = nlinfit(xgrid, margX, norm_func2, initGuess);
%     coefEstsY = nlinfit(xgrid, margY, norm_func2, initGuess);
%     coefEstsZ = nlinfit(xgrid, margZ, norm_func2, initGuess);
%     
%     coefEstsI = nlinfit(xgrid, margI, norm_func2, initGuess);
%     coefEstsE = nlinfit(xgrid, margE, norm_func2, initGuess);
%     
%     initGuess(1) = max(margX_s);  % amplitude
%     coefEstsX_s = nlinfit(xgrid, margX_s, norm_func, initGuess);
%     coefEstsY_s = nlinfit(xgrid, margY_s, norm_func, initGuess);
%     coefEstsZ_s = nlinfit(xgrid, margZ_s, norm_func, initGuess);
%     
%     %reescalar
%     xgrid = 1e-6*xgrid;
%     
%     coefEstsX(2) = 1e-6*coefEstsX(2);
%     coefEstsX(3) = 1e-12*coefEstsX(3);
%     
%     coefEstsY(2) = 1e-6*coefEstsY(2);
%     coefEstsY(3) = 1e-12*coefEstsY(3);
%     
%     coefEstsZ(2) = 1e-6*coefEstsZ(2);
%     coefEstsZ(3) = 1e-12*coefEstsZ(3);
%     
%     coefEstsI(2) = 1e-6*coefEstsI(2);
%     coefEstsI(3) = 1e-12*coefEstsI(3);
%     coefEstsE(2) = 1e-6*coefEstsE(2);
%     coefEstsE(3) = 1e-12*coefEstsE(3);
%     
%     
%     %--
%     coefEstsX_s(2) = 1e-6*coefEstsX_s(2);
%     coefEstsX_s(3) = 1e-12*coefEstsX_s(3);
%     
%     coefEstsY_s(2) = 1e-6*coefEstsY_s(2);
%     coefEstsY_s(3) = 1e-12*coefEstsY_s(3);
%     
%     coefEstsZ_s(2) = 1e-6*coefEstsZ_s(2);
%     coefEstsZ_s(3) = 1e-12*coefEstsZ_s(3);
    
    

    %----------------------------------------------------------
    %cambiar figura
    set(0,'CurrentFigure',fig1)
    
    %-----------------------------------------------------------
    % plottear suavizado
    %figure
    subplot(2, length(icvfs), currICVF);
    hold on
    title(['Smoothed, f=', gt_icvfLvl{1}, ', L' num2str(lMetric), ' fitted'])
    
    plot(xgrid, margX_s, 'o')
    
    curr_y = norm_func(coefEstsX_s, xgrid);
    plot(xgrid, curr_y, '-')
    
    text(xgrid(130), margX_s(130), strjoin({'\leftarrow \sigma^2 =' num2str(estVarX_s)}))
    
    estV = coefEstsX_s(3) - scaledKernCM(1,1);
    
    varErr_ts(currICVF) = 100* (estV - estVarX)/estVarX;
    varGt_ts(currICVF) = estV;
    varEst_ts(currICVF) = estVarX;
    
    text(xgrid(110), curr_y(110), ...
        {strjoin({'\leftarrow \sigma_{comp}^2 =' num2str(estV)}), ...
        strjoin({' \epsilon = ' num2str( varErr_ts(currICVF) ) '%'})})
    
    %legend({'Marginal X' 'Fitted X'})
    xlabel('Displacement in m')
    hold off
    
    
    %---------------------------------------------------------
    % plottear sin suavizado
    
    %compensar el kernel
    %coefEstsX_c = 1.0*coefEstsX_s;
    %coefEstsX_c(3) = coefEstsX_c(3) - scaledKernCM(1,1);
    
    %figure
    subplot(2, length(icvfs), currICVF+length(icvfs))
    hold on
    title(['Bundle, f=', gt_icvfLvl{1}, ', L' num2str(lMetric), ' fitted'])
    
    plot(xgrid, margX, 'o')
    
    curr_y = norm_func(coefEstsX, xgrid);
    plot(xgrid, curr_y, '-')
    
    text(xgrid(130), margX(130), strjoin({'\leftarrow \sigma^2 =' num2str(estVarX)}))
    
    varErr_tr(currICVF) = 100*(coefEstsX(3) - estVarX)/estVarX;
    varGt_tr(currICVF) = coefEstsX(3);
    varEst_tr(currICVF) = estVarX;
    
    text(xgrid(110), curr_y(110), ...
        {strjoin({'\leftarrow \sigma_{est}^2 =' num2str(coefEstsX(3))}), ...
        strjoin({' \epsilon = ' num2str( varErr_tr(currICVF) ) '%' })})
    
    
    
    %legend({'Marginal X' 'Fitted X'})
    xlabel('Displacement in m')
    hold off
    
    %----------------------------------------------------------
    %cambiar figura
    set(0,'CurrentFigure',fig2)
    
    %---------------------------------------------------------
    % plottear intra
    
    %figure
    subplot(2, length(icvfs), currICVF);
    hold on
    title(['Intra, f=', gt_icvfLvl{1}, ', L' num2str(lMetric), ' fitted'])
    
    plot(xgrid, margI, 'o')
    
    curr_y = norm_func(coefEstsI, xgrid);
    plot(xgrid, curr_y, '-')
    
    text(xgrid(130), margI(130), strjoin({'\leftarrow \sigma^2 =' num2str(estVarI)}))
    
    varErr_i(currICVF) = 100*(coefEstsI(3)-estVarI)/estVarI;
    varGt_i(currICVF) = coefEstsI(3);
    varEst_i(currICVF) = estVarI;
    
    
    %text(xgrid(110), curr_y(110), strjoin({'\leftarrow \sigma_{est}^2 =' num2str(coefEstsI(3))}))
    text(xgrid(110), curr_y(110), ...
        {strjoin({'\leftarrow \sigma_{est}^2 =' num2str(coefEstsI(3))}), ...
        strjoin({' \epsilon = ' num2str( varErr_i(currICVF) ) '%' })})
    
    
    
    %legend({'Marginal X' 'Fitted X'})
    xlabel('Displacement in m')
    hold off
    
        
    %---------------------------------------------------------
    % plottear extra
    
    %figure
    subplot(2, length(icvfs), currICVF+length(icvfs))
    hold on
    title(['Extra, f=', gt_icvfLvl{1}, ', L' num2str(lMetric), ' fitted'])
    
    plot(xgrid, margE, 'o')
    
    curr_y = norm_func(coefEstsE, xgrid);
    plot(xgrid, curr_y, '-')
    
    text(xgrid(130), margE(130), strjoin({'\leftarrow \sigma^2 =' num2str(estVarE)}))
    
    varErr_e(currICVF) = 100*(coefEstsE(3) - estVarE)/estVarE;
    varGt_e(currICVF) = coefEstsE(3);
    varEst_e(currICVF) = estVarE;
    
    %text(xgrid(110), curr_y(110), strjoin({'\leftarrow \sigma_{est}^2 =' num2str(coefEstsE(3))}))
    text(xgrid(110), curr_y(110), ...
        {strjoin({'\leftarrow \sigma_{est}^2 =' num2str(coefEstsE(3))}), ...
        strjoin({' \epsilon = ' num2str( varErr_e(currICVF) ) '%' })})
    
    %legend({'Marginal X' 'Fitted X'})
    xlabel('Displacement in m')
    hold off
    
    currICVF = currICVF+1;
end
%% generar tablas
results = table(varErr_tr', varErr_ts', varErr_i', varErr_e', ...
    'VariableNames',{'TotalRaw', 'TotalSmoothed', 'Intra', 'Extra'}, ...
    'RowNames', icvfLvls')
writetable(results, '../results/1d_hist/zeroCrossHistFit.csv', 'WriteRowNames',true)

compResults1 = table(varGt_ts', varGt_ts', varGt_i', varGt_e', ...
    'VariableNames',{'TotalRaw', 'TotalSmoothed', 'Intra', 'Extra'}, ...
    'RowNames', icvfLvls') 
writetable(compResults1, '../results/1d_hist/groundTruth.csv', 'WriteRowNames',true)

compResults2 = table(varEst_ts', varEst_ts', varEst_i', varEst_e', ...
    'VariableNames',{'TotalRaw', 'TotalSmoothed', 'Intra', 'Extra'}, ...
    'RowNames', icvfLvls') 
writetable(compResults2, '../results/1d_hist/predicted.csv', 'WriteRowNames',true)

%% imprimir figuras a eps

set(0,'CurrentFigure',fig1)
print -depsc ../results/1d_hist/Fit1d_rs
saveas(fig1, '../results/1d_hist/Fit1d_rs.svg')

set(0,'CurrentFigure',fig2)
print -depsc ../results/1d_hist/Fit1d_ie
saveas(fig2, '../results/1d_hist/Fit1d_ie.svg')



