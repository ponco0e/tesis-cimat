function [salto, vecs2]=compVecinos(salto,ldato,idxs,lDB)
    
    newDim = salto;
    salto = newDim/ldato;
    %
    vecPos = idxs + salto;
    bIdx = floor((idxs-1) ./ newDim)+1;
    bVecPos = floor((vecPos-1) ./ newDim)+1;
    vecs2(:,1) = vecPos;  vecs2(bIdx~=bVecPos,1) = 0 ; vecs2(vecs2(:,1)>lDB,1) = 0;
    
    vecPos = idxs - salto;
    bVecPos = floor((vecPos-1) ./ newDim)+1;
    vecs2(:,2) = vecPos; vecs2(bIdx~=bVecPos,2) = 0 ; vecs2(vecs2(:,2)<1              ,2) = 0;
    
end
