function hist3DAnaly = computeHistAnaly3D(DT1,DT2,alpha1,nBins,scalePerBin)

%Con función gauss3D_grid:  1.679172 seconds
%Con mvnpdf:                7.434364 seconds

mrangeXYZ = gpuArray(   ((-double(nBins)+0.5)/scalePerBin):(1/scalePerBin):(double(nBins)-0.5)/scalePerBin  )*1e-6;

[Y, X, Z] = meshgrid(mrangeXYZ,mrangeXYZ,mrangeXYZ);

hist3DAnaly = MMG_grid(DT1,DT2,alpha1,X,Y,Z);

hist3DAnaly = hist3DAnaly / sum(hist3DAnaly(:));


end
