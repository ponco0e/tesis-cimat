function e = computeErrorHistsProyecciones(histAnaly,histsMC,T1,T2,alpha,nBins,scalePerBin)


    e3D=KLdiv(histAnaly,histsMC);

    
    mrangeX = ((0.5/scalePerBin):(1/scalePerBin):double(nBins)/scalePerBin);   
    mrangeYZ = (  ((-double(nBins)+0.5)/scalePerBin):(1/scalePerBin):(double(nBins)-0.5)/scalePerBin    );   
    [Y, X, Z] = meshgrid(mrangeYZ,mrangeX,mrangeYZ);

    % dimensiones histograma 1D
    minXH = -50;
    maxXH =  50;

    
    [ps,~] = eigs(T1);
    p1 = ps(:,3);
    
    scalePerBin1D = 1.0; %Arturo ten??a 4
    pp1_v = pp_v_Grid(p1,X,Y,Z,scalePerBin1D);
    HAnaly_v = histAnaly(:);
    HMC_v    = histsMC(:);
    


    [hAnaly_1_vals,centers] =  hist1d_fromProy(pp1_v,HAnaly_v,minXH,maxXH,scalePerBin1D);
    [hMC_1_vals,~] =  hist1d_fromProy(pp1_v,HMC_v,minXH,maxXH,scalePerBin1D);
    err_p1=KLdiv(hAnaly_1_vals,hMC_1_vals);
    
    if alpha<1
        [ps,~] = eigs(T2);
        p2 = ps(:,3);
        pp2_v = pp_v_Grid(p2,X,Y,Z,scalePerBin1D);
        [hAnaly_2_vals,~] =  hist1d_fromProy(pp2_v,HAnaly_v,minXH,maxXH,scalePerBin1D);    
        [hMC_2_vals,~] =  hist1d_fromProy(pp2_v,HMC_v,minXH,maxXH,scalePerBin1D);
        err_p2=KLdiv(hAnaly_2_vals,hMC_2_vals);
    else
        err_p2 = 0;
    end
    
   
    figure;  plot(centers,[hAnaly_1_vals,hAnaly_2_vals,hMC_1_vals,hMC_2_vals]); legend('h1-p1','h1-p2','h2-p1','h2-p2');
    
    e = e3D + err_p1 + err_p2;
end
