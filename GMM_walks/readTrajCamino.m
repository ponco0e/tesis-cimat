function [xs, N, T] = readTrajCamino(fPrefix, nChunks)

    bytesPerPoint = int64(8 + 4 + 8*3);
    bytesPerPHeader = int64(8 + 4);
    bytesPerHeader = int64(8*3);
    
    %machinePath = '~/devel/cimat/tesis-cimat/';
    %dataPath = 'recorrersimuladormc/testCamino/';
    %filename = 'cross_90';
    
    xs = [];
    
    for chunk = 1:nChunks %por cada proceso
        %fp = fopen([machinePath dataPath filename '_' num2str(chunk) '.traj'],'r');
        fp = fopen(strjoin([fPrefix '_' num2str(chunk-1) '.traj'],""),'r');
        
        duration = fread(fp,1,'double','ieee-be');
        nSpins = fread(fp,1,'double','ieee-be');
        N = nSpins;
        T = fread(fp,1,'double','ieee-be');
        
        trajIni = zeros(nSpins,3);
        trajFin = zeros(nSpins,3);
        
        
        %idxTime =1;
        for step = 1:2 % para cada paso desde el inicial
            for k=1:nSpins % para cada caminante
                time = fread(fp,1,'double','ieee-be');
                sIndex = fread(fp,1,'int','ieee-be');
                
                
                %if sIndex == 0
                %display([idxTime sIndex time]);
                %pause;
                %end
                
                coord = fread(fp,3,'double' ,'ieee-be');
                
                if step == 1
                    trajIni(k,:) = coord;
                end
                if step == 2
                    trajFin(k,:) = coord;
                end
            end
            rv = fseek(fp, uint64(T*nSpins*bytesPerPoint + bytesPerHeader), 'bof');
            %idxTime = idxTime + 1;
            
        end
        
        fclose(fp);
        xsp = 1e6*(trajFin - trajIni);
        xs = vertcat(xs, xsp);
        %graficar algunas trayectorias
        %figure; hold on;
        %for k=1:nSpins
        %    plot3(squeeze(traj(:,k,1)),squeeze(traj(:,k,2)),squeeze(traj(:,k,3)));
        %end
        %hold off
    end
   
end
