function v = pp_v_Grid(v,GX,GY,GZ,scalePerBin1D)
    v = v(1)*GX + v(2)*GY + v(3)*GZ;
    v = round(v(:)*scalePerBin1D)/scalePerBin1D;
end
