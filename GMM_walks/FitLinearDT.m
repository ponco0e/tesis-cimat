function [D, Ep ] = FitLinearDT(E, protocol, fitS0)
% Fits the DT model using linear least squares.
%
% D=FitLinearDT(E, protocol, fitS0)
% returns [logS(0) Dxx Dxy Dxz Dyy Dyz Dzz].
%
% E is the set of measurements.
%
% protocol is the acquisition protocol.
%
%
% fitS0 is a flag for enabling the fitting of S0
%
% author: Daniel C Alexander (d.alexander@ucl.ac.uk)
%         Gary Hui Zhang     (gary.zhang@ucl.ac.uk)
%

if (nargin < 3)
	fitS0 = 0;
end

if (isfield(protocol, 'dti_subset'))
    protocol.delta = protocol.delta(protocol.dti_subset);
    protocol.smalldel = protocol.smalldel(protocol.dti_subset);
    protocol.G = protocol.G(protocol.dti_subset);
    protocol.grad_dirs = protocol.grad_dirs(protocol.dti_subset,:);
    protocol.b0_Indices = intersect(protocol.b0_Indices, protocol.dti_subset);
    S0 = squeeze(mean(E(protocol.b0_Indices)));
    %S0 = squeeze(median(E(protocol.b0_Indices)));
    E = E(protocol.dti_subset);
    E = E/S0;
end

X = DT_DesignMatrix(protocol);
if fitS0 == 0
  X = X(:,2:end);
end
Xi = pinv(X);

% We assume that E are all positives
% If not, first filter out the nonpositive values with RemoveNegMeas

if (fitS0)
	D = Xi*log(E);
    if (nargout > 1)
        Ep = exp(X*D);
    end
else
	D = zeros(7,1);
    if isfield(protocol, 'TE') && ~isfield(protocol,'dti_subset')
        S0_per_TE = compute_S0_per_TE(protocol,E);
        
        outliersDW = [];
        for i=1:length(protocol.uTE)
            idxB0 = intersect ( find(protocol.TE == protocol.uTE(i)),protocol.b0_Indices);
            S0_TE = mean(E(idxB0));
            %S0_TE = median(E(idxB0));
            %fprintf('\n%f',S0_TE);
            idxDW = find(protocol.TE == protocol.uTE(i));
            
            
            %outlier detection %%%%%%%%%%%%%%%%
            idxDWHigh = setdiff(idxDW,protocol.b0_Indices);
            idxDWHigh = intersect(find(E > S0_TE),idxDWHigh);
            
            if ~isempty(idxDWHigh) > 0
                disp(['S0= ' num2str(S0_TE) ' TE= ' num2str(protocol.uTE(i))])
                disp('idxFile val G DELTA delta b');
                disp([idxDWHigh+1 E(idxDWHigh) 1000*protocol.G(idxDWHigh)' 1000*protocol.delta(idxDWHigh)' 1000*protocol.smalldel(idxDWHigh)' floor(1e-6*protocol.B(idxDWHigh)')]);
                outliersDW = [outliersDW ; idxDWHigh+1];
            end
            %%%%%%%%%%%%%%%%%%%%
            
            E(idxDW) =  E(idxDW) / S0_TE;
            S0_per_TE(idxDW) = S0_TE;
            
            
        end
        display('list of outliers');
        
        if ~isempty(outliersDW)
            fprintf('[');
            fprintf('%d, ',outliersDW(1:end-1));
            fprintf('%d]',outliersDW(end));
        end

        S0 = squeeze(mean(E(protocol.b0_Indices))); % dummy
        %S0 = squeeze(median(E(protocol.b0_Indices))); % dummy
    elseif ~isfield(protocol,'dti_subset')
        S0 = squeeze(mean(E(protocol.b0_Indices)));
        %S0 = squeeze(median(E(protocol.b0_Indices)));
        E = E/S0;
    end
    
	D(1) = log(S0);
	D(2:end) = Xi*log(E);
    if (nargout > 1)
        if isfield(protocol, 'TE') && ~isfield(protocol,'dti_subset')
            Ep = S0_per_TE .* exp(X*D(2:end));
        else
            Ep = S0*exp(X*D(2:end));            
        end
        
    end
end

