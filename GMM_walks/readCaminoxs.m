function [ xs, N, T ] = readCaminoxs( fPath )
%readCaminoxs Lee archivos de desplazamiento extraidos por el script en C
%   by ponco
fp = fopen(fPath,'r');

N = fread(fp,1,'double');
T = fread(fp,1,'double');

xs = fread(fp, uint64(N)*3, 'double');
xs = reshape(xs, [3, uint64(N)]);

xs = xs';

end

