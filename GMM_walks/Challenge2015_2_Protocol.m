function protocol = Challenge2015_2_Protocol(bval, bvec,G,delta,DELTA,TE)
%
% function protocol = Challenge2015_2_Protocol(bvalfile, bvecfile)
%
% Note: for NODDI, the exact sequence timing is not important.
%  this function reverse-engineerings one possible sequence timing
%  given the b-values.
%
% author: Gary Hui Zhang (gary.zhang@ucl.ac.uk)
%

protocol.pulseseq = 'PGSE';
protocol.schemetype = 'multishell';
protocol.teststrategy = 'none';


% set total number of measurements
protocol.totalmeas = length(bval);

% set the b=0 indices
protocol.b0_Indices = find(bval==0);
protocol.dw_Indices = find(bval~=0);
protocol.numZeros = length(protocol.b0_Indices);

% find the unique non-zero b-values
B = unique(bval(bval>0));

% set the number of shells
protocol.M = length(B);
for i=1:length(B)
    protocol.N(i) = length(find(bval==B(i)));
end

% maximum b-value in the s/mm^2 unit
%maxB = max(B);

% set maximum G = 40 mT/m
%Gmax = 0.04;

% set smalldel and delta and G
%GAMMA = 2.675987E8;
%tmp = nthroot(3*maxB*10^6/(2*GAMMA^2*Gmax^2),3);

protocol.uB = B'; %yet in SI units
for i=1:length(B)
    idx = find(bval==B(i));
    
    protocol.udelta(i) = DELTA(idx(1));
    protocol.usmalldel(i) = delta(idx(1));
    protocol.uG(i) = G(idx(1));    
    
end

protocol.uTE = unique(TE)';
if protocol.uTE(1) == 0
    protocol.uTE = protocol.uTE(2:end);
end

protocol.delta = DELTA';
protocol.smalldel = delta';
protocol.G = G';
protocol.TE = TE';
protocol.B = bval';  %yet in SI units


protocol.grad_dirs = bvec;

% make the gradient directions for b=0's [1 0 0]
for i=1:length(protocol.b0_Indices)
    protocol.grad_dirs(protocol.b0_Indices(i),:) = [1 0 0];
end

% make sure the gradient directions are unit vectors
for i=1:protocol.totalmeas
    protocol.grad_dirs(i,:) = protocol.grad_dirs(i,:)/norm(protocol.grad_dirs(i,:));
end
% 
% %idx_B_1000 = find( abs(bval - 51.010213e6) < 1); % RESNORM_dt =       1,038,985.69068445
% %idx_B_1000 = find( abs(bval - 650.509037162678e6) < 1); % RESNORM_dt =  853,447.610260052
%%%%% %idx_B_1000 = find( abs(bval - 1004.74663165721e6) < 1);  % RESNORM_dt = 712,293.985887534
% idx_B_1000 = find( abs(bval - 1317.94056479237e6) < 1); % RESNORM_dt =  571,998.43743235 quiza usar este
% %idx_B_1000 = find( abs(bval - 1604.84658567606e6) < 1); % RESNORM_dt =  590,408.513702202 este fluctua, verlo 
% %idx_B_1000 = find( abs(bval - 1709.88220696079e6) < 1); % RESNORM_dt =  596,774.633487391 
% %idx_B_1000 = find( abs(bval - 1951.09773258562e6) < 1); % RESNORM_dt =  710,382.605552778 
% %idx_B_1000 = find( abs(bval - 4457.90718243349e6) < 1); % RESNORM_dt= 2,306,180.9852966
% %idx_B_1000 = find( abs(bval - 21495.6620303642e6) < 1); % RESNORM_dt=15,330,311.7667996
% 
% if ~isempty(idx_B_1000)
%     TE_B_1000 = unique(protocol.TE(idx_B_1000));
%     idx_B0_B_1000 = intersect ( find(protocol.TE == TE_B_1000) , protocol.b0_Indices);
%  
%      protocol.dti_subset = [idx_B0_B_1000 ; idx_B_1000];
% end

end
