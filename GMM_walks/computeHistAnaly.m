function histAnaly = computeHistAnaly(DT1,DT2,alpha1,nBins,gs)
    for k=1:length(gs)    
        idx = 1;
        histAnaly(k,idx) = MMG(DT1,DT2,alpha1,[0;0;0]); 
        for x=1.5e-6:1e-6:double(nBins)*1e-6
            idx = idx + 1;
            vecX = x*gs(:,k);
            histAnaly(k,idx) = MMG(DT1,DT2,alpha1,vecX);
        end
        histAnaly(k,:) = histAnaly(k,:) / sum(histAnaly(k,:));

    end

end
