function eigenVals = getEigenVals(D)

        T = MakeDT_Matrix(D(2), D(3), D(4), D(5), D(6), D(7));
        
        eigenVals = sort(eig(T),1,'descend');
        
end