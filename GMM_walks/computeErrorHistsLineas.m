function e = computeErrorHistsLineas(histAnaly,histsMC,T1,T2,alpha,nBins,scalePerBin)

    %e3D=KLdiv(histAnaly,histsMC);

    scalePerBin1D = 1.0;
    
    
    mrangeXYZ = (  ((-double(nBins)+0.5)/scalePerBin):(1/scalePerBin):(double(nBins)-0.5)/scalePerBin    );   
    [Y, X, Z] = meshgrid(mrangeXYZ,mrangeXYZ,mrangeXYZ);

        
    % dimensiones histograma 1D
    minXH = -100; %Arturo 100
    maxXH =  100;

    
    [ps,~] = eigs(T1);
    p1 = ps(:,3); % antes 3
    
    
    distanciaMin = 20;
    [i_idx,j_idx,k_idx] = distanciaRectaVec(X,Y,Z,p1,distanciaMin);
    
    

    
    [hAnaly_1_vals,centers] =  hist1d_fromLinea(X,Y,Z,i_idx,j_idx,k_idx,histAnaly,p1,minXH,maxXH,scalePerBin1D);
    [hMC_1_vals,~] =  hist1d_fromLinea(X,Y,Z,i_idx,j_idx,k_idx,histsMC,p1,minXH,maxXH,scalePerBin1D);
    err_p1=KLdiv(hAnaly_1_vals,hMC_1_vals);

    
    if alpha<1
        [ps,~] = eigs(T2);
        p2 = ps(:,3); %antes 3
        
        [i_idx,j_idx,k_idx] = distanciaRectaVec(X,Y,Z,p2,distanciaMin);
        
        [hAnaly_2_vals,~] =  hist1d_fromLinea(X,Y,Z,i_idx,j_idx,k_idx,histAnaly,p2,minXH,maxXH,scalePerBin1D);
        [hMC_2_vals,~] =  hist1d_fromLinea(X,Y,Z,i_idx,j_idx,k_idx,histsMC,p2,minXH,maxXH,scalePerBin1D);
        err_p2=KLdiv(hAnaly_2_vals,hMC_2_vals);
        
        
%         p3 = cross(p1,p2);
%         [i_idx,j_idx,k_idx] = distanciaRectaVec(X,Y,Z,p3,distanciaMin);
%         
%         [hAnaly_3_vals,~] =  hist1d_fromLinea(X,Y,Z,i_idx,j_idx,k_idx,histAnaly,p3,minXH,maxXH,scalePerBin1D);
%         [hMC_3_vals,~] =  hist1d_fromLinea(X,Y,Z,i_idx,j_idx,k_idx,histsMC,p3,minXH,maxXH,scalePerBin1D);
%         err_p3=KLdiv(hAnaly_3_vals,hMC_3_vals);

        
    else
        err_p2 = 0;
        %err_p3 = 0;

    end
    
   
    %figure;  plot(centers,[hAnaly_1_vals,hAnaly_2_vals,hAnaly_3_vals,hMC_1_vals,hMC_2_vals,hMC_3_vals]); legend('h1-p1','h1-p2','h1-p3','h2-p1','h2-p2','h2-p3');
    %figure;  plot(centers,[hAnaly_1_vals,hAnaly_2_vals,hMC_1_vals,hMC_2_vals]); legend('h1-p1','h1-p2','h2-p1','h2-p2');
    
    e =  err_p1 + err_p2;
    %e = e3D + err_p3;
end
