function [l1_1,l1_23,h1,l2_1,l2_23,h2] = fitGMM_2_Ds_priori(Ds,gs)

    nMeas = length(Ds);
    h1 = [0;0;1];
    h2 = [1;0;0];
    
%     h1 = rand(3,1); h1 = h1 / norm(h1); 
%     h1 = h1 .* ((randi(2,3,1)-1)*2-1);
%     h2 = rand(3,1); h2 = h2 / norm(h2); 
%     h2 = h2 .* ((randi(2,3,1)-1)*2-1);
    
    alpha1 = 0.75;
    alpha2 = 1-alpha1;
    
    for i=1:nMeas
        ppsqrt1 = (gs(i,:)*h1)^2;
        ppsqrt2 = (gs(i,:)*h2)^2;

        MatLS(i,1) = alpha1 * ppsqrt1;
        MatLS(i,2) = alpha1 * (1 - ppsqrt1);  
        MatLS(i,3) = alpha2 * ppsqrt2;
        MatLS(i,4) = alpha2 * (1 - ppsqrt2);  

%         MatLS(i,1) = alpha1 * ppsqrt1 + alpha2 * ppsqrt2; % axiales
%         MatLS(i,2) = alpha1 * (1 - ppsqrt1) ; %radial 1  
%         MatLS(i,3) = alpha2 * (1 - ppsqrt2) ; %radial 2
        
%         MatLS(i,1) = alpha1 * ppsqrt1 + alpha2 * ppsqrt2; % axiales
%         MatLS(i,2) = alpha1 * (1 - ppsqrt1) + alpha2 * (1 - ppsqrt2) ; %radial 2
        
    end
    
    sol = MatLS\Ds;
    sol = lsqnonneg(MatLS,Ds);
    sol = inv(MatLS'*MatLS+1e-5*eye(4))*MatLS'*Ds
    
    figure, plot(Ds,'r')   , hold on; 
    plot(MatLS*sol,'b+')   
    
    l1_1 = sol(1);
    l1_23 = sol(2); 
    l2_1 = sol(3);
    l2_23 = sol(4); 
%     l2_1 = sol(3);
%     l2_23 = sol(4); 
    
    %T1 = sol(1)*(h1')*h1 + sol(2)*(h2')*h2 + sol(2)*(h3')*h3;
    
end
