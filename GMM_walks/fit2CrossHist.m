function [coeffs, distances] = fit2CrossHist(xs, FGs, s, bins, scalepBin, maxTime)
%by ponco

%kernel de suavizado
GKern = gpuArray(FGs(:,:,:,s));

%generar histogramas
hist3D_sim = computeHistogram3DFromSamples(xs,bins,scalepBin);

%suavizar
hist3D_comb_s = SmoothHistogram(hist3D_sim, GKern);
%plotHist3D(hist3D_comb_s,1,'From simulator (smoothed)');


%coeficientes a barrer
diff_rotang = 10.0 : 5.0 : 90.0;
%para in vivo
diff_ort = 1e-9*(0.01 : 0.1 : 2.1);
diff_par = 1e-9*(1.6 : 0.1 : 2.5);
%para ex vivo
%diff_ort = 0.2857*1e-9*(0.1 : 0.1 : 2.1);
%diff_par = 0.2857*1e-9*(1.6 : 0.1 : 2.5);

%angulo, coeficientes del T1, coeficientes del T2
distances = gpuArray(zeros(...
    length(diff_rotang), ... %angulo
    length(diff_ort), ... %coeficiente ortogonal compartido
    length(diff_par), length(diff_par)  ... %coeficientes paralelos (T1, T2)
    ));



%p = parpool(2);
%par
for i = 1 : length(diff_rotang)  %por cada angulo (T2)
    rotAng = diff_rotang(i);
    %crear subtensor para cada proceso
    subdistances = gpuArray(zeros(length(diff_ort), ...
    length(diff_par), length(diff_par)));
    j = 1;
    for c_ort = diff_ort %por cada coeff ortogonal compartido
        k = 1;
        for c_par1 = diff_par  %por cada coeff paralelo (T1)
            l = 1;
            for c_par2 = diff_par %por cada coeff paralelo (T2)
                %generar tensores
                T1 =  MakeDT_Matrix(c_ort, 0, 0, c_ort, 0, c_par1);
                T2 =  MakeDT_Matrix(c_ort, 0, 0, c_ort, 0, c_par2);
                    
                %rotar T2
                rotang_r = deg2rad(rotAng); %a radianes
                orient2 = [-sin(rotang_r); 0 ; cos(rotang_r)];
                R = fromToRotation([0; 0; 1], orient2);
                T2 = R*T2*R';
                    
                %xs_gauss = mvnrnd([0;0;0],T_samp*2*maxTime,N);
                %xs_gauss = xs_gauss * 1e6; %el muestreo los genera en m, se converten a um
                %hist3D_samples_g = computeHistogram3DFromSamples(xs_gauss,nBins,scalePerBin);
                    
                %generar histograma analitico suavizado
                hist3D_an = gpuArray(computeHistAnaly3D(T1*2*maxTime,T2*2*maxTime,0.5,bins,scalepBin));
                hist3D_an_s = SmoothHistogram(hist3D_an, GKern);
                    
                %comparar
                subdistances(j,k,l) = computeErrorHists3DL1(hist3D_comb_s, hist3D_an_s);
                
                l = l+1;
            end
            k = k+1;
        end
        j = j+1;
    end
    distances(i,:,:,:) = subdistances;
end


%conseguir minimo
[minL1, flatIdx] = min(distances(:));
%minL1
%distances(flatIdx)

%obtener del indice flettened los coeficientes de las tablas de busqueda

[minRotangIdx, minOrtIdx, minParIdx1, minParIdx2] = ind2sub(size(distances),flatIdx);
%distances(minOrtIdx, minParIdx, minRotangIdx)

rotang_min = diff_rotang(minRotangIdx);
coef_ort_min = diff_ort(minOrtIdx); 
coef_par_min1 = diff_par(minParIdx1);
coef_par_min2 = diff_par(minParIdx2);


disp([ 'min L1: ', num2str(minL1), ...
    ', RotAng: ', num2str(rotang_min), '°', ...
    ', C_ort: ', num2str(coef_ort_min), ...
    ', C_par1: ', num2str(coef_par_min1), ...
    ', C_par2: ', num2str(coef_par_min2)
    ])


coeffs = [minRotangIdx, minOrtIdx, minParIdx1, minParIdx2];

