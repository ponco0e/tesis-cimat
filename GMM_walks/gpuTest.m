clc , clear, close all

maxTime = 0.072; %max(TE); %

coeff_diff = 2e-9;
%process = 1:16;

rng(100);

[params, DB, vecindades, nRotAlea, RotAlea] = createDB_single_Crossing();

inspect = 1;
params = params(inspect,:);
DB = DB(inspect,:);


% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
% parametros del los histogramas
scalePerBin = 3; % escala que define el tamanio de los hisotgramas
display(scalePerBin)

maxAvgDisplacement = sqrt(2*coeff_diff*maxTime) * 1e6; % desplazamisnt maximo en um
nBins = int32(maxAvgDisplacement*8);

tamHist = double([2*nBins,2*nBins,2*nBins])
sigmas = 6.6; % sigmas de suavizado
display(sigmas)

%%

% crear kernels de suavizado
FGs = zeros([2*nBins, 2*nBins, 2*nBins, length(sigmas)]);
sepFGs = zeros([length(sigmas), 3, 2*nBins]);
for s=1:length(sigmas)
    [FGs(:,:,:,s), sepFGs(s, :, :)] = makeKernel(tamHist,sigmas(s),nBins);
end


%crear histograma dummy
hist3D_sim = ones([2*nBins, 2*nBins, 2*nBins]); %dummy

disp('SmoothHistogram con CPU')
tic
SmoothHistogram( hist3D_sim, FGs(:,:,:,s) );
toc


disp('SmoothHistogram con CUDA')
hist3D_sim_gpu = gpuArray(hist3D_sim);
kern_gpu = gpuArray(FGs(:,:,:,s));
tic
SmoothHistogram( hist3D_sim_gpu, kern_gpu );
toc


disp('convnsep con CPU')
h = {squeeze(sepFGs(1, 1 , :)), ... 
    squeeze(sepFGs(1, 2 , :)), ...
    squeeze(sepFGs(1, 3 , :))};
tic
convnsep(h, hist3D_sim, 'same');
toc


disp('convnsep con CUDA')
h_gpu = {gpuArray(squeeze(sepFGs(1, 1 , :))), ... 
    gpuArray(squeeze(sepFGs(1, 2 , :))), ...
    gpuArray(squeeze(sepFGs(1, 3 , :)))};
tic
convnsep(h_gpu, hist3D_sim_gpu, 'same');
toc


disp('imwarp con CPU')     
tic
theta = pi/8;
t = [cos(theta)  0      -sin(theta)   0
    0             1              0     0
    sin(theta)    0       cos(theta)   0
    0             0              0     1];
tform = affine3d(t);
0.5*imwarp(hist3D_sim,tform,'OutputView',imref3d(size(hist3D_sim))) + 0.5*hist3D_sim; %simula mezcla de histogramas suavizados
toc

%esto no se puede
disp('imwarp con CUDA')
     
tic
theta = pi/8;
t = [cos(theta)  0      -sin(theta)   0
    0             1              0     0
    sin(theta)    0       cos(theta)   0
    0             0              0     1];
tform = affine3d(t);
0.5*imwarp(hist3D_sim_gpu,tform,'OutputView',imref3d(size(hist3D_sim_gpu))) + 0.5*hist3D_sim_gpu; %simula mezcla de histogramas suavizados
toc

