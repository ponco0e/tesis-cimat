function dqm = accumulateDQM(dqm,g,posFinCentered)
    posFinCentered = posFinCentered * 1e-3; % en m
    pos = abs((g')*posFinCentered);

    dqm = dqm + sum(pos.^2);
end
