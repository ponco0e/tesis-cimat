function S = readDWSignal(idxSubstrate,strSubstrates,generic_names_data,protocol,my_path)

    S = zeros(protocol.totalmeas,1);
    for nSub = idxSubstrate
        strSubstrate = strSubstrates(nSub,:);
        generic_name_data = generic_names_data(nSub,:);
        
        my_file = [generic_name_data strSubstrate '_DWI.txt'];
        fprintf('\n%s',my_file)
        S = S + load([my_path 'outputs/' my_file]);
                
    end
    
    idxPlot = 2:66;
    
    gs =  protocol.grad_dirs(idxPlot,1:3)';

    gSc =[];
    gSc(1,:) = gs(1,:) .* S(idxPlot)';
    gSc(2,:) = gs(2,:) .* S(idxPlot)';
    gSc(3,:) = gs(3,:) .* S(idxPlot)';
    
    %figure; plot3([gSc(1,:),-gSc(1,:)] ,[gSc(2,:),-gSc(2,:)],[gSc(3,:),-gSc(3,:)],'+'); axis equal;
    figure; showPointCloud([gSc';-gSc'],'MarkerSize',20);
    xlabel('X (m)');
    ylabel('Y (m)');
    zlabel('Z (m)');
    
end
