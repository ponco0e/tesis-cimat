function mtx = fromToRotation(from,to)
% mtx = fromToRotation(from,to)
% Generates the mtx rotation matrix for going from FROM to TO 
%
%once the result is given for operate use Mr = mtx * M * mtx' for matrixes
%or
%use Vr = mtx * V for vectors

EPSILON = 0.000001;

mtx = zeros(3,3);
v = zeros(3,1);


v = cross( from, to);


e = from' * to;

f = abs(e);

if (f > 1.0 - EPSILON)     % "from" and "to"-vector almost parallel
    
    u= zeros(3,1); v= zeros(3,1); %/* temporary storage vectors */
    
    x = abs(from);
    
    if (x(1) < x(2))
        
        if (x(1) < x(3))
            
            x(1) = 1.0; x(2) = 0.0; x(3) = 0.0;
            
        else
            
            x(3) = 1.0; x(1) = 0.0; x(2) = 0.0;
        end
        
    else
        
        if (x(2) < x(3))
            
            x(2) = 1.0; x(1) = 0.0; x(3) = 0.0;
            
        else
            
            x(3) = 1.0; x(1) = 0.0; x(2) = 0.0;
        end
    end
    
    u(1) = x(1) - from(1); u(2) = x(2) - from(2); u(3) = x(3) - from(3);
    v(1) = x(1) - to(1);   v(2) = x(2) - to(2);   v(3) = x(3) - to(3);
    
    c1 = 2.0 / dot(u, u);
    c2 = 2.0 / dot(v, v);
    c3 = c1 * c2  * dot(u, v);
    
    for i = 1:3  
        for j = 1:3  
            mtx(i,j) =  - c1 * u(i) * u(j) - c2 * v(i) * v(j) + c3 * v(i) * u(j);
        end
        mtx(i,i) = mtx(i,i) + 1.0;
    end
    
else  %/* the most common case, unless "from"="to", or "from"=-"to" */
    
    % ...otherwise use this hand optimized version (9 mults less) */
    %float hvx, hvz, hvxy, hvxz, hvyz;
    h = (1.0 - e)/dot(v, v);
    hvx = h * v(1);
    hvz = h * v(3);
    hvxy = hvx * v(2);
    hvxz = hvx * v(3);
    hvyz = hvz * v(2);
    mtx(1,1) = e + hvx * v(1);
    mtx(1,2) = hvxy - v(3);
    mtx(1,3) = hvxz + v(2);
    
    mtx(2,1) = hvxy + v(3);
    mtx(2,2) = e + h * v(2) * v(2);
    mtx(2,3) = hvyz - v(1);
    
    mtx(3,1) = hvxz - v(2);
    mtx(3,2) = hvyz + v(1);
    mtx(3,3) = e + hvz * v(3);
end


