function [hist3D,nBins] = computeHistogram3DFromSamples(samples,nBins,scalePerBin)

 
  
    hist3D = zeros(2*nBins,2*nBins,2*nBins);
    
    nSamples = length(samples);
    
    fuera = 0;
    
    for k=1:nSamples
        posFinCentered = samples(k,:)*scalePerBin; %*1e6; % en um, * 4 hace que cada bin sea de 0.25 um
        %del lector de archivos de montecarlo ya los convierte a um

        
%         if posFinCentered(1)<0 % trabajar son en el semiespacion x>0
%             posFinCentered = -posFinCentered;
%         end

        idx3D = floor(posFinCentered) + 1 ; %sumar [1,1,1] por logica 1
        
        idx3D(1) = idx3D(1) + nBins;
        idx3D(2) = idx3D(2) + nBins;
        idx3D(3) = idx3D(3) + nBins;

        
        if idx3D(1) <= 2*nBins && idx3D(2) <= 2*nBins && idx3D(3) <= 2*nBins && sum(double(idx3D <= 0)) == 0
            hist3D(idx3D(1),idx3D(2),idx3D(3)) = hist3D(idx3D(1),idx3D(2),idx3D(3)) + 1; % contar
        else
            fuera = fuera + 1;
            %display('fuera de bines en data samples !!!')
        end
        
        
        
%         idx3D = floor(abs(posFinCentered)) + 1; %sumar [1,1,1] por logica 1
%         if max(idx3D) <= nBins
%             hist3D(idx3D(1),idx3D(2),idx3D(3)) = hist3D(idx3D(1),idx3D(2),idx3D(3)) + 1; % contar
%         end
        
    end
    
    display(['fuera de bines en data samples = ' num2str(fuera) ' (' num2str(100*fuera/nSamples) '%)'])
    
    hist3D = hist3D /sum(hist3D(:));
    
    
end
