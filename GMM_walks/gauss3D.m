function v = gauss3D(DT,x)
    v = (1/( ((2*pi)^(3/2))*(det(DT)^(1/2)))) * exp(-0.5*(x'*inv(DT)*x));
end


