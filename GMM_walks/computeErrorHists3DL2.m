function e = computeErrorHists3DL2(histsMC,histAnaly)
    [dx,dy,dz] = size(histsMC);
    e = (1/(dx*dy*dz))*sum((histsMC(:) - histAnaly(:)).^2);
    
end
