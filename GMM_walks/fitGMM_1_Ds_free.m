function T1 = fitGMM_1_Ds_free(Ds,gs)

    nMeas = length(Ds);
    
    for i=1:nMeas
        MatLS(i,1) = gs(i,1) ^2;
        MatLS(i,2) = gs(i,2) ^2;
        MatLS(i,3) = gs(i,3) ^2;
        MatLS(i,4) = 2*gs(i,1)*gs(i,2);
        MatLS(i,5) = 2*gs(i,1)*gs(i,3);
        MatLS(i,6) = 2*gs(i,2)*gs(i,3);
    end
    sol = MatLS\Ds;
    
    figure, plot(Ds,'r')   , hold on; 
    plot(MatLS*sol,'b+')    
    T1 = [sol(1)   sol(4)   sol(5);   sol(4)   sol(2)   sol(6);   sol(5)   sol(6)   sol(3)];
    
end
