function pts3D = cargaPnts3Dsample(rads)

    mpath = '/Users/alram/Desktop/desktop_files/temp/backup_source/PointSets/';
    
    
    pts3D = [0;0;0];
    
    d = load([mpath 'orientations10.txt'])*rads(1); 
    pts3D = [pts3D , d'];
    
    d = load([mpath 'orientations20.txt'])*rads(2);
    pts3D = [pts3D , d'];
    
    d = load([mpath 'orientations30.txt'])*rads(3);
    pts3D = [pts3D , d'];

    d = load([mpath 'orientations40.txt'])*rads(4);
    pts3D = [pts3D , d'];
    
    d = load([mpath 'orientations50.txt'])*rads(5);
    pts3D = [pts3D , d'];
    
    d = load([mpath 'orientations60.txt'])*rads(6);
    pts3D = [pts3D , d'];

    idx = pts3D(1,:)<0; pts3D( :,idx ) = - pts3D(:,idx);
    
    figure; plot3(pts3D(1,:),pts3D(2,:),pts3D(3,:),'+'); axis equal;
    
end
