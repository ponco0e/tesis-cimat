%function fit_GMM_walks

clc , clear, close all

%     path_maquina = '/Users/alram/';
%     my_scheme_file = '2Shells_human_1_5k_2_5k_scheme_64o.txt';
%     name_experiment = 'analysis_traj';
%     voxel = 'analytic';
%     experiment_dir_Str = [name_experiment  '/'  voxel '/'];
%     my_path = [path_maquina  'CIMAT/investigacion/data/synthetic/simulations/'  experiment_dir_Str ];
%
%     [bval , bvec ,G ,delta ,DELTA , TE  ]=readChallenge2015ProtocolData([path_maquina 'Dropbox/datosEPFL/simulations/schemes/'],my_scheme_file);
%     protocol = Challenge2015_2_Protocol(bval , bvec ,G ,delta ,DELTA ,TE );
maxTime = 0.0359; %max(TE); %
%
%     gs = protocol.grad_dirs(2:65,:)';
%     %gs = load('/Users/alram/Google Drive/sinc/matlabs/source/PointSets/R3_4999_orientations.txt')';
%

%N = 400; % 2Nk caminates adentro del voxel distribuidos por tama??o de comp de manojo
%T = 3;  % 3,000 pasos por mol?cula
coeff_diff = 2e-9;
%process = 1:16;

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
% funcion nueva que crea el conjunto de experimentos con manojos
% sencillos y cruces,
% params = [angulo de cruce 2 eigen valores manojo 1, 2 eigen valores manojo 2, % de mezcla]
% DB = [ Matriz_de_covarianza_1 (6 valores), Matriz_de_covarianza_2 (6 valores), % de mezcla]

rng(100);

[params, DB, vecindades, nRotAlea, RotAlea] = createDB_single_Crossing();

inspect = 1;
params = params(inspect,:);
DB = DB(inspect,:);


% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
% parametros del los histogramas
scalePerBin = 3; % escala que define el tamanio de los hisotgramas
display(scalePerBin)

maxAvgDisplacement = sqrt(2*coeff_diff*maxTime) * 1e6; % desplazamisnt maximo en um
nBins = int32(maxAvgDisplacement*8);

tamHist = double([2*nBins,2*nBins,2*nBins])
sigmas = 6.6; % sigmas de suavizado
display(sigmas)

%%

% crear kernels de suavizado
FGs = zeros([2*nBins, 2*nBins, 2*nBins, length(sigmas)]);
sepFGs = zeros([length(sigmas), 3, 2*nBins]);
for s=1:length(sigmas)
    [FGs(:,:,:,s), sepFGs(s, :, :)] = makeKernel(tamHist,sigmas(s),nBins);
end

%%
idxPrec = 1;
Precs = 0.075; % precisión del error con que se perturban los parametros
display(Precs)
for prec=Precs
    minErrorCasoSigmaVecino(idxPrec,:,:) = minMaxErrorVecino(params,DB,nRotAlea,RotAlea,prec,maxTime,nBins,scalePerBin,FGs);
    idxPrec = idxPrec + 1;
end

%%
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
% AQUI EMPEZARI'A TU CODIGO A BARRER LA PRIMERA DIMENSION DE DB CON LA
% VARIABLE idx

Ns = 750000;    % tamanio de la poblacion de Monte Carlo

errSigmas = zeros(size(DB,1),length(sigmas));

errTotales = zeros(length(Ns),size(DB,1),length(sigmas));


%fallos ser? una variableque indique si el error de recuperaci?n de la
%distribuci?n con los verdaderos valores genera un error m?s grande que
%el generado por los vecinos
%fallos=zeros(length(Precs),length(Ns),size(DB,1),length(sigmas));




for nn = 1:length(Ns)
    
    N = Ns(nn);
    tic
    for idx= 1:size(DB,1)
    %for idx = 1:1
        
        DBv = DB(idx,:);
        % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        %para sacar las matrices de covarianza del, por ejemplo, renglon 10,000
        T1 = MakeDT_Matrix(DBv(1), DBv(2), DBv(3), DBv(4 ), DBv(5 ), DBv(6 ));
        T2 = MakeDT_Matrix(DBv(7), DBv(8), DBv(9), DBv(10), DBv(11), DBv(12));
        alpha = DBv(13); % porcentaje de mezcla
        
        
        precici
        % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        % calcular histograma analitico para esas 2 matrices de covarianza
        histAnalytic = computeHistAnaly3D(T1*2*maxTime,T2*2*maxTime,alpha,nBins,scalePerBin);
        plotHist3D(histAnalytic,1,'from analytic');
        
        
        % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        % calcular histograma desde muestras para las mismas matrices de covarianza
        if alpha < 1 %crossing
            N1 = floor(2*N*alpha); N2 = floor(2*N*(1-alpha));
            %xs = [ mvnrnd([0;0;0],T1*2*maxTime,N1*1000) ; mvnrnd([0;0;0],T2*2*maxTime,N2*1000)];
            xs = [ mvnrnd([0;0;0],T1*2*maxTime,N1) ; mvnrnd([0;0;0],T2*2*maxTime,N)];
        else % single bundle
            %xs = mvnrnd([0;0;0],T1*2*maxTime,2*N*1000);
            xs = mvnrnd([0;0;0],T1*2*maxTime,N);
        end
        xs = xs * 1e6; %el muestreo los genera en m, se converten a um
        hist3D_samples = computeHistogram3DFromSamples(xs,nBins,scalePerBin);
        %plotHist3D(hist3D_samples,1,'from samples, x');
        names = ['x' 'y' 'z'];
        for i = 1:3
            %figure;
            plotHist3D(hist3D_sim,i,strcat('From gaussian sampling: ', names(i)) );
            %title(names(i));
        end
        
        
        display([  N idx ]);
        
        errores = zeros(1,length(sigmas));
        
        for s=1:length(sigmas)
            %display([s idx])
            % - - - - -
            %Smooth histograma Samples
            histAnalytic_s =  SmoothHistogram( histAnalytic, FGs(:,:,:,s)) ;
            hist3D_samples_s =  SmoothHistogram( hist3D_samples, FGs(:,:,:,s)) ;
            % plotHist3D(hist3D_samples_s,'smoothed from samples');
            
            % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            % medir la diferencia entre histogramas
            %errores(s)= computeErrorHistsProyecciones(histAnalytic_s,hist3D_samples_s,T1,T2,alpha,nBins,scalePerBin);
            %errores(s)= computeErrorHistsLineas(histAnalytic_s,hist3D_samples_s,T1,T2,alpha,nBins,scalePerBin);
            %errores(s) = KLdiv(histAnalytic_s,hist3D_samples_s);
            %errores(s) = computeErrorHists3DL2(hist3D_samples_s,histAnalytic_s);
            errores(s) = computeErrorHists3DL1(hist3D_samples_s,histAnalytic_s);
        end
        
        errSigmas(idx,:) = errores;
        %for idxPrec=1:length(Precs)
        %    fallos(idxPrec,nn,idx,:)= int32(errores>squeeze(minErrorCasoSigmaVecino(idxPrec,idx,:)));
        %end
        
    end %end for de DBscal
    toc
    
    errTotales(nn,:,:) = errSigmas;precici
    
end


%save('enviroment_15_Ago_scalePerBin3p0');


%ratioError=errTotales./minErrorCasoSigmaVecino

%% importar .traj del simulador MC/DC

N = 750000;
T = 5750;
nTrajsMC = 24

my_path = 'D:\recorrersimuladormc\750kN_5750T_20um_0.7icvf\diffZ_extra\' % el path a los archivos .traj
process = 1:nTrajsMC; % si paralelizaste en 24 nucleos
[strSubstrates, generic_names_data,idxSubstrate ] = selectInputData(N,T);
xs = load3DPointsMC_in_um(N,T,idxSubstrate,strSubstrates,generic_names_data,my_path,process);

%hist3D_sim = computeHistogram3DFromWalks(T3, idxSubstrate, strSubstrates, generic_names_data, my_path, maxTime,coeff_diff,process);

%% importar .traj del simulador CAMINO

%N = 75000;
%T = 4000;

myPath = '/home/ponco/devel/cimat/tesis-cimat/recorrersimuladormc/crossing_total';
nChunks = 1;
[xs, N, T] = readTrajCamino(myPath, nChunks);

icvf = 0.5502871265636782;

%% guardar variables extraidas de simulador para reuso

save('crossing_45.mat', 'xs', 'N', 'T')


%% plotear los tres histogramas de muestra
figure;
%hist3D_sim_intra = computeHistogram3DFromSamples(xs_intra,nBins,scalePerBin);
%hist3D_sim_extra = computeHistogram3DFromSamples(xs_extra,nBins,scalePerBin);
%hist3D_comb = icvf*hist3D_sim_intra + (1-icvf)*hist3D_sim_extra;
%plotHist3D(hist3D_comb,1,'From simulator (complete)');

hist3D_sim = computeHistogram3DFromSamples(xs,nBins,scalePerBin);
hist3D_sim_s = SmoothHistogram(hist3D_sim, FGs(:,:,:,s));
names = ['x' 'y' 'z'];
for i = 1:3
    %figure;
    plotHist3D(hist3D_sim,i,strcat('From simulator: ', names(i)) );
    plotHist3D(hist3D_sim_s,i,strcat('From simulator (smoothed out): ', names(i)) );
    %title(names(i));
end

%% plotear vectores de desplazamiento en tres dimensiones e histogramas
figure;
scatter3(xs(:,1),xs(:,2),xs(:,3), 30); 
axis('equal');
xlabel('x');
ylabel('y');
zlabel('z');


names = ['x' 'y' 'z'];
for i = 1:3
    figure;
    hist(xs(:,i));
    title(names(i));
end
%view([0 0 1]);

%% visualizar trayectorias finales de la nube de puntos

process = 1:4;

traj = [];
for np = process
    
    my_file = [my_path 'outputs\' 'bundle_z_extra_750000N_5750T_' num2str(np-1) '.traj'];
    fprintf('\n%s',my_file)
    
    fileID = fopen(my_file,'rb');
    trajp = fread(fileID,'float32');
    fclose(fileID);
    
    N_p = length(trajp)/(3*(T+1));
    trajp = reshape(trajp,3,N_p*(T+1))';
    
    
    
    %block = (1:T+1)+(i-1)*(T+1);
    idxs = (T+1) : (T+1) : (N_p*(T+1));
    traj = vertcat(traj, trajp(idxs,:));
    clearvars trajp
    %scatter3(traj( block(end),1) , traj( block(end) ,2), traj( block(end) ,3),'r*')
    %scatter3(traj( walkerEnd,1) , traj( walkerEnd ,2), traj( walkerEnd ,3),'r*')
    %scatter3(trajp( idxs,1) , trajp( idxs ,2), trajp( idxs ,3),'r*')
    %hold on
    

end

figure;
scatter3(traj(:,1), traj(:,2), traj(:,3))
axis('equal');
xlabel('x');
ylabel('y');
zlabel('z');
%hold off;

names = ['x' 'y' 'z'];
for i = 1:3
    figure;
    hist(traj(:,i));
    title(names(i));
end

%% estadisticas de vectores de desplazamiento
MD = mean( (xs*1e-6))
MSD = mean( (xs*1e-6) .^2)
estimated_cdiffus = MSD/(2*maxTime)
relError_perComponent = 100*abs( estimated_cdiffus - coeff_diff)/coeff_diff


%coef dif
%% estimar difusion con signal DWI
realCoeff = 6E-10

S_0 = 750000; %cantidad de caminantes
S = 453918 %signal DWI 

G = 0.280; %potencia del gradiente
smalldel = 0.0015; %duracion del gradiente
delta = 0.07; 

GAMMA = 2.675987E8;


modQ = GAMMA*smalldel*G; 
diffTime = delta - smalldel/3; 
b = diffTime.*modQ.^2;

dwi_cdifuss = -log(S/S_0)/b

relError = 100*abs(realCoeff - dwi_cdifuss)/realCoeff


%% generar histograma gaussiano muestreado con coeficientes de difusion estimados desde los vectores de desplazamiento
cx = estimated_cdiffus(1);
cy = estimated_cdiffus(2);
cz = estimated_cdiffus(3);
T_samp =  MakeDT_Matrix(cx, 0, 0, cy, 0, cz);

ms = size(xs);
Nsamp = ms(1);

xs_gauss = mvnrnd([0;0;0],T_samp*2*maxTime,Nsamp);

xs_gauss = xs_gauss * 1e6; %el muestreo los genera en m, se converten a um
hist3D_samples_g = computeHistogram3DFromSamples(xs_gauss,nBins,scalePerBin);

names = ['x' 'y' 'z'];

for i = 1:3
    plotHist3D(hist3D_samples_g,i,strcat('From gaussian sampling: ', names(i)) );
end

%% explorar parametros con bootstrapping

[btst, btsm] = bootstrp( 10, ...
    @(x)fit1CrossHist(xs_intra(x,:), xs_extra(x,:), FGs, s, nBins, scalePerBin, icvf, maxTime), ... 
    1:N, ...
    'Options', statset('UseParallel', true));

figure;
coefNames = {'Coeficiente Ortogonal' 'Coeficiente Paralelo'};
boxplot(btst,'Notch','on','Labels',coefNames)
title(strcat('Un cruce. ICVF: ', num2str(icvf)))
ylabel('Valor')

%% explorar parámetros con dos cruces
tic
[coeffs_2Cross, distances_2Cross ] = fit2CrossHist(xs, FGs, s, nBins, scalePerBin, maxTime);
toc
