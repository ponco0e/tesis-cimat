function [l1,l23,h1] = fitGMM_1_Ds_priori(Ds,gs,h1)

    nMeas = length(Ds);
    %h2 = [1,0,0];
    %h3 = [0,1,0];
    
    for i=1:nMeas
        ppsqrt = (gs(i,:)*h1)^2;
        MatLS(i,1) = ppsqrt;
        MatLS(i,2) = 1- ppsqrt;  % = ( (h2*gs(i,:)')^2 + (h3*gs(i,:)')^2  ) ;
    end
    
    sol = MatLS\Ds;
    
    figure, plot(Ds,'r')   , hold on; 
    plot(MatLS*sol,'b+')   
    
    l1 = sol(1);
    l23 = sol(2); 
    %T1 = sol(1)*(h1')*h1 + sol(2)*(h2')*h2 + sol(2)*(h3')*h3;
    
end
