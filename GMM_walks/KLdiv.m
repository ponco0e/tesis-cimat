function dist=KLdiv(P,Q)
%  dist = KLDiv(P,Q) Kullback-Leibler divergence of two discrete probability
%  distributions
%  P and Q  deben entrar normalizadas 
% P =  adim
% Q =  adim
% dist = 1 x 1



    if size(P,1)~=size(Q,1)  || size(P,2)~=size(Q,2)  || size(P,3)~=size(Q,3)
        error('the number of size of P and Q should be the same');
    end

    if sum(~isfinite(P(:))) || sum(~isfinite(Q(:)))
       warning('the inputs contain non-finite values!') 
       dist = NaN;
       return 
    end

    temp =  P.*log(P./Q);
    temp(isnan(temp))=0; % resolving the case when P(i)==0
    %numinf = sum(double(isinf(temp(:))))
    temp(isinf(temp))= 0; %1/numel(P); %0; % resolving the case when Q(i)==0, not sure!
    
    temp = real(temp);
    
    dist = sum(temp(:));
end
