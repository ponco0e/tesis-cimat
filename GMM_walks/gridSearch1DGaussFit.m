function [ oparams, distances ] = gridSearch1DGaussFit( x_in, y_in, centerVar, normToFit, binSize)
%gridSearch1DFuncFit Ajusta una gaussiana con un grid de búsqueda a un
%histograma normalizado
%   by ponco

ortCDTol = centerVar*0.999;
ortCDSteps = 1000;
diff_ort =  centerVar-ortCDTol :  2*ortCDTol/(ortCDSteps-1) : centerVar+ortCDTol;

%diff_amp = 0.0001 : 0.0001 : 1;


fitFunc = @(p,x) (1/sqrt(2*pi*p(3))) * exp(-((x-p(2)).^2)/(2*p(3)));
%fitFunc = @(p,x) p(1) * exp(-((x-p(2)).^2)/(2*p(3)));

distances = zeros( ...
    length(diff_ort),1 ); %, ...
    %length(diff_amp) ... %amplitud
     %);%, ... %coeficiente ortogonal 
    %'gpuArray');

for i = 1 : length(diff_ort)  %por cada angulo (T2)
    c_ort = diff_ort(i);
    j = 1;
    %for c_amp = diff_amp
        %oparams(1) = c_amp;  % amplitude
        oparams(1) = 0;
        oparams(2) = 0;       % media
        oparams(3) = c_ort;      %  varianza
        
        %y_est = fitFunc(oparams, x_in);
        y_est = 0.5*(fitFunc(oparams, x_in-(binSize/2)) + fitFunc(oparams, x_in+(binSize/2)) );
        
        
        if(isa(normToFit, 'numeric'))
            distances(i) = norm((y_in-y_est), normToFit); %norma normal
            %distances(i) = norm((y_in-y_est)./(y_in + 10*eps), normToFit); %norma con error relativo
        elseif(normToFit == 'l')
            distances(i) = -sum( log(fitFunc(oparams, x_in))  );
        end
        
        
        j = j+1;
    %end
end

%conseguir minimo
[minDist, flatIdx] = min(distances(:));
%minL1
%distances(flatIdx)

%obtener del indice flettened los coeficientes de las tablas de busqueda

%[minOrtIdx, minAmpIdx] = ind2sub(size(distances),flatIdx);
[minOrtIdx] = ind2sub(size(distances),flatIdx);
%distances(minOrtIdx, minParIdx, minRotangIdx)


%oparams(1) = diff_amp(minAmpIdx);  % amplitude
oparams(3) = diff_ort(minOrtIdx);      %  varianza


end

