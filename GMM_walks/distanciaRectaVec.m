function  [i_idx,j_idx,k_idx] = distanciaRectaVec(X,Y,Z,p1,distanciaMin)


    
    QPx = p1(1) - X;
    QPy = p1(2) - Y;
    QPz = p1(3) - Z;
    
    idxVec = find(sqrt(  (QPy*p1(3)-QPz*p1(2)).^2 + (QPx*p1(3)-QPz*p1(1)).^2 + (QPx*p1(2)-QPy*p1(1)).^2 ) ./ norm(p1) < distanciaMin);
    
    [i_idx,j_idx,k_idx] = ind2sub(size(QPx),idxVec);
    %aqui mejor un FIND?
end
