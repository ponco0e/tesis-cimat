function [h_vals,centers] =  hist1d_fromProy(pp_v,hist_v,minXH,maxXH,scalePerBin1D)

    dimXH = (maxXH - minXH)*scalePerBin1D + 1;
    h_vals = zeros(dimXH,1);

    centers = minXH:1/scalePerBin1D:maxXH;
    
    for k=1:size(pp_v,1)
        idx = (pp_v(k) - minXH)*scalePerBin1D + 1;
        if idx >0 && idx <= dimXH
            h_vals(idx) = h_vals(idx) + hist_v(k);
        end
    end
    h_vals = h_vals / sum(h_vals);
end
