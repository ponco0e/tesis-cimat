function [result] = fit1CrossHist_histGT(hist3D, GKernRAM, bins, scalepBin, maxTime, scaledKernCM, groundTruthParams)
%by ponco

%NOTA: usar pasos impares (para incluir al valor central en cuestión)
ortCDTol = groundTruthParams.gtCDOrt*0.999;
ortCDSteps = 41;

parCDTol = str2double(groundTruthParams.gtCD)*0.15;
parCDSteps = 21;



diff_ort =    groundTruthParams.gtCDOrt-ortCDTol :  2*ortCDTol/(ortCDSteps-1) : groundTruthParams.gtCDOrt+ortCDTol;
diff_par =    str2double(groundTruthParams.gtCD)-parCDTol    :  2*parCDTol/(parCDSteps-1) : str2double(groundTruthParams.gtCD)+parCDTol;

%de prueba
%diff_rotang = 55.0 : 20.0 : 95.0;
%diff_ort = 1e-9*(0.1 : 0.5 : 1.5);
%diff_par = 1e-9*(1.5 : 0.5 : 2.5);


%angulo, coeficientes del T1, coeficientes del T2
distances = zeros( ...
    length(diff_ort), ... %coeficiente ortogonal compartido
    length(diff_par),  ... %coeficientes paralelos (T1, T2)
    'gpuArray');

hist3D_comb_s = SmoothHistogram(hist3D, GKernRAM);

%par
for i = 1 : length(diff_ort)  %por cada angulo (T2)
    c_ort = diff_ort(i);
    
    j = 1;
    for c_par1 = diff_par %por cada coeff paralelo (T2)
        %disp(['Buscando en ang=' num2str(rotAng) '°, c_ort=' num2str(c_ort) ', c_par1= ' num2str(c_par1) ', cpar2=' num2str(c_par2) ])
        %generar tensores
        T1 =  MakeDT_Matrix(c_ort, 0, 0, c_ort, 0, c_par1);
        
        %generar histograma analitico suavizado
        hist3D_an_s = computeHistAnaly3D(T1*2*maxTime + scaledKernCM, ...
            T1, ... %dummy
            1.0, ...
            bins, ...
            scalepBin);
        
        %comparar
        distances(i,j) = computeErrorHists3DL1(hist3D_comb_s, hist3D_an_s);
        
        j = j+1;
    end
    
end


%conseguir minimo
[minL1, flatIdx] = min(distances(:));
%minL1
%distances(flatIdx)

%obtener del indice flettened los coeficientes de las tablas de busqueda

[minOrtIdx, minParIdx1] = ind2sub(size(distances),flatIdx);
%distances(minOrtIdx, minParIdx, minRotangIdx)

coef_ort_min = diff_ort(minOrtIdx);
coef_par_min1 = diff_par(minParIdx1);


disp([ 'min L1: ', num2str(minL1), ...
    ', C_ort: ', num2str(coef_ort_min), ...
    ', C_par1: ', num2str(coef_par_min1)
    ])


result = struct('minL1', gather(minL1), ...
    'cOrt', coef_ort_min, ...
    'cPar1', coef_par_min1, ...
    'distanceSurface', gather(distances));

end
