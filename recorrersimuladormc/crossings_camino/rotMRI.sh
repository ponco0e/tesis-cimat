#!/bin/bash

rotang=90

for f in ./lvl*.traj
do
	#generar el nombre del archivo rotado
	rfn=${f%.*}.${rotang}rotAng.traj
	echo $f
	echo $rfn

	#rotar
	./rotateTrajCamino $f $rotang

	#escanear archivo rotado
	../camino/bin/scan -trajfile $rfn -schemefile ../schemes/moddedDyrby_1shell_b1000.0.txt > $rfn.bfloat
        
	#eliminar traj rotado
	rm $rfn
done
