#!/bin/bash

#escanear todos los trajs de los niveles disponibles 

#declare -a icvfLvls=("low" "mid" "high")
declare -a icvfLvls=("lvl2" "lvl3" "lvl4" "lvl5" "lvl6" "lvl7")

for lvl in "${icvfLvls[@]}"
do
    for f in ./$lvl*.traj
    do
        ../camino/bin/scan -trajfile $f -schemefile ../schemes/moddedDyrby_1shell_b1000.0.txt > $f.bfloat &
        
        pids[${i}]=$!
        sleep 5
    done
    
    #esperar a que los procesos intra y extra acaben
    for pid in ${pids[*]}
    do
        wait $pid
    done
done
