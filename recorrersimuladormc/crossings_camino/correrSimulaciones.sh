#!/bin/bash

# con seed 60476047
# 170 cyl da icvf de 0.20284519076726346
# 260 cyl da icvf de 0.30812232342739665
# 330 cyl da icvf de 0.4014456195874796
# 412 cyl da icvf de 0.5016271405754553
# 490 cyl da icvf de 0.6076080511924874
# 580 cyl da icvf de 0.7064550523980454

# 300 cyl da icvf  BAJO de 0.37179500637303714
# 425 cyl da icvf MEDIO de 0.5228519024512746
# 600 cyl da icvf  ALTO de 0.7207069831677195


declare -A icfvs
icfvs[lvl2]=170
icfvs[lvl3]=260
icfvs[lvl4]=330
icfvs[lvl5]=412
icfvs[lvl6]=490
icfvs[lvl7]=580
#icfvs[low]=300
#icfvs[mid]=425
#icfvs[high]=600


cDiff=2.1e-9

walkerNo=500000
tSteps=4000
simLenght=0.035900

declare -a initpos=("intra" "extra") # "uniform")

#por cada icvf
for K in "${!icfvs[@]}"
do
    icvfLvl=$K
    cylNo=${icfvs[${K}]}
    
    #por cada caso (intra y extra)
    for i in "${initpos[@]}"
    do
        ../camino/bin/datasynth -seed 60476047 -walkers $walkerNo -tmax $tSteps -voxels 1 -p 0.0 -initial $i -substrate inflammation -increments 1 -gamma 5.9242 1.065E-7 -substratesize 3.5E-5 -numcylinders $cylNo -diffusivity $cDiff -cylfile "${icvfLvl}ICVF_cd${cDiff}_gammac_${i}.centers.txt" -duration $simLenght -trajfile "${icvfLvl}ICVF_cd${cDiff}_gammac_${i}.traj" &
        echo "whow"
        pids[${i}]=$!
        sleep 5
    done
    
    #esperar a que los procesos intra y extra acaben
    for pid in ${pids[*]}
    do
        wait $pid
    done
done




