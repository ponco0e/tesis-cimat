#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  4 15:53:07 2018

@author: alfonso
"""

import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation
import pandas as pd

experiment = "test_intra"
tfilename = "bundle_z_intra_69478kN_1kT_0.traj"



scaleFact = 1E-1

voxelXLim = [x*scaleFact for x in [0,0.2]] 
voxelYLim = [x*scaleFact for x in [0,0.2]] 
voxelZLim = [x*scaleFact for x in [-0,0.2]] 


#%%

routeNames = ["outputs_cyls_1", "outputs_free_1"]

pTraj = None

routes = []

for rName in routeNames:
    filename = "{}/{}/{}".format(experiment, rName, tfilename)
    with open(filename, 'rb') as f:
        data = np.fromfile(f, dtype=np.float32)
        pTraj = data.reshape([3,-1], order='F')
        routes.append(pTraj)
        
T = routes[0].shape[1] 

#%%    
    
def Gen_RandLine(length, dims=2):
    """
    Create a line using a random walk algorithm

    length is the number of points for the line.
    dims is the number of dimensions the line has.
    """
    lineData = np.empty((dims, length))
    lineData[:, 0] = np.random.rand(dims)
    for index in range(1, length):
        # scaling the random numbers by 0.1 so
        # movement is small compared to position.
        # subtraction by 0.5 is to change the range to [-0.5, 0.5]
        # to allow a line to move backwards.
        step = ((np.random.rand(dims) - 0.5) * 0.1)
        lineData[:, index] = lineData[:, index - 1] + step

    return lineData


def update_lines(num, dataLines, lines):
    ax.view_init(elev=10., azim=num)
    #ax.title("$Z_1 =$ " + str(dataLines[0][2,num]) +
    #         "$Z_2 =$ " + str(dataLines[1][2,num]))
    title = ""
    lineNo = 1
    for line, data in zip(lines, dataLines):
        # NOTE: there is no .set_data() for 3 dim data...
        line.set_data(data[0:2, :num])
        line.set_3d_properties(data[2, :num])
        title += "$Z_" + str(lineNo) + " =$ " + str(data[2,num]) + ", "
        lineNo += 1
    ax.set_title(title)
    return lines

# Attaching 3D axis to the figure
fig = plt.figure()
ax = p3.Axes3D(fig)

# Fifty lines of random 3-D lines
#data = [Gen_RandLine(25, 3) for index in range(50)]
data = routes

# Creating fifty line objects.
# NOTE: Can't pass empty arrays into 3d version of plot()
lines = [ax.plot(dat[0, 0:1], dat[1, 0:1], dat[2, 0:1])[0] for dat in data]

# Setting the axes properties
ax.set_xlim3d(voxelXLim)
ax.set_xlabel('X')

ax.set_ylim3d(voxelYLim)
ax.set_ylabel('Y')

ax.set_zlim3d(voxelZLim)
ax.set_zlabel('Z')

ax.set_title('3D Test')

# Creating the Animation object
line_ani = animation.FuncAnimation(fig, update_lines, T, fargs=(data, lines),
                                   interval=50, blit=False)


#%%

line_ani.save("trajs.mp4", dpi=140)

#%%
plt.show()