#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 11 11:32:03 2019

@author: alfonso
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import json
from tabulate import tabulate
import matplotlib.animation as animation
from matplotlib.colors import Normalize
from mpl_toolkits.mplot3d import Axes3D

import seaborn as sns
sns.set()


#%% Leer jsons
dataPath = '/home/ponco/devel/cimat/tesis-cimat/recorrersimuladormc/xss/' 

#tolerancias en porcentaje
tolerances = {'rotAng': 15.0,
              'cPar' : 15.0,
              'cOrt' : 99.9
              }


idata = []

for gt_rotang in np.arange(60, 90+1, 10):
    for gt_cd in ['1.9e-9', '2e-9', '2.1e-9']:
        for gt_icvfLvl in ['low','mid', 'high']:
            with open( dataPath + '/' + 'Cross1_' + 'rotAng' + str(gt_rotang) + 
                      '_' + gt_cd +'CD_' + gt_icvfLvl + 
                      'ICVF' + '.json') as json_data:
                d = json.load(json_data)
                d['distanceSurface'] = np.reshape(
                        d['distanceSurface']['_ArrayData_'], 
                        d['distanceSurface']['_ArraySize_'],
                        order='F'
                        )
                d['gtCD'] = float(d['gtCD'])
                idata.append(d)

results = pd.DataFrame(idata)
results['gtICVFLvl'] = results['gtICVFLvl'].astype('category')
results['gtICVFLvl'] = results['gtICVFLvl'].cat.reorder_categories(['low', 'mid', 'high'])


#%% Añadir información extra
expandRes = results.copy()
#boolean que indica si se le atinó al ángulo
expandRes['missedRotang'] = expandRes['gtRotang'] == expandRes['rotAng']

#desviación con signo del coeficiente paralelo real al aproximado
expandRes['cPar1_relErr'] = 100.0*(expandRes['cPar1'] - expandRes['gtCD'])/expandRes['gtCD']
expandRes['cPar2_relErr'] = 100.0*(expandRes['cPar2'] - expandRes['gtCD'])/expandRes['gtCD']
expandRes['rotAng_relErr'] = 100.0*(expandRes['rotAng'] - expandRes['gtRotang'])/expandRes['gtRotang']
expandRes['cOrt_relErr'] = 100.0*(expandRes['cOrt'] - expandRes['gtCDOrt'])/expandRes['gtCDOrt']

#%% Escribir a excel
with open('expandRes.html', 'w') as ofile:
    #tmp['distanceSurface'] = None
    ofile.write(expandRes.style.bar(align='mid', color=['#d65f5f', '#5fba7d']).render())
                                                        
    expandRes.style.hide_columns(['distanceSurface'])\
    .background_gradient(cmap='viridis', low=.5, high=0)\
    .to_excel('expandRes.xls', engine='openpyxl')

#%% Contar equivocaciones angulares
#expandRes.columns = ['D_{ort}', 'D_{par,1}', D]
rotAngResults = pd.crosstab(expandRes['gtICVFLvl'], expandRes['missedRotang'])
print(tabulate(rotAngResults, headers='keys', tablefmt='psql'))

#%% Plottear

#histograma de desviaciones en cd paralelos
expandRes[['gtICVFLvl', 'cPar1_relErr', 'cPar2_relErr']].hist(by='gtICVFLvl',
         layout=(1,3), 
         label=['cPar1_relErr', 'cPar2_relErr']
         )[2].legend()
plt.figure()
#swarmplots de los coeficientes
sns.catplot(x="gtICVFLvl", 
            y ='cPar1_relErr', 
            hue='gtRotang',
            kind='swarm', 
            data=expandRes,
            ).set(ylim=(-tolerances['cPar'], tolerances['cPar']))
plt.figure()
sns.catplot(x="gtICVFLvl", 
            y ='cPar2_relErr', 
            hue='gtRotang',
            kind='swarm', 
            data=expandRes
            ).set(ylim=(-tolerances['cPar'], tolerances['cPar']))

plt.figure()
sns.catplot(x="gtICVFLvl", 
            y ='rotAng_relErr', 
            hue='gtRotang',
            kind='swarm', 
            data=expandRes
            ).set(ylim=(-tolerances['rotAng'], tolerances['rotAng']))
plt.figure()
sns.catplot(x="gtICVFLvl", 
            y ='cOrt_relErr', 
            hue='gtRotang',
            kind='swarm', 
            data=expandRes
            ).set(ylim=(-tolerances['cOrt'], tolerances['cOrt']))
plt.figure()
sns.catplot(x="gtICVFLvl", 
            y ='cOrt', 
            hue='gtRotang',
            kind='swarm', 
            data=expandRes,
            ).set(ylim=(0, 1.2e-9))

plt.figure()
#sns.distplot(expandRes.groupby('gtICVFLvl')['rotAng_dev'])

#sns.catplot(y="cPar1_dev", x="gtICVFLvl", kind="violin",inner="stick",
#            palette="pastel",
#            data=expandRes).set(ylim=(-tolerances['cPar'], tolerances['cPar']))
#
#sns.catplot(y="cPar2_dev", x="gtICVFLvl", kind="violin",inner="stick",
#            palette="pastel",
#            data=expandRes).set(ylim=(-tolerances['cPar'], tolerances['cPar']))
#
#sns.catplot(y="rotAng_dev", x="gtICVFLvl", kind="violin",inner="stick",
#            palette="pastel",
#            data=expandRes).set(ylim=(-tolerances['rotAng'], tolerances['rotAng']))

#%% Superficies de busqueda
#experimentNo = 0
#
#fig,axn = plt.subplots(3, 4, sharex=True, sharey=True)
#cbar_ax = fig.add_axes([.91, .3, .03, .4])
#
#def animF(frameNo):
#    valMin = np.min(expandRes['distanceSurface'][experimentNo][frameNo])
#    valMax = np.max(expandRes['distanceSurface'][experimentNo][frameNo])
#    
#    gtAng = expandRes['gtRotang'][experimentNo]
#    gtCort = expandRes['gtCDOrt'][experimentNo]
#    
#    nAngSteps = len(expandRes['distanceSurface'][experimentNo])
#    angStep = 0.02*gtAng*tolerances['rotAng']/(nAngSteps-1)
#    estAng = gtAng - tolerances['rotAng']*gtAng*0.01 + (frameNo*angStep)
#    
#    
#    
#    
#
#    fig.suptitle("$\\hat{\\theta} = $" + '%.2f' % estAng + "°")
#    
#    for i, ax in enumerate(axn.flat):
#        nCortSteps = len(expandRes['distanceSurface'][experimentNo][frameNo])
#        cOrtStep = 0.02*gtCort*tolerances['cOrt']/(nCortSteps-1)
#        estCort = gtCort - tolerances['cOrt']*gtCort*0.01 + (i*cOrtStep)
#        
#        sns.heatmap(expandRes['distanceSurface'][experimentNo][frameNo][i], 
#                    ax=ax, 
#                    square=True,
#                    cbar=(i==0),
#                    cbar_ax=None if i else cbar_ax,
#                    vmin=valMin,
#                    vmax=valMax
#                    )
#        #.set(title='$C_{ort} = $' + str(i))
#        
#        ax.grid(False)
#        #plt.axis('off')
#        ax.set_xticks([])
#        ax.set_yticks([])
#        ax.set_xlabel("$\\hat{D}_{Par,1}$")
#        ax.set_ylabel("$\\hat{D}_{Par,2}$")
#        ax.set(title='$\\hat{D}_{ort} = $' + '%.2e' % estCort)
#        
#    fig.tight_layout(rect=[0, 0, .9, 1])
#        
#    
#ani = animation.FuncAnimation(fig, 
#                              animF, 
#                              frames=len(expandRes['distanceSurface'][experimentNo]),
#                              interval=750
#                              )
#
#ani.save("surface.mp4", dpi=240)

#%%
#plt.figure()
experimentNo = 3

def explode(data):
    shape_arr = np.array(data.shape)
    size = shape_arr[:3]*2 - 1
    exploded = np.zeros(np.concatenate([size, shape_arr[3:]]), dtype=data.dtype)
    exploded[::2, ::2, ::2] = data
    return exploded

def expand_coordinates(indices):
    x, y, z = indices
    x[1::2, :, :] += 1
    y[:, 1::2, :] += 1
    z[:, :, 1::2] += 1
    return x, y, z

def normalize(arr):
    arr_min = np.min(arr)
    return (arr-arr_min)/(np.max(arr)-arr_min)

def plot_cube(cube, angle=320):
    fig.clear()
    ax = fig.gca(projection='3d')
    gtAng = expandRes['gtRotang'][0]
    
    nAngSteps = len(expandRes['distanceSurface'][experimentNo])
    angStep = 0.02*gtAng*tolerances['rotAng']/(nAngSteps-1)
    estAng = gtAng - tolerances['rotAng']*gtAng*0.01 + (angle*angStep)

    fig.suptitle("$\\hat{\\theta} = $" + '%.2f' % estAng + "°")
    #cube = normalize(cube)
    nrml = Normalize(cube.min(), cube.max())
    cube = nrml(cube)

    facecolors = plt.cm.viridis(cube)
    facecolors[:,:,:,-1] = 0.5
    facecolors = explode(facecolors)

    filled = facecolors[:,:,:,-1] != 0
    #x, y, z = expand_coordinates(np.indices(np.array(filled.shape) + 1))


    ax.view_init(30, 320)
    #ax.set_xlim(right=IMG_DIM*2)
    #ax.set_ylim(top=IMG_DIM*2)
    #ax.set_zlim(top=IMG_DIM*2)
    
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])
    ax.set_xlabel('$\\hat{D}_{ort}$')
    ax.set_ylabel("$\\hat{D}_{Par,1}$")
    ax.set_zlabel("$\\hat{D}_{Par,2}$")

    ax.voxels(#x, y, z, 
            filled, facecolors=facecolors, edgecolors=facecolors)
    
    sm = plt.cm.ScalarMappable(cmap='viridis', norm=nrml)
    sm.set_array([])
    plt.colorbar(sm)
    
    #plt.show()



#ax = plt.axes(projection='3d')
#ax.voxels(filled=np.ones(shape=(13,13,13)), 
#          facecolors=plt.cm.viridis(normalize(expandRes['distanceSurface'][0][0])));

fig = plt.figure(figsize=(30/2.54, 30/2.54))


ani2 = animation.FuncAnimation(fig, 
                              lambda x: plot_cube(expandRes['distanceSurface'][experimentNo][x], x), 
                              frames=len(expandRes['distanceSurface'][experimentNo]),
                              interval=500
                              )

ani2.save("surface2_{}.mp4".format(experimentNo), dpi=240)