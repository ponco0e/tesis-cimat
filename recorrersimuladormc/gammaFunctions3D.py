##Script para generar un cruce de fibras con distribucion Gamma
import numpy as np
import matplotlib.pyplot as plt
import scipy.special as sps
import random as rn
import matplotlib.pyplot as plt
import collections
import math
import copy;
import matplotlib.patches as patches

## Naive Cylinder class
class Cylinder:
    center = [0,0,0];
    B = [0,0,1]
    radius = 1;

    def __init__(self, center,B,radius):
        self.center = np.array(center)
        self.B = np.array(B);
        self.radius = radius;


## Distance between two lines
def dist3D_Line_to_Line( L1P0,L1P1, L2P0,L2P1):
    u = L1P1 - L1P0;
    v = L2P1 - L2P0;
    w = L1P0 - L2P0;
    a = np.dot(u,u);         # always >= 0
    b = np.dot(u,v);
    c = np.dot(v,v);         # always >= 0
    d = np.dot(u,w);
    e = np.dot(v,w);
    D = a*c - b*b;          # always >= 0
#     // compute the line parameters of the two closest points
    if (D < 0.00001) : #        // the lines are almost parallel
        sc = 0.0;
        # tc = (b>c)? d/b:e/c;    ## use the largest denominator
        if(b>c):
            tc=d/b;
        else:
            tc=e/c;
    else:
        sc = (b*e - c*d) / D;
        tc = (a*e - b*d) / D;

#     // get the difference of the two closest points
    dP = w + (sc * u) - (tc * v);  ## =  L1(sc) - L2(tc)

    return np.sqrt(np.dot(dP,dP))   ## return the closest distance

## Collition between two list of cylinders
def cylinderCollision3D(cyl,cyl2):

    for cyl2_ in cyl2:
        dist_ = dist3D_Line_to_Line(cyl.center,cyl.B,cyl2_.center,cyl2_.B);
        dist  = dist_ - float(cyl.radius) - float(cyl2_.radius);
        if(dist <= 1e-5):
            return True;

    return False;

## Boundary Collitions only work for axis parallel
def checkBoundaryCollition(cyl2,voxel_limits):
    #cyl_ = []; Wrong CODE, non periodic
    #
    #rad  = cyl2.radius;
    #temp = Cylinder(cyl2.center,cyl2.B,cyl2.radius);
    #
    #if(cyl2.B[2] == 1): # along z
    #    if cyl2.center[0]-rad >= voxel_limits[0][0]:
    #        if cyl2.center[0]+rad <= voxel_limits[0][1]:
    #            if cyl2.center[1]-rad >= voxel_limits[1][0]:
    #                if cyl2.center[1]+rad <= voxel_limits[1][1]:
    #                    cyl_.append(temp)            
    #else: #along x
    #    if cyl2.center[2]-rad >= voxel_limits[2][0]:
    #        if cyl2.center[2]+rad <= voxel_limits[2][1]:
    #            if cyl2.center[1]-rad >= voxel_limits[1][0]:
    #                if cyl2.center[1]+rad <= voxel_limits[1][1]:
    #                    cyl_.append(temp)
    #    
    #
    #return cyl_;
    #
    
    cyl_ = [];
    cyl_.append(cyl2);
    
    for i in range(3):
        temp = Cylinder(cyl2.center,cyl2.B,cyl2.radius);
        rad  = cyl2.radius;
        #x,y,z + rad
        if (temp.center[i]+rad  >= voxel_limits[i][1]):
            temp.center[i] = temp.center[i]-voxel_limits[i][1]+voxel_limits[i][0];
            temp.B[i] = temp.B[i]-voxel_limits[i][1]+voxel_limits[i][0];
            cyl_.append(temp)
        #x,y,z - rad
        temp2 = Cylinder(cyl2.center,cyl2.B,cyl2.radius);
        if (temp2.center[i]-rad  <= voxel_limits[i][0]):
            temp2.center[i] = temp2.center[i]-voxel_limits[i][0]+voxel_limits[i][1];
            temp2.B[i] = temp2.B[i]-voxel_limits[i][1]+voxel_limits[i][0];
            cyl_.append(temp2)
    
    return cyl_;

## True if there is No collitions
def checkForNOCylinderColition(cylinders,cyl2,voxel_limits):

    collition = collections.namedtuple('no_collition_flag','cylinders');
    collition.no_collition_flag = False;
    cyl22 = checkBoundaryCollition(cyl2,voxel_limits)

    for cyl in cylinders:
            if(cylinderCollision3D(cyl,cyl22)):
                collition.cylinders = [];
                collition.no_collition_flag = False;
                return collition;

    collition.no_collition_flag = True;
    collition.cylinders = cyl22;
    return collition

def computeICVF(cylinders,voxel_limits):

    if len(cylinders) == 0:
        return 0

    AreaV = (voxel_limits[0][1] - voxel_limits[0][0])*(voxel_limits[1][1] - voxel_limits[1][0]);
    radii = [cyl.radius for cyl in cylinders];
    AreaC = np.sum([math.pi*r*r for r in radii ]);
    return AreaC/AreaV;


def computeICVFUndulation(cylinders,voxel_limits,undulAmpl):

    if len(cylinders) == 0:
        return 0

    AreaV = (voxel_limits[0][1] - voxel_limits[0][0])*(voxel_limits[1][1] - voxel_limits[1][0]);
    radii = [cyl.radius for cyl in cylinders];
    AreaC = np.sum([math.pi*r*r for r in radii ]);
    AreaCUndulation = np.sum([math.pi*(r-undulAmpl)*(r-undulAmpl) for r in radii ]);    
    return (AreaC/AreaV, AreaCUndulation/AreaV);



def gammaDistributedCylinders3D(shape,scale,max_num_cylinders, voxel_size, max_icfv = 1,plot = False,output_folder='',probz = 0.5):
    #Gamma shape and scale parameters
    # GAMA=4.8184
    # GAMB=1.3008E-7
    # shape, scale = 4.8184, 1.3008E-1 # mean=4, std=2*sqrt(2)
    # max_num_cylinders = cN = 10000;
    # max_icfv = 0.75;



    epsilon = 0.001;
    # plot_gamma_dist =True;

    substrate = collections.namedtuple('icvf','cylinders');

    voxel_x_limits = [0, voxel_size]
    voxel_y_limits = [0, voxel_size]
    voxel_z_limits = [0, voxel_size]
    voxel_limits = [voxel_x_limits, voxel_y_limits,voxel_z_limits]

    ## Compute/Generate gamma distribtion of radios

    cN = max_num_cylinders;
    cN_z = int(max_num_cylinders*probz);
    cN_x = cN - cN_z;

    #radiis = np.random.gamma(shape, scale, cN)
    radiis = np.random.gamma(shape, scale/2.0, cN) #divided by 2 because it is for radii no for diameters

    ## If plot_gamma_dist
    if plot:
        count, bins, ignored = plt.hist(radiis, 50, normed=False)
        plt.show(False)

    ## Sort'em all
    radiis = (np.sort(radiis))[::-1]
    ## Until all radiis are used or icvf is achieved, we sample, yes we shall!!!!

    #final list
    fig, ax = plt.subplots()
    cylinders  = [];
    cylinders_no_rep = [];




    switch = True;
    count_x = 0;
    count_z = 0;
    perc_display = 0.09;
    for rad in radiis:
        stuck = 0 ;
        while(stuck <= 50):
            t = rn.random();
            x = (t*voxel_limits[0][1] + (1-t)+voxel_limits[0][0]);
            t = rn.random();
            y = (t*voxel_limits[1][1] + (1-t)+voxel_limits[1][0]);
            t = rn.random();
            z = (t*voxel_limits[2][1] + (1-t)+voxel_limits[2][0]);

            t = rn.random();

            if( ((switch == True) or (count_x >= cN_x)) and probz>0.0): 
                cyl = Cylinder([x,y,voxel_size/2],[x,y,1],rad);
            else:
                cyl = Cylinder([voxel_size/2,y,z],[1,y,z],rad);

            collition = checkForNOCylinderColition(cylinders,cyl,voxel_limits)

            if( collition.no_collition_flag == True):
                
                for cyl_ in collition.cylinders:
                    cylinders.append(cyl_)
                    
                    
                cylinders_no_rep.append(cyl);
                break;
            else:
                stuck+=1;

        if(switch == True):
            count_z+=1;
        else:
            count_x+=1;

        switch ^= True;
        icvf = computeICVF(cylinders_no_rep,voxel_limits);

        perc_ = float(count_z+count_x)/len(radiis);

        if( perc_ > perc_display ):
            perc_display+=0.05;
            print("Completed so far: "+str(perc_*100)+ "%,\nICVF achieved: " + str(icvf) + "  ("+ str( int(100.0*icvf/max_icfv)) + "% of the desired icvf)\n");

        if(icvf + epsilon > max_icfv):
            break

    ## Plot 2d "cylinders"
    if(plot):
        X = [x.center[0] for x in cylinders]
        Y = [x.center[2] for x in cylinders]
        # ax.set_xlim((min(voxel_x_limits[0],min(X)),max(voxel_x_limits[1],max(X)) ))
        # ax.set_ylim((min(voxel_y_limits[0],min(Y)),max(voxel_y_limits[1],max(Y)) ))

        ax.set_xlim(voxel_x_limits[0],voxel_x_limits[1])
        ax.set_ylim(voxel_y_limits[0],voxel_y_limits[1])


        for cyl in cylinders:
            if(cyl.B[2] == 1):
                circle = plt.Circle((cyl.center[0], cyl.center[1]), cyl.radius, color='k')
                ax.add_artist(circle)
            else:
                ax.add_patch(patches.Rectangle((0, cyl.center[1]), voxel_size,cyl.radius,alpha=0.1))

        fig.show();

        fig2 = plt.figure()
        radiis = [x.radius for x in cylinders_no_rep];
        count, bins, ignored = plt.hist(radiis, 50, normed=False)
        #y = bins**(shape-1)*(np.exp(-bins/scale) /
                             ##(sps.gamma(shape)*scale**shape))
        #plt.plot(bins, y, linewidth=2, color='r')

        plt.show(False)

    else:
        fig.clf()

    substrate.icvf =  computeICVF(cylinders_no_rep,voxel_limits);
    substrate.cylinders = cylinders;

    f = open(output_folder + "cylinders_gamma_3D_" + str(shape) + "_" + str(scale) + "_" + str(len(cylinders)) + ".txt",'w');

    for cyl in cylinders:
        f.write(str(cyl.center[0])+" "+ str(cyl.center[1])+" "+ str(cyl.center[2])+" " \
                + str(cyl.B[0])+" "+ str(cyl.B[1])+" "+ str(cyl.B[2])+" "+ str(cyl.radius)+"\n")

    f.close();

    return substrate ;



def gammaDistributedBoundingBox3D(shape,scale,max_num_cylinders, voxel_size, \
                                  max_icfv = 1,plot = False,output_folder='',probz = 0.5,undulAmpl=0.4,minInnerRad=0.1):
    #Gamma shape and scale parameters
    # GAMA=4.8184
    # GAMB=1.3008E-7
    # shape, scale = 4.8184, 1.3008E-1 # mean=4, std=2*sqrt(2)
    # max_num_cylinders = cN = 10000;
    # max_icfv = 0.75;

#    rndState = (3, (1360218728L, 744163005L, 2856413205L, 2502662498L, 1391563373L, 332002055L, 527209175L, 4167838870L, 3631280605L, 2700252001L, 631971721L, 623596265L, 1284097257L, 2328825792L, 2997018079L, 2380364861L, 1909108091L, 74404647L, 146153611L, 3679572635L, 3753134262L, 51317565L, 51136867L, 1355515746L, 3850661632L, 432397301L, 3634131438L, 2639200671L, 2169912669L, 4072736823L, 304734880L, 3167000723L, 367916564L, 3875157847L, 1337438261L, 2862884209L, 3531987869L, 265392631L, 3884672775L, 3970674068L, 2231322119L, 1876892160L, 2448092438L, 2312279324L, 115950879L, 3316023155L, 558578828L, 3546904979L, 3298362156L, 999413500L, 1022624147L, 3547193152L, 4265287538L, 3912744133L, 2939269163L, 2930835904L, 2449517843L, 173510904L, 2437257322L, 1634944926L, 3512381970L, 501988009L, 3668713130L, 556305955L, 1272766231L, 97682805L, 2406810132L, 2920635880L, 742185951L, 4227528651L, 3942762834L, 2022331336L, 3950570273L, 1991631600L, 2723225679L, 1031002684L, 1683111283L, 4096678022L, 4193915L, 3925577867L, 3373996286L, 373679003L, 1629452041L, 1479341478L, 1081340802L, 1836497550L, 3700601743L, 2031209736L, 364709434L, 4277485282L, 776594577L, 2066199675L, 4274203324L, 2889266241L, 3642522829L, 775923503L, 4082834596L, 759371230L, 451173972L, 756577389L, 4031478550L, 4279811719L, 2690752941L, 2991237130L, 470088655L, 3584335943L, 2160214380L, 756294252L, 82739919L, 2819003249L, 1024252218L, 557051672L, 511800686L, 1709569923L, 3982378680L, 2605589528L, 3577067686L, 1733883541L, 1906495289L, 3793467563L, 943272403L, 2891595340L, 615741535L, 2733689992L, 3080856993L, 4196576826L, 2307528463L, 1624288465L, 1495044626L, 4091931797L, 2386581700L, 1471110297L, 3818665550L, 3362448212L, 3930540837L, 2969911969L, 3854203952L, 2878270765L, 3998434731L, 1127863196L, 1924709770L, 1316831908L, 732916911L, 609626237L, 3611188692L, 2769046019L, 933700739L, 2775348278L, 3311463125L, 2543294800L, 3479066388L, 3015793187L, 1576420621L, 3364194511L, 2048618089L, 4065414870L, 2424922239L, 662524519L, 4185471887L, 1714218132L, 1210387321L, 813777794L, 2720066094L, 2488664249L, 3346193029L, 1477441944L, 1211641540L, 3873867644L, 468082248L, 1343912518L, 3186520432L, 3889512480L, 2428757458L, 764980487L, 1832398166L, 3880618890L, 3882025898L, 2234842824L, 817479220L, 1402404226L, 1060688575L, 1574280229L, 1845594379L, 1949819002L, 2857759267L, 934169116L, 2424686881L, 2611051213L, 1981180308L, 438608667L, 3526082818L, 2395194636L, 2059003053L, 3329170881L, 4219769251L, 370444033L, 1249655449L, 3975315899L, 3708070264L, 3478523146L, 1919671958L, 2183083162L, 3909983572L, 1620533514L, 474942658L, 971625871L, 1517556560L, 3906858063L, 3087779447L, 1862322664L, 389476158L, 2974185197L, 290395747L, 2515220792L, 2670121905L, 960452739L, 1824242703L, 2051481090L, 202290717L, 4216668986L, 2836393967L, 2514856209L, 3450661628L, 1379939176L, 1945128449L, 2880592219L, 4189318298L, 3342267770L, 1242782591L, 323358987L, 1439077537L, 717972887L, 2903461865L, 3113078953L, 711248366L, 2149617493L, 2862035605L, 971581230L, 2742123072L, 188008256L, 1531814348L, 270918428L, 2151112598L, 1585840295L, 1472200690L, 1708536165L, 625156837L, 4089351920L, 2387975059L, 1941950792L, 1206930075L, 850422925L, 722737009L, 635351901L, 704951644L, 222575238L, 3376752384L, 1411914725L, 2256245822L, 1422266144L, 1246574026L, 2356176670L, 2027580827L, 3318880117L, 3481958007L, 1773904858L, 2009208380L, 2209943101L, 88743045L, 2362012421L, 86530938L, 2549486350L, 921271269L, 862140972L, 1566738902L, 3036217894L, 3742900903L, 2843034060L, 1512067525L, 1150336166L, 1390482378L, 2720219463L, 1319119062L, 2008691546L, 3815402200L, 3941688392L, 1591199714L, 723349478L, 4190807130L, 531897541L, 2488064857L, 34996890L, 1025671864L, 1756548021L, 3071669727L, 3323423097L, 2341131920L, 3652816686L, 1471208709L, 3939277818L, 2116205445L, 3528820072L, 4054282010L, 3481785164L, 2814751420L, 2601332653L, 2237210484L, 2391361719L, 2103622678L, 4083483130L, 4195519571L, 750570875L, 4108043816L, 2978087255L, 2170083476L, 1858988582L, 2075211715L, 3768456229L, 1954764208L, 2279596397L, 2671440416L, 635780247L, 2721450730L, 612348189L, 3471864862L, 3008783572L, 2078646914L, 221773433L, 1219820426L, 3567928639L, 1170988323L, 3321839524L, 805461919L, 500213024L, 3424582571L, 1266298357L, 1173614308L, 290652673L, 3801656987L, 316472488L, 4177315341L, 3931372865L, 2864215229L, 1327970248L, 2612000667L, 3223005773L, 2965910141L, 1799544438L, 3055832619L, 2532624477L, 1112529980L, 2615421950L, 83899588L, 4096256296L, 1794186312L, 755278020L, 2565228146L, 262717241L, 419614546L, 3032534909L, 729198411L, 1468263399L, 3200057284L, 3669539784L, 4075074143L, 2296474593L, 1199152106L, 1972501213L, 3105295011L, 3002137347L, 221530461L, 4044927186L, 961912718L, 3300862011L, 351708802L, 495783717L, 3840772185L, 3159111259L, 768609003L, 2872617796L, 3992279069L, 1148786084L, 1158937945L, 3577404567L, 3721887649L, 1966348826L, 3190257827L, 1370221855L, 1914944018L, 3460813824L, 2397957979L, 467326998L, 4278059828L, 2899567989L, 4043861245L, 197578305L, 3575060129L, 622255747L, 795546234L, 3454407260L, 1464930642L, 162330093L, 1114533996L, 465145018L, 1681640998L, 1725198352L, 4141394795L, 1205780273L, 1093904546L, 3351959199L, 1513278769L, 4170309341L, 2137908809L, 1530623038L, 3189794370L, 5469861L, 613010159L, 1457186186L, 219119668L, 2473878250L, 2947041105L, 3703852363L, 2436457676L, 3692508402L, 3719845441L, 679607338L, 474044308L, 1616187512L, 2946948588L, 1458844007L, 595182471L, 891555954L, 3532598212L, 3322989785L, 1355042070L, 3344765592L, 1846000083L, 202489318L, 2794580056L, 1093609305L, 1594127674L, 2516402274L, 817330161L, 1008844334L, 1912417314L, 2946965613L, 1846102779L, 2240690499L, 3542842439L, 407624092L, 1370978674L, 2701428616L, 3390510018L, 1977755752L, 2966564808L, 425765449L, 710139664L, 882708561L, 4225124461L, 2569330749L, 1331661550L, 1815751155L, 2843707224L, 2777344411L, 764869468L, 251670205L, 1606104190L, 1717068161L, 2102772147L, 4067472320L, 1996641180L, 1636312454L, 3698445736L, 1948945410L, 821608884L, 3377798489L, 927499111L, 798219803L, 878846677L, 869899009L, 2170218357L, 2811720246L, 1508909502L, 1503534112L, 1154939811L, 2854729035L, 2133240365L, 3479617387L, 2279022171L, 3428193071L, 1138078694L, 2469648163L, 3279594585L, 378876428L, 1585047349L, 170994842L, 4098146286L, 838855076L, 485686986L, 3993999616L, 3174944493L, 803811432L, 2077975445L, 717226436L, 1046364734L, 221887215L, 932453347L, 3742776991L, 565429783L, 866027875L, 1343723019L, 515535009L, 740261865L, 343230992L, 1881578145L, 3803731384L, 964934306L, 3856333664L, 981655467L, 3912372386L, 1964055671L, 1664410050L, 1883183014L, 3617571492L, 3336531077L, 2861503630L, 1117273045L, 1974026121L, 489746049L, 4059628779L, 2157788423L, 4018079504L, 363063840L, 1552637452L, 1798016469L, 1928269967L, 1192205437L, 126109452L, 90814601L, 686088016L, 2058590103L, 845912924L, 352220501L, 773510276L, 1347770651L, 3269183241L, 553341546L, 1099071285L, 242875706L, 3741707464L, 3272335297L, 1310830439L, 2874700209L, 4275456989L, 2323415802L, 2563519727L, 2079164129L, 4058701694L, 265155343L, 2803145277L, 1121131744L, 2757567657L, 2194305076L, 2851991369L, 106937598L, 3230343754L, 3717713579L, 1114637504L, 2920011523L, 528815945L, 1226727831L, 2871436455L, 2235851399L, 3453520853L, 3522818727L, 3186598482L, 876985476L, 1917010065L, 1982371639L, 3994567090L, 275643045L, 1771094753L, 2416159671L, 1331684524L, 462277259L, 787288502L, 1511165713L, 586039367L, 2508795835L, 141344399L, 571804528L, 2219457637L, 143499780L, 2557159775L, 2684182887L, 973511253L, 3928719966L, 4048311978L, 72701729L, 834755429L, 2717515555L, 1250953221L, 114484116L, 2158997187L, 1175718225L, 1356765210L, 3130131044L, 2928960501L, 54343231L, 4130912065L, 875594970L, 733738671L, 1583446630L, 3743854050L, 1582239401L, 1545940444L, 2048638566L, 2849778671L, 958863504L, 2655903368L, 1827282823L, 1241515027L, 3615854015L, 3181294485L, 58L), None)
#    rn.setstate(rndState)


    epsilon = 0.001;
    # plot_gamma_dist =True;

    substrate = collections.namedtuple('substrate',['icvf','cylinders','icvfUndulation']);

    voxel_x_limits = [0, voxel_size]
    voxel_y_limits = [0, voxel_size]
    voxel_z_limits = [0, voxel_size]
    voxel_limits = [voxel_x_limits, voxel_y_limits,voxel_z_limits]

    ## Compute/Generate gamma distribtion of radios

    cN = max_num_cylinders;
    cN_z = int(max_num_cylinders*probz);
    cN_x = cN - cN_z;

    radiis = np.random.gamma(shape, scale/2.0, cN) #divided by 2 because it is for radii no for diameters


    ## If plot_gamma_dist
    if plot:
        count, bins, ignored = plt.hist(radiis, 50, normed=False)
        plt.show(False)


    ## Sort'em all
    radiis = (np.sort(radiis))[::-1]
    ## Until all radiis are used or icvf is achieved, we sample, yes we shall!!!!

    #final list
    fig, ax = plt.subplots()
    cylinders  = [];
    cylinders_no_rep = [];


    
    ondul_rotAng = [];
    ondul_innerRad = [];
    ondul_ampl = [];

    switch = True;
    count_x = 0;
    count_z = 0;
    perc_display = 0.09;
    for rad in radiis:
        
        if rad < undulAmpl + minInnerRad: #only the ones that can contain the smallest allowed radii
            continue
        
        
        stuck = 0 ;
        while(stuck <= 500):
            t = rn.random();
            #x = (t*voxel_limits[0][1] + (1-t)+voxel_limits[0][0]);
            x = (voxel_limits[0][1]-voxel_limits[0][0])*t + voxel_limits[0][0];
            
            t = rn.random();
            #y = (t*voxel_limits[1][1] + (1-t)+voxel_limits[1][0]);
            y = (voxel_limits[1][1]-voxel_limits[1][0])*t + voxel_limits[1][0];
            
            t = rn.random();
            #z = (t*voxel_limits[2][1] + (1-t)+voxel_limits[2][0]);
            z = (voxel_limits[2][1]-voxel_limits[2][0])*t + voxel_limits[2][0];

            t = rn.random();

            if((switch == True) or (count_x >= cN_x)):
                cyl = Cylinder([x,y,voxel_size/2],[x,y,1],rad);
            else:
                cyl = Cylinder([voxel_size/2,y,z],[1,y,z],rad);

            collition = checkForNOCylinderColition(cylinders,cyl,voxel_limits)

            if( collition.no_collition_flag == True):
                angle = rn.uniform(-math.pi, math.pi);
                
                for cyl_ in collition.cylinders:
                    cylinders.append(cyl_)
                    #set the same rotation angle and same inner radius
                    ondul_rotAng.append(angle);
                    ondul_innerRad.append(cyl_.radius-undulAmpl);
                    ondul_ampl.append(undulAmpl);
                    
                cylinders_no_rep.append(cyl);
                break;
            else:
                stuck+=1;

        if(switch == True):
            count_z+=1;
        else:
            count_x+=1;

        switch ^= True;
        icvf,icvfUndulation = computeICVFUndulation(cylinders_no_rep,voxel_limits,undulAmpl);

        perc_ = float(count_z+count_x)/len(radiis);

        if( perc_ > perc_display ):
            perc_display+=0.05;
            print("Completed so far: "+str(perc_*100)+ "%,\nICVF achieved: " + str(icvf) + "-" + str(icvfUndulation) + "  ("+ str( int(100.0*icvf/max_icfv)) + "% of the desired icvf)\n");

        if(icvf + epsilon > max_icfv):
            break

#    idx = 0;
#    for cyl in cylinders:
#        print(cyl.radius,cyl.center[0],cyl.center[1],ondul_rotAng[idx],ondul_innerRad[idx],ondul_ampl[idx])
#        idx = idx +1;


    ## Plot 2d "cylinders"
    if(plot):
        X = [x.center[0] for x in cylinders]
        Y = [x.center[2] for x in cylinders]
        # ax.set_xlim((min(voxel_x_limits[0],min(X)),max(voxel_x_limits[1],max(X)) ))
        # ax.set_ylim((min(voxel_y_limits[0],min(Y)),max(voxel_y_limits[1],max(Y)) ))

        ax.set_xlim(voxel_x_limits[0],voxel_x_limits[1])
        ax.set_ylim(voxel_y_limits[0],voxel_y_limits[1])


        for cyl in cylinders:
            if(cyl.B[2] == 1):
                circle = plt.Circle((cyl.center[0], cyl.center[1]), cyl.radius, color='k')
                ax.add_artist(circle)
            else:
                ax.add_patch(patches.Rectangle((0, cyl.center[1]), voxel_size,cyl.radius,alpha=0.1))

        fig.show();

        fig2 = plt.figure()
        radiis = [x.radius for x in cylinders_no_rep];
        count, bins, ignored = plt.hist(radiis, 50, normed=False)
        #y = bins**(shape-1)*(np.exp(-bins/scale) /
                             ##(sps.gamma(shape)*scale**shape))
        #plt.plot(bins, y, linewidth=2, color='r')

        plt.show(False)

    else:
        fig.clf()

    substrate.icvf,substrate.icvfUndulation = computeICVFUndulation(cylinders_no_rep,voxel_limits,undulAmpl);
    substrate.cylinders = cylinders;

    f = open(output_folder + "cylinders_gamma_ondulation_3D_Amp" + str(undulAmpl) + '_voxelSize' +str(voxel_size) + '_' + str(shape) + "_" + str(scale) + "_" + str(len(cylinders)) + ".txt",'w');

    idx = 0;
    for cyl in cylinders:
        f.write(str(cyl.center[0])+" "+ str(cyl.center[1])+" "+ str(cyl.center[2])+" " \
                + str(cyl.B[0])+" "+ str(cyl.B[1])+" "+ str(cyl.B[2])+" " \
                + str(cyl.radius) +" "+ str(ondul_rotAng[idx]) +" "+ str(ondul_innerRad[idx]) +" "+ str(ondul_ampl[idx])+"\n")
        idx = idx +1;

    f.close();

    return substrate ;
