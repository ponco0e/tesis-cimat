## execute
# python 
# >> execfile('3DGammaDistribution.py')
# quit()



##includes

from gammaFunctions3D import *
import matplotlib.pyplot as plt

## Script to create parallel cylinders with  Gamma distribution
plt.close("all")

#Gamma shape and scale parameters
# GAMA=4.8184
# GAMB=1.3008E-7
shape_, scale_ = 6.27, 4.90E-1/4.0 # mean=4, std=2*sqrt(2)

substrate = gammaDistributedCylinders3D(shape=shape_,scale=scale_, \
    max_num_cylinders=2000, voxel_size=20, max_icfv = 0.7, \
    plot = True,output_folder='',probz = 1.0)

print("Finished with ICFV: " + str(substrate.icvf) + " and " + str(len(substrate.cylinders)) + \
    " cylinders\n");

wait = input('Press a + ENTER to continue ...');


#==============================================================================
#==============================================================================
# rn.seed(12);
# np.random.seed(12);
#  
# substrate = gammaDistributedBoundingBox3D(shape=shape_,scale=scale_, \
#             max_num_cylinders=100, voxel_size=5, max_icfv = 0.7, \
#             plot = True,output_folder='',probz = 1.0, \
#             undulAmpl = 0.3,minInnerRad = 0.1)
#
# print("Finished with ICFV: " + str(substrate.icvf) + "-" + \
#    str(substrate.icvfUndulation) + " and " + str(len(substrate.cylinders)) + \
#    " cylinders\n");
#==============================================================================


# import bpy
# import math
#
#
# voxel_size = 100.0a
# # gather list of items of interest in order to clean.
# candidate_list = [item.name for item in bpy.data.objects if item.type == "MESH"]
# # select previous vertex.
# for object_name in candidate_list:
#   bpy.data.objects[object_name].select = True
# # remove all selected.
# bpy.ops.object.delete()
#
# add_cylinder = bpy.ops.mesh.primitive_cylinder_add
#
# #Read centers and radius file
# file = '/home/jonathan/Documents/Programming/SpecialProjects/Diffusion_Simulator_2.0/Project/Python-Interface/cylinders_gamma_3D3.27_0.49_487.txt';
#
# with open(file) as f:
#     xyzdabcr = []
#     for line in f: # read rest of lines
#         xyzdabcr.append([float(x) for x in line.split()])
#
# #Reads the parametrs and scale them
# for i in range(len(xyzdabcr )):
#     for j in range(len(xyzdabcr [i])):
#         xyzdabcr[i][j] = xyzdabcr[i][j];
#
#
# num_vertices = 20;
#
# for i in range(len(xyzdabcr)):
#     if (xyzdabcr[i][5] == 1.0):
#         add_cylinder(location=[xyzdabcr [i][0],xyzdabcr [i][1],xyzdabcr [i][2]],rotation=[0,0,0],radius=xyzdabcr[i][6],depth=voxel_size,end_fill_type='NOTHING',vertices=num_vertices)
#     else:
#         add_cylinder(location=[xyzdabcr [i][0],xyzdabcr [i][1],xyzdabcr [i][2]],rotation=[0,3.14159/2,0],radius=xyzdabcr[i][6],depth=voxel_size,end_fill_type='NOTHING',vertices=num_vertices)
# #JOin the resulting cylidners in one MESH
# candidate_list = [item.name for item in bpy.data.objects if item.type == "MESH"]
# for object_name in candidate_list:
#   bpy.data.objects[object_name].select = True
#
# #bpy.ops.object.join()
# bpy.ops.object.editmode_toggle()
# bpy.ops.mesh.quads_convert_to_tris()
# bpy.ops.object.editmode_toggle()
