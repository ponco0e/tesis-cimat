#execute
#python
# >>>execfile('SimpleGammaDistribution.py');
# >>>quit()

##includes
from gammaFunctions2D import *
import matplotlib.pyplot as plt


## Script to create parallel cylinders with  Gamma distribution
plt.close("all")
#Gamma shape and scale parameters
# GAMA=4.8184
# GAMB=1.3008E-7
## Gamma distribtion shape parameters, number of cylinders to sample
#shape, scale = 3.27, 4.90E-1 # mean=4, std=2*sqrt(2)a
shape, scale = 6.27, 4.90E-1/8.0 # mean=4, std=2*sqrt(2)

cN = 1000;	# n = 100000 en script de matlab
## Compute/Generate gamma distribtion of radios
radiis = np.random.gamma(shape, scale, cN);
# Sort'em all
radiis = (np.sort(radiis))[::-1]

substrate = gammaDistributedCylinders(radius_list = radiis,max_num_cylinders=cN, voxel_size=30, max_icfv = 0.6,plot = False) #,output_folder='')
    
print("Finished with ICFV: " + str(substrate.icvf) + " and " + str(len(substrate.cylinders)) + " cylinders")


