%function plotSubstrate

close all

figure; hold on;

path = ''%/home/alfonso/devel/tesis-cimat/recorrersimuladormc/'
%[xc,yc,zc,radc] = textread('/Users/alram/tempPrograms/MC-DC-Simulator-master/test/centers.txt','%f %f %f %f','headerlines',1);
[xc,yc,zc,radc] = textread(strcat(path, 'Cilindros_gamma_932-cylinders_0.577896903979.txt'),'%f %f %f %f','headerlines',1);

%[xc,yc,zc,ox,oy,oz,radc] = textread('/Users/alram/Dropbox/datosEPFL/simulations/analysis_traj/analytic/confs/centers_z_orientation.txt','%f %f %f %f %f %f %f','headerlines',1);
%[xc,yc,zc,ox,oy,oz,radc,dummy1,dummy2,dummy3] = textread('/Users/alram/Dropbox/datosEPFL/simulations/axonPackUndulation/voxel2/plys/cylinders_gamma_ondulation_3D_Amp0.3_voxelSize5_6.27_0.1225_23.txt','%f %f %f %f %f %f %f %f %f %f');


%[xw,yw,zw] = textread('/Users/alram/tempPrograms/MC-DC-Simulator-master/test/ini_walker_pos.txt','%f %f %f','headerlines',1);
%[xw,yw,zw] = textread('/Users/alram/Dropbox/datosEPFL/simulations/analysis_traj/analytic/confs/ini_walker_extra_pos.txt');

%posIntra=[];
%nPosIntra = 0;
for i=1:length(xc)
    h = circle2(xc(i),yc(i),radc(i));
    %if (xc(i)-0.070)^2 + (yc(i)-0.070)^2 < 0.065^2
    %    nPosIntra = nPosIntra +1;
    %    posIntra = [posIntra;[xc(i),yc(i)]];
    %end
end

min(radc)
max(radc)

svx=30;
svy=30;
rectangle('Position',[0 0 svx svy]);


nPs =100000;
x = rand(nPs,1)*svx ;
y = rand(nPs,1)*svy;
indIntra = zeros(nPs,1) > 1;

for i=1:length(xc)
    indIntra = indIntra |  ( ((x-xc(i)).^2 + (y-yc(i)).^2) < (zeros(nPs,1)+(radc(i)^2)));  
%
end

%return

 plot(x(indIntra),y(indIntra),'+b')
 plot(x(~indIntra),y(~indIntra),'+r')
% 
posIntra = [x(indIntra),y(indIntra), zeros(length(x(indIntra)),1)]*1e-3; %escribirlo en milimetros
save('ini_walker_intra_pos.txt','posIntra','-ascii')

posExtra = [x(~indIntra),y(~indIntra), zeros(length(x(~indIntra)),1)]*1e-3; %escribirlo en milimetros
save('ini_walker_extra_pos.txt','posExtra','-ascii')


newCyl=[xc,yc,zc,radc]*1e-3; %escribirlo en milimetros
save( ['centersRadio_' 'Cilindros_gamma_943-cylinders_0.5793106551279116.txt'],'newCyl','-ascii')

%for X bundle
%centers_x_orientation = [zc,yc,xc,oz,oy,ox,radc]; % hay que agregar un 1 al inicio, y dejar solo un espacio entre numeros
%save('/Users/alram/Dropbox/datosEPFL/simulations/analysis_traj/analytic/confs/centers_x_orientation.txt','centers_x_orientation','-ascii', '-tabs')
%ini_walker_extra_x_pos = [zw,yw,xw];
%save('/Users/alram/Dropbox/datosEPFL/simulations/analysis_traj/analytic/confs/ini_walker_extra_x_pos.txt','ini_walker_extra_x_pos','-ascii')
% ini_walker_intra_x_pos = [posIntra(:,3),posIntra(:,2),posIntra(:,1)];
% save('/Users/alram/Dropbox/datosEPFL/simulations/analysis_traj/analytic/confs/ini_walker_intra_x_pos.txt','ini_walker_intra_x_pos','-ascii')

hold off;

%end



