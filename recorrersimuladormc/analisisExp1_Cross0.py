#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 13:19:27 2019

@author: ponco
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import json
from tabulate import tabulate
import matplotlib.animation as animation
from matplotlib.colors import Normalize
from mpl_toolkits.mplot3d import Axes3D

import seaborn as sns
sns.set()


#%% Leer jsons
dataPath = '/home/ponco/devel/cimat/tesis-cimat/recorrersimuladormc/xss/' 

#tolerancias en porcentaje
tolerances = {'cPar' : 15.0,
              'cOrt' : 99.9
              }


idata = []


for gt_cd in ['1.9e-9', '2e-9', '2.1e-9']:
    for gt_icvfLvl in ['low','mid', 'high']:
        with open( dataPath + '/' + 'Cross0' +
                  '_' + gt_cd +'CD_' + gt_icvfLvl + 
                  'ICVF' + '.json') as json_data:
            d = json.load(json_data)
            d['distanceSurface'] = np.array(d['distanceSurface'])
#            d['distanceSurface'] = np.reshape(
#                    d['distanceSurface']['_ArrayData_'], 
#                    d['distanceSurface']['_ArraySize_'],
#                    order='F'
#                    )
            d['gtCD'] = float(d['gtCD'])
            idata.append(d)

results = pd.DataFrame(idata)
#del results['gtRotang']
results['gtICVFLvl'] = results['gtICVFLvl'].astype('category')
results['gtICVFLvl'] = results['gtICVFLvl'].cat.reorder_categories(['low', 'mid', 'high'])

#%% Añadir información extra
expandRes = results.copy()

#desviación con signo del coeficiente paralelo real al aproximado
expandRes['cPar1_relErr'] = 100.0*(expandRes['cPar1'] - expandRes['gtCD'])/expandRes['gtCD']
expandRes['cOrt_relErr'] = 100.0*(expandRes['cOrt'] - expandRes['gtCDOrt'])/expandRes['gtCDOrt']

#%% Plottear
plt.figure()
sns.catplot(x="gtICVFLvl", 
            y ='cPar1_relErr', 
            kind='swarm', 
            data=expandRes,
            ).set(ylim=(-tolerances['cPar'], tolerances['cPar']))
plt.figure()
sns.catplot(x="gtICVFLvl", 
            y ='cOrt_relErr', 
            kind='swarm', 
            data=expandRes
            ).set(ylim=(-tolerances['cOrt'], tolerances['cOrt']))
plt.figure()
sns.catplot(x="gtICVFLvl", 
            y ='cOrt', 
            kind='swarm', 
            data=expandRes,
            ).set(ylim=(0, 1.2e-9))

for idx, row in expandRes.iterrows():
    plt.figure()
    ax = sns.heatmap(row.distanceSurface, center=np.median(row.distanceSurface))
    
    y_min_big, x_min_big = np.unravel_index(
            np.argmin(row.distanceSurface, axis=None), 
            row.distanceSurface.shape)
    
    #groundtruth en estrella verde 
    ax.scatter(row.distanceSurface.shape[1]//2 +1, 
               row.distanceSurface.shape[0]//2 +1, 
               marker='*', s=100, color='lawngreen') 
    
    #estimado en cruz guinda
    ax.scatter(x_min_big, y_min_big, marker='x', s=100, color='plum') 

    #ax.scatter(1, 0, marker='x', s=100, color='white') 

    # get min of new_thick_df
#    min_idx = new_thick_df.values.flatten().argmin()
#    x_min, y_min = min_idx % 10 + .5, 9 - min_idx // 10 + .5
#    ax.scatter(x_min, y_min, marker='*', s=100, color='yellow') 
    
    ax.set_title('Nominals: $ICVF=%s, D_{par,1}=%.2g, D_{ort}=%.2g$' % (row.gtICVFLvl, row.gtCD, row.gtCDOrt))
    ax.set_xlabel("$\\hat{D}_{Par,1}$")
    ax.set_ylabel("$\\hat{D}_{Ort}$")
    
#%% tablas
    
results_L1 = pd.read_csv('../results/1d_hist/L1/zeroCrossHistFit.csv', index_col='Row')
results_L2 = pd.read_csv('../results/1d_hist/L2/zeroCrossHistFit.csv', index_col='Row')
#results_L1_relErr = pd.read_csv('results_L1_relErr.csv', index_col='Row')
#results_L2_relErr = pd.read_csv('results_L2_relErr.csv', index_col='Row')
#results_ml = pd.read_csv('results_ml.csv', index_col='Row')

results_L1['Metric'] = 'L1'
results_L2['Metric'] = 'L2'
#results_L1_relErr['Metric'] = 'L1_relErr'
#results_L2_relErr['Metric'] = 'L2_relErr'
#results_ml['Metric'] = '-LogLike'


results_T1 = pd.concat([results_L1, results_L2])#, results_L1_relErr, results_L2_relErr])
results_T1 = results_T1.pivot_table(index = 'Row',
                              columns=['Metric'], 
                              values=['TotalRaw', 'TotalSmoothed', 'Intra', 'Extra'])
results_T1.loc['Mean'] = results_T1.mean()


results_T1.to_latex('../results/1d_hist/exp1_cross0.tex', float_format='%.2f%%')


iesubset = ['Intra', 'Extra']
results_T2 = pd.concat([results_L1, 
                        results_L2])#, 
                        #results_L1_relErr, 
                        #results_L2_relErr,
                        #results_ml])
results_T2 = results_T2.pivot_table(index = 'Row',
                              columns=['Metric'], 
                              values=['Intra', 'Extra'])
#results_T2.to_latex('../results/1d_hist/exp1_cross0_ie.tex', float_format='%.2f%%')
