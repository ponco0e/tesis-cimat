#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 28 18:06:01 2019

@author: ponco
"""

import numpy as np
import pandas as pd
#import json
import pickle

dataPath = 'crossings_camino/'

bValue_desired = 1000 * 1e6 # de 1000

bValues = [1000 * 1e6,
           1500 * 1e6,
           2000 * 1e6,
           2500 * 1e6
           ]
bValVect = np.ones(91*4)
for i,b in enumerate(bValues):
    bValVect[i*91:(i+1)*91] = b


icvfs = {'lvl2' : 0.20284519076726346,
         'lvl3' : 0.30812232342739665,
         'lvl4' : 0.4014456195874796,
         'lvl5' : 0.5016271405754553,
         'lvl6' : 0.6076080511924874,
         'lvl7' : 0.7064550523980454
         }

parCDs = ['2.1e-9']
iniPoses = ['intra', 'extra']
rotAngs = ['60', '70', '80', '90']
alphas = [0.5]

#%%
def getMinIdx(a):
    return np.unravel_index(np.argmin(a, axis=None), a.shape)

def getNeighborhood(center, relErr, steps):
    return np.arange(center*(1-relErr), center*(1+relErr), center*2*relErr/steps)

def readCaminoXS(fpath):
    idata = np.fromfile(fpath, dtype='double')
    N = idata[0]
    T = idata[1]
    return ( N, T, idata[2:].reshape((-1,3)) )

def readScheme(fpath):
    idata = pd.read_csv(fpath, 
                        delim_whitespace=True, 
                        skiprows=1, 
                        header=None, 
                        names=['g_x',
                               'g_y',
                               'g_z',
                               'G',
                               'bDelta',
                               'sDelta',
                               'tEcho'
                               ])
    return idata

###########################
    
def makeTotalSignal(fibData, currIcvf, alpha): #hacer combinaciones convexas
    fib1_Data = fibData.query('gt_rotAng == \'0\'') #sin rotación
    fib2_Data = fibData.query('gt_rotAng != \'0\'') #con rotación
    
    fib1_intra = fib1_Data[fib1_Data.gt_iniPos == 'intra']
    fib1_extra = fib1_Data[fib1_Data.gt_iniPos == 'extra']
    fib1_sig = currIcvf*fib1_intra['signal'].iloc[0] + (1-currIcvf)*fib1_extra['signal'].iloc[0]
    fib1_cort = currIcvf*fib1_intra['gt_cOrt'].iloc[0] + (1-currIcvf)*fib1_extra['gt_cOrt'].iloc[0]
    
    
    fib2_intra = fib2_Data[fib2_Data.gt_iniPos == 'intra']
    fib2_extra = fib2_Data[fib2_Data.gt_iniPos == 'extra']
    fib2_sig = currIcvf*fib2_intra['signal'].iloc[0] + (1-currIcvf)*fib2_extra['signal'].iloc[0]
    fib2_cort = currIcvf*fib2_intra['gt_cOrt'].iloc[0] + (1-currIcvf)*fib2_extra['gt_cOrt'].iloc[0]
    
    return (alpha*fib1_sig + (1-alpha)*fib2_sig, fib1_cort, fib2_cort)
    
###########################

def gridSearch_xing_3dof(y, 
                         scheme, 
                         groundTruths, 
                         relErr,
                         steps,
                         bValue, 
                         lMetric,
                         alphaSearch = False,
                         currIcvf = None
                         ):
    #direcciones del gradiente magnético
    dirCols= ['g_x','g_y','g_z']
    G = scheme[dirCols]#.mul(scheme['G'], axis=0)
    G = G.to_numpy()#[1:]
    
    #obtener S_0
    S_0 = y[0]
    
    y_est = 0
    
    #reja de búsqueda
    rotAngDiff = getNeighborhood(groundTruths['gt_rotAng'], relErr, steps)
    alphaDiff = getNeighborhood(groundTruths['gt_alpha'], relErr, steps)
    parDiff = getNeighborhood(groundTruths['gt_cPar1'], relErr, steps)
    ortDiff = 0
    
    ortDiff = getNeighborhood(groundTruths['gt_cOrt1'], relErr, steps)
    
    paramDiff = alphaDiff if alphaSearch else rotAngDiff
    
    
    distances = np.zeros((len(paramDiff), len(parDiff), len(ortDiff)))

    
    for i, currParam in enumerate(paramDiff):
        currAlpha = groundTruths['gt_alpha']
        currRotAng = groundTruths['gt_rotAng']
        
        if alphaSearch:
            currAlpha = currParam
        else:
            currRotAng = currParam
            
        #matriz de rotación
        #print(currAlpha, currRotAng)
        s = np.sin((np.pi/180)*currRotAng)
        c = np.cos((np.pi/180)*currRotAng)
        rotM = np.array([
                [c,   0.0,   s],
                [0.0, 1.0, 0.0],
                [-s,  0.0,   c]
                ])
    
        for j, currPar in enumerate(parDiff):
            #tensores de difusión
            D_t1 = np.zeros((3,3))
            D_t1[2,2] = currPar
            #D_t2 = np.zeros((3,3))
            #D_t2[2,2] = currPar
            
            y_stick1 = np.exp(-bValue * currPar * (G @ np.array([0, 0, 1]))**2)
            y_stick2 = np.exp(-bValue * currPar * (G @ rotM @ np.array([0, 0, 1]))**2)
            
            for k, currOrt in enumerate(ortDiff):
                
                #setear tensores
                D_t1[0,0] = D_t1[1,1] = currOrt
                D_t2 = rotM @ D_t1 @ rotM.transpose()
                
                
                #calcular la señal analítica que corresponde a ese tensor gaussiano
                if currIcvf is None:
                    y_est = currAlpha * np.exp(-bValue * np.einsum('ij,ji->i', G, D_t1 @ G.transpose()))
                    y_est += (1-currAlpha) * np.exp(-bValue * np.einsum('ij,ji->i', G, D_t2 @ G.transpose()))
                    y_est *= S_0 
                else:
                    y_est1 = currIcvf * y_stick1 #stick
                    y_est1 += (1-currIcvf)*np.exp(-bValue * np.einsum('ij,ji->i', G, D_t1 @ G.transpose())) #zeppelin
                    
                    y_est2 = currIcvf * y_stick2 #stick
                    y_est2 += (1-currIcvf)*np.exp(-bValue * np.einsum('ij,ji->i', G, D_t2 @ G.transpose())) #zeppelin
                    
                    y_est = S_0 * (currAlpha*y_est1 + (1-currAlpha)*y_est2) 
                    
                distances[i, j, k] = np.linalg.norm((y_est-y), ord=lMetric)
    
    miParam, miCPar, miCOrt = getMinIdx(distances)
    
    print('Found: (param, cPar, cOrt): ', (paramDiff[miParam], parDiff[miCPar], ortDiff[miCOrt]))
    
    return (paramDiff[miParam], parDiff[miCPar], ortDiff[miCOrt], distances)    

###########################
    
def gridSearch_xing_4dof(y, 
                         scheme, 
                         groundTruths, 
                         relErr,
                         steps,
                         bValue, 
                         lMetric,
                         currPar,
                         zsModel = False
                         ):
    #direcciones del gradiente magnético
    dirCols= ['g_x','g_y','g_z']
    G = scheme[dirCols]#.mul(scheme['G'], axis=0)
    G = G.to_numpy()#[1:]
    
    #obtener S_0
    S_0 = y[0]
    
    y_est = 0
    
    #currPar = groundTruths['gt_cPar']
    
    #reja de búsqueda
    rotAngDiff = getNeighborhood(groundTruths['gt_rotAng'], relErr, steps)
    alphaDiff = getNeighborhood(groundTruths['gt_alpha'], relErr, steps)
    #parDiff = getNeighborhood(groundTruths['gt_cPar1'], relErr, steps)
    icvfDiff = getNeighborhood(groundTruths['gt_icvf'], relErr, steps)
    ortDiff = 0
    
    ortDiff = getNeighborhood(groundTruths['gt_cOrt1'], relErr, steps)
    
    distances = np.zeros( (len(icvfDiff), len(rotAngDiff), len(alphaDiff), len(ortDiff)) )

    for i, currIcvf in enumerate(icvfDiff):
        
        for j, currRotAng in enumerate(rotAngDiff):
            
            #currAlpha = groundTruths['gt_alpha']
            #currRotAng = groundTruths['gt_rotAng']
            
            #matriz de rotación
            #print(currAlpha, currRotAng)
            s = np.sin((np.pi/180)*currRotAng)
            c = np.cos((np.pi/180)*currRotAng)
            rotM = np.array([
                    [c,   0.0,   s],
                    [0.0, 1.0, 0.0],
                    [-s,  0.0,   c]
                    ])
        
            for k, currAlpha in enumerate(alphaDiff):
                #tensores de difusión
                D_t1 = np.zeros((3,3))
                D_t1[2,2] = currPar
                #D_t2 = np.zeros((3,3))
                #D_t2[2,2] = currPar
                
                y_stick1 = np.exp(-bValue * currPar * (G @ np.array([0, 0, 1]))**2)
                y_stick2 = np.exp(-bValue * currPar * (G @ rotM @ np.array([0, 0, 1]))**2)
                
                for l, currOrt in enumerate(ortDiff):
                    
                    #setear tensores
                    D_t1[0,0] = D_t1[1,1] = currOrt
                    D_t2 = rotM @ D_t1 @ rotM.transpose()
                    
                    
                    #calcular la señal analítica que corresponde a ese tensor gaussiano
                    if not zsModel:
                        y_est = currAlpha * np.exp(-bValue * np.einsum('ij,ji->i', G, D_t1 @ G.transpose()))
                        y_est += (1-currAlpha) * np.exp(-bValue * np.einsum('ij,ji->i', G, D_t2 @ G.transpose()))
                        y_est *= S_0 
                    else:
                        y_est1 = currIcvf * y_stick1 #stick
                        y_est1 += (1-currIcvf)*np.exp(-bValue * np.einsum('ij,ji->i', G, D_t1 @ G.transpose())) #zeppelin
                        
                        y_est2 = currIcvf * y_stick2 #stick
                        y_est2 += (1-currIcvf)*np.exp(-bValue * np.einsum('ij,ji->i', G, D_t2 @ G.transpose())) #zeppelin
                        
                        y_est = S_0 * (currAlpha*y_est1 + (1-currAlpha)*y_est2) 
                        
                    distances[i, j, k, l] = np.linalg.norm((y_est-y), ord=lMetric)
    
    miIcvf, miRotAng, miAlpha, miCOrt = getMinIdx(distances)
    
    print('Found: (icvf, rotAng, alpha, cOrt): ', 
          (icvfDiff[miIcvf], rotAngDiff[miRotAng], alphaDiff[miAlpha], ortDiff[miCOrt])
          )
    
    return (icvfDiff[miIcvf], rotAngDiff[miRotAng], alphaDiff[miAlpha], ortDiff[miCOrt], distances)  
    
#%% cargar datos sin rotar para obtener el ground truth
    
inputData = {'gt_cPar' : [],
             'gt_cOrt' : [],
             'gt_icvfLvl' : [],
             'gt_iniPos' : [],
             'gt_rotAng' : [],
             'signal' : [],
             'gt_cPar_measured' : []
             }

shell1Schm = readScheme('schemes/moddedDyrby_1shell_b1000.0.txt')  
shells4Schm = readScheme('schemes/moddedDyrby_4shell.txt')  
bDelta_desired = shell1Schm['bDelta'][1]
sDelta_desired = shell1Schm['sDelta'][1]


for gt_cd in parCDs:
    for gt_icvfLvl in icvfs:
        for gt_iniPos in iniPoses:
            for gt_rotAng in ['0', *rotAngs]:
                #leer señal y caminatas
                mriData = None
                try:
                    mriData = np.fromfile(dataPath + #'/1shell/' +
                                          gt_icvfLvl + 'ICVF_cd' + 
                                          gt_cd + '_gammac_' + 
                                          gt_iniPos + '.' +
                                          ('' if gt_rotAng=='0' else gt_rotAng + 'rotAng.') +
                                          'traj.bfloat', 
                                          dtype='>f'
                                          )
                    
                    #siempre lee la que no tiene rotanción
                    N, T, xs = readCaminoXS(dataPath + '/' + 
                                            gt_icvfLvl + 'ICVF_cd' + 
                                            gt_cd + '_gammac_' + 
                                            gt_iniPos + '.' +
                                            'traj_'+
                                            'delta' + str(sDelta_desired) + '.xs'
                                            )
                except:
                    print('No existe el archivo')
                    continue
                
                
                inputData['gt_cPar'].append(gt_cd)
                inputData['gt_icvfLvl'].append(gt_icvfLvl)
                inputData['gt_iniPos'].append(gt_iniPos)
                inputData['gt_rotAng'].append(gt_rotAng)
                
                inputData['signal'].append(mriData)
                
                
                #calcular coeficientes ortogonales ground truth
                #MD = np.mean( (xs*1e-6), axis=0)
                MSD = np.mean( (xs*1e-6)**2, axis=0)
                estimated_cdiffus = MSD/(2*(bDelta_desired))
                gt_cOrt = estimated_cdiffus[:2].mean()
                inputData['gt_cOrt'].append(gt_cOrt)
                inputData['gt_cPar_measured'].append(estimated_cdiffus[2])
                #print(estimated_cdiffus)
                
                print('Imported', gt_cd, gt_icvfLvl, gt_iniPos, gt_rotAng)
            
            
            
            
inputData =  pd.DataFrame(inputData)

# escribir tabla de Ground Truths
gts_tab = inputData.query('gt_rotAng == \'0\'')
gts_tab = gts_tab.pivot_table(index='gt_icvfLvl', columns='gt_iniPos', values=['gt_cOrt', 'gt_cPar_measured'], aggfunc='first')
gts_tab.to_latex('../results/substrate/groudTruths.tex', float_format='%.3e')


#%% generar resultados


##############################################################
#
#  RotAng, 3 DOF, Zeppelin Stick
#
##############################################################

cNorm = 1

#la fibra 1 siempre es la que no tiene rotación

rotDF = {'gt_cPar1' : [],
         'gt_cPar2' : [],
         'gt_cOrt1' : [],
         'gt_cOrt2' : [],
         'gt_icvfLvl' : [],
         'gt_rotAng' : [],
         'gt_alpha' : [],
         'cPar1' : [], #predichos
         'cPar2' : [],
         'cOrt1' : [],
         'cOrt2' : [],
         'rotAng' : [],
         'signal' : [],
         'distanceSurface' : []
        }

for gt_cd in parCDs:
    for gt_icvfLvl, gt_icvf in icvfs.items():
        for gt_rotAng in rotAngs:
            for gt_alpha in alphas:
                cResult = {}
            
                cData = inputData.query('gt_cPar==\'{}\' & gt_icvfLvl==\'{}\' & (gt_rotAng==\'0\' | gt_rotAng==\'{}\')'.format(gt_cd, gt_icvfLvl, gt_rotAng))
                
                cResult['gt_cPar1'] = float(gt_cd)
                cResult['gt_cPar2'] = float(gt_cd)
                
                cResult['gt_icvfLvl'] = gt_icvfLvl
                cResult['gt_rotAng'] = float(gt_rotAng)
                cResult['gt_alpha'] = gt_alpha
                
                crossSig, cort1, cort2 = makeTotalSignal(cData, gt_icvf, gt_alpha)
                
                #en zs, el gt es el extra
                fib1_extra = cData.query('gt_rotAng == \'0\' & gt_iniPos == \'extra\'')
                cResult['gt_cOrt1'] = fib1_extra['gt_cOrt'].iloc[0]
                cResult['gt_cOrt2'] = fib1_extra['gt_cOrt'].iloc[0]
                
                cResult['signal'] = crossSig
                
                
                #búsqueda
                print('Searching', gt_cd, gt_icvfLvl, gt_rotAng, gt_alpha)
                mRotAng, mCPar, mCOrt, ds =  gridSearch_xing_3dof(crossSig, 
                                                                  shell1Schm, 
                                                                  cResult,
                                                                  0.999,
                                                                  100,
                                                                  bValue_desired, 
                                                                  cNorm,
                                                                  False,
                                                                  gt_icvf
                                                                  )
                
                
                cResult['rotAng'] = mRotAng
                cResult['cPar1'] = cResult['cPar2'] = mCPar
                cResult['cOrt1'] = cResult['cOrt2'] = mCOrt
                cResult['distanceSurface'] = ds
                
                outFname =  '../results/crossings/rotAng_zs/' + gt_icvfLvl +  '_' + gt_rotAng + '_' + str(gt_alpha) + '.pickle' #'.json'
                
                #with open(outFname, 'w') as ofile:
                #    pd.Series(your_array).to_json(orient='values')
                #    json.dump(cResult, ofile)
                with open(outFname, 'wb') as ofile:
                    pickle.dump(cResult, ofile, pickle.HIGHEST_PROTOCOL)
                
                for k, v in cResult.items(): #agregar a df principal
                    rotDF[k].append(v)
                
rotDF =  pd.DataFrame(rotDF).set_index('gt_icvfLvl')
rotDF['cPar_err'] = 100*(rotDF['cPar1']/rotDF['gt_cPar1'] - 1)
rotDF['cOrt_err'] = 100*(rotDF['cOrt1']/rotDF['gt_cOrt1'] - 1)
rotDF['rotAng_err'] = 100*(rotDF['rotAng']/rotDF['gt_rotAng'] - 1)

outFname = '../results/crossings/rotAng_zs/' + 'final.pickle'
with open(outFname, 'wb') as ofile:
    pickle.dump(rotDF, ofile, pickle.HIGHEST_PROTOCOL)



##############################################################
#
#  RotAng, 3 DOF, Normal
#
##############################################################

cNorm = 1

#la fibra 1 siempre es la que no tiene rotación

rotDF = {'gt_cPar1' : [],
         'gt_cPar2' : [],
         'gt_cOrt1' : [],
         'gt_cOrt2' : [],
         'gt_icvfLvl' : [],
         'gt_rotAng' : [],
         'gt_alpha' : [],
         'cPar1' : [], #predichos
         'cPar2' : [],
         'cOrt1' : [],
         'cOrt2' : [],
         'rotAng' : [],
         'signal' : [],
         'distanceSurface' : []
        }

for gt_cd in parCDs:
    for gt_icvfLvl, gt_icvf in icvfs.items():
        for gt_rotAng in rotAngs:
            for gt_alpha in alphas:
                cResult = {}
            
                cData = inputData.query('gt_cPar==\'{}\' & gt_icvfLvl==\'{}\' & (gt_rotAng==\'0\' | gt_rotAng==\'{}\')'.format(gt_cd, gt_icvfLvl, gt_rotAng))
                
                cResult['gt_cPar1'] = float(gt_cd)
                cResult['gt_cPar2'] = float(gt_cd)
                
                cResult['gt_icvfLvl'] = gt_icvfLvl
                cResult['gt_rotAng'] = float(gt_rotAng)
                cResult['gt_alpha'] = gt_alpha
                
                crossSig, cort1, cort2 = makeTotalSignal(cData, gt_icvf, gt_alpha)
                
                cResult['gt_cOrt1'] = cort1
                cResult['gt_cOrt2'] = cort2
                
                cResult['signal'] = crossSig
                
                
                #búsqueda
                print('Searching', gt_cd, gt_icvfLvl, gt_rotAng, gt_alpha)
                mRotAng, mCPar, mCOrt, ds =  gridSearch_xing_3dof(crossSig, 
                                                                  shell1Schm, 
                                                                  cResult,
                                                                  0.999,
                                                                  100,
                                                                  bValue_desired, 
                                                                  cNorm,
                                                                  False
                                                                  )
                
                
                cResult['rotAng'] = mRotAng
                cResult['cPar1'] = cResult['cPar2'] = mCPar
                cResult['cOrt1'] = cResult['cOrt2'] = mCOrt
                cResult['distanceSurface'] = ds
                
                outFname = '../results/crossings/rotAng/' + gt_icvfLvl +  '_' + gt_rotAng + '_' + str(gt_alpha) + '.pickle' #'.json'
                
                #with open(outFname, 'w') as ofile:
                #    pd.Series(your_array).to_json(orient='values')
                #    json.dump(cResult, ofile)
                with open(outFname, 'wb') as ofile:
                    pickle.dump(cResult, ofile, pickle.HIGHEST_PROTOCOL)
                
                for k, v in cResult.items(): #agregar a df principal
                    rotDF[k].append(v)
                
rotDF =  pd.DataFrame(rotDF).set_index('gt_icvfLvl')
rotDF['cPar_err'] = 100*(rotDF['cPar1']/rotDF['gt_cPar1'] - 1)
rotDF['cOrt_err'] = 100*(rotDF['cOrt1']/rotDF['gt_cOrt1'] - 1)
rotDF['rotAng_err'] = 100*(rotDF['rotAng']/rotDF['gt_rotAng'] - 1)

outFname =  '../results/crossings/rotAng/' + 'final.pickle'
with open(outFname, 'wb') as ofile:
    pickle.dump(rotDF, ofile, pickle.HIGHEST_PROTOCOL)
    


#%% alphas


##############################################################
#
#  Alpha, 3 DOF, Zeppelin Stick
#
##############################################################

cNorm = 1

alpDF = {'gt_cPar1' : [],
         'gt_cPar2' : [],
         'gt_cOrt1' : [],
         'gt_cOrt2' : [],
         'gt_icvfLvl' : [],
         'gt_rotAng' : [],
         'gt_alpha' : [],
         'cPar1' : [], #predichos
         'cPar2' : [],
         'cOrt1' : [],
         'cOrt2' : [],
         'alpha' : [],
         'signal' : [],
         'distanceSurface' : []
        }

for gt_cd in parCDs:
    for gt_icvfLvl, gt_icvf in icvfs.items():
        for gt_rotAng in rotAngs:
            for gt_alpha in alphas:
                cResult = {}
            
                cData = inputData.query('gt_cPar==\'{}\' & gt_icvfLvl==\'{}\' & (gt_rotAng==\'0\' | gt_rotAng==\'{}\')'.format(gt_cd, gt_icvfLvl, gt_rotAng))
                
                cResult['gt_cPar1'] = float(gt_cd)
                cResult['gt_cPar2'] = float(gt_cd)
                
                cResult['gt_icvfLvl'] = gt_icvfLvl
                cResult['gt_rotAng'] = float(gt_rotAng)
                cResult['gt_alpha'] = gt_alpha
                
                crossSig, cort1, cort2 = makeTotalSignal(cData, gt_icvf, gt_alpha)
                
                #en zs, el gt es el extra
                fib1_extra = cData.query('gt_rotAng == \'0\' & gt_iniPos == \'extra\'')
                cResult['gt_cOrt1'] = fib1_extra['gt_cOrt'].iloc[0]
                cResult['gt_cOrt2'] = fib1_extra['gt_cOrt'].iloc[0]
                
                cResult['signal'] = crossSig
                
                
                #búsqueda
                print('Searching', gt_cd, gt_icvfLvl, gt_rotAng, gt_alpha)
                mAlpha , mCPar, mCOrt, ds =  gridSearch_xing_3dof(crossSig, 
                                                                  shell1Schm, 
                                                                  cResult,
                                                                  0.999,
                                                                  100,
                                                                  bValue_desired, 
                                                                  cNorm,
                                                                  True,
                                                                  currIcvf = gt_icvf
                                                                  )
                
                
                cResult['alpha'] = mAlpha
                cResult['cPar1'] = cResult['cPar2'] = mCPar
                cResult['cOrt1'] = cResult['cOrt2'] = mCOrt
                cResult['distanceSurface'] = ds
                
                outFname = '../results/crossings/alpha_zs/' + gt_icvfLvl +  '_' + gt_rotAng + '_' + str(gt_alpha) + '.pickle' #'.json'
                
                #with open(outFname, 'w') as ofile:
                #    pd.Series(your_array).to_json(orient='values')
                #    json.dump(cResult, ofile)
                with open(outFname, 'wb') as ofile:
                    pickle.dump(cResult, ofile, pickle.HIGHEST_PROTOCOL)
                
                for k, v in cResult.items(): #agregar a df principal
                    alpDF[k].append(v)
                
alpDF =  pd.DataFrame(alpDF).set_index('gt_icvfLvl')
alpDF['cPar_err'] = 100*(alpDF['cPar1']/alpDF['gt_cPar1'] - 1)
alpDF['cOrt_err'] = 100*(alpDF['cOrt1']/alpDF['gt_cOrt1'] - 1)
alpDF['alpha_err'] = 100*(alpDF['alpha']/alpDF['gt_alpha'] - 1)

outFname = '../results/crossings/alpha_zs/' + 'final.pickle' 

with open(outFname, 'wb') as ofile:
    pickle.dump(alpDF, ofile, pickle.HIGHEST_PROTOCOL)


##############################################################
#
#  Alpha, 3 DOF, Normal
#
##############################################################
    
    
cNorm = 1

alpDF = {'gt_cPar1' : [],
         'gt_cPar2' : [],
         'gt_cOrt1' : [],
         'gt_cOrt2' : [],
         'gt_icvfLvl' : [],
         'gt_rotAng' : [],
         'gt_alpha' : [],
         'cPar1' : [], #predichos
         'cPar2' : [],
         'cOrt1' : [],
         'cOrt2' : [],
         'alpha' : [],
         'signal' : [],
         'distanceSurface' : []
        }

for gt_cd in parCDs:
    for gt_icvfLvl, gt_icvf in icvfs.items():
        for gt_rotAng in rotAngs:
            for gt_alpha in alphas:
                cResult = {}
            
                cData = inputData.query('gt_cPar==\'{}\' & gt_icvfLvl==\'{}\' & (gt_rotAng==\'0\' | gt_rotAng==\'{}\')'.format(gt_cd, gt_icvfLvl, gt_rotAng))
                
                cResult['gt_cPar1'] = float(gt_cd)
                cResult['gt_cPar2'] = float(gt_cd)
                
                cResult['gt_icvfLvl'] = gt_icvfLvl
                cResult['gt_rotAng'] = float(gt_rotAng)
                cResult['gt_alpha'] = gt_alpha
                
                crossSig, cort1, cort2 = makeTotalSignal(cData, gt_icvf, gt_alpha)
                
                cResult['gt_cOrt1'] = cort1
                cResult['gt_cOrt2'] = cort2
                
                cResult['signal'] = crossSig
                
                
                #búsqueda
                print('Searching', gt_cd, gt_icvfLvl, gt_rotAng, gt_alpha)
                mAlpha , mCPar, mCOrt, ds =  gridSearch_xing_3dof(crossSig, 
                                                                  shell1Schm, 
                                                                  cResult,
                                                                  0.999,
                                                                  100,
                                                                  bValue_desired, 
                                                                  cNorm,
                                                                  True,
                                                                  )
                
                
                cResult['alpha'] = mAlpha
                cResult['cPar1'] = cResult['cPar2'] = mCPar
                cResult['cOrt1'] = cResult['cOrt2'] = mCOrt
                cResult['distanceSurface'] = ds
                
                outFname = '../results/crossings/alpha/' + gt_icvfLvl +  '_' + gt_rotAng + '_' + str(gt_alpha) + '.pickle' #'.json'
                
                #with open(outFname, 'w') as ofile:
                #    pd.Series(your_array).to_json(orient='values')
                #    json.dump(cResult, ofile)
                with open(outFname, 'wb') as ofile:
                    pickle.dump(cResult, ofile, pickle.HIGHEST_PROTOCOL)
                
                for k, v in cResult.items(): #agregar a df principal
                    alpDF[k].append(v)
                
alpDF =  pd.DataFrame(alpDF).set_index('gt_icvfLvl')
alpDF['cPar_err'] = 100*(alpDF['cPar1']/alpDF['gt_cPar1'] - 1)
alpDF['cOrt_err'] = 100*(alpDF['cOrt1']/alpDF['gt_cOrt1'] - 1)
alpDF['alpha_err'] = 100*(alpDF['alpha']/alpDF['gt_alpha'] - 1)

outFname =  '../results/crossings/alpha/' + 'final.pickle' 

with open(outFname, 'wb') as ofile:
    pickle.dump(alpDF, ofile, pickle.HIGHEST_PROTOCOL)
    
#%% 4DOF

##############################################################
#
#  4 DOF, Normal
#  4DOF normal es sólo 3DOF (rotAng, cOrt, alpha)
#
##############################################################
    
cNorm = 1

dofDF = {'gt_cOrt1' : [],
         'gt_cOrt2' : [],
         'gt_icvfLvl' : [],
         'gt_icvf' : [],
         'gt_rotAng' : [],
         'gt_alpha' : [],
         'cOrt1' : [],
         'cOrt2' : [],
         'alpha' : [],
         'rotAng': [],
         'icvf' : [],
         'signal' : [],
         'distanceSurface' : []
        }

for gt_cd in parCDs:
    for gt_icvfLvl, gt_icvf in icvfs.items():
        for gt_rotAng in rotAngs:
            for gt_alpha in alphas:
                cResult = {}
            
                cData = inputData.query('gt_cPar==\'{}\' & gt_icvfLvl==\'{}\' & (gt_rotAng==\'0\' | gt_rotAng==\'{}\')'.format(gt_cd, gt_icvfLvl, gt_rotAng))
                
                
                cResult['gt_icvfLvl'] = gt_icvfLvl
                cResult['gt_icvf'] = gt_icvf
                cResult['gt_rotAng'] = float(gt_rotAng)
                cResult['gt_alpha'] = gt_alpha
                
                crossSig, cort1, cort2 = makeTotalSignal(cData, gt_icvf, gt_alpha)
                
                cResult['gt_cOrt1'] = cort1
                cResult['gt_cOrt2'] = cort2
                
                cResult['signal'] = crossSig
                
                
                #búsqueda
                print('Searching', gt_cd, gt_icvfLvl, gt_rotAng, gt_alpha)
                mIcvf, mRotAng, mAlpha, mCOrt, ds =  gridSearch_xing_4dof(crossSig, 
                                                                  shell1Schm, 
                                                                  cResult,
                                                                  0.999,
                                                                  50,
                                                                  bValue_desired, 
                                                                  cNorm,
                                                                  float(gt_cd)
                                                                  )
                
                
                cResult['alpha'] = mAlpha
                cResult['rotAng'] = mRotAng
                cResult['icvf'] = mIcvf
                cResult['cOrt1'] = cResult['cOrt2'] = mCOrt
                cResult['distanceSurface'] = ds
                
                outFname =  '../results/crossings/4DOF/' + gt_icvfLvl +  '_' + gt_rotAng + '_' + str(gt_alpha) + '.pickle' #'.json'
                
                #with open(outFname, 'w') as ofile:
                #    pd.Series(your_array).to_json(orient='values')
                #    json.dump(cResult, ofile)
                with open(outFname, 'wb') as ofile:
                    pickle.dump(cResult, ofile, pickle.HIGHEST_PROTOCOL)
                
                for k, v in cResult.items(): #agregar a df principal
                    dofDF[k].append(v)
                
dofDF = pd.DataFrame(dofDF).set_index('gt_icvfLvl')

dofDF['rotAng_err'] = 100*(dofDF['rotAng']/dofDF['gt_rotAng'] - 1)
dofDF['cOrt_err'] = 100*(dofDF['cOrt1']/dofDF['gt_cOrt1'] - 1)
dofDF['icvf_err'] = 100*(dofDF['icvf']/dofDF['gt_icvf'] - 1)
dofDF['alpha_err'] = 100*(dofDF['alpha']/dofDF['gt_alpha'] - 1)

outFname = '../results/crossings/4DOF/' + 'final.pickle' 

with open(outFname, 'wb') as ofile:
    pickle.dump(dofDF, ofile, pickle.HIGHEST_PROTOCOL)



##############################################################
#
#  4 DOF, Zeppelin Stick
#
##############################################################

cNorm = 1

dofDF = {'gt_cOrt1' : [],
         'gt_cOrt2' : [],
         'gt_icvfLvl' : [],
         'gt_icvf' : [],
         'gt_rotAng' : [],
         'gt_alpha' : [],
         'cOrt1' : [],
         'cOrt2' : [],
         'alpha' : [],
         'rotAng': [],
         'icvf' : [],
         'signal' : [],
         'distanceSurface' : []
        }

for gt_cd in parCDs:
    for gt_icvfLvl, gt_icvf in icvfs.items():
        for gt_rotAng in rotAngs:
            for gt_alpha in alphas:
                cResult = {}
            
                cData = inputData.query('gt_cPar==\'{}\' & gt_icvfLvl==\'{}\' & (gt_rotAng==\'0\' | gt_rotAng==\'{}\')'.format(gt_cd, gt_icvfLvl, gt_rotAng))
                
                
                cResult['gt_icvfLvl'] = gt_icvfLvl
                cResult['gt_icvf'] = gt_icvf
                cResult['gt_rotAng'] = float(gt_rotAng)
                cResult['gt_alpha'] = gt_alpha
                
                crossSig, cort1, cort2 = makeTotalSignal(cData, gt_icvf, gt_alpha)
                
                #en zs, el gt es el extra
                fib1_extra = cData.query('gt_rotAng == \'0\' & gt_iniPos == \'extra\'')
                cResult['gt_cOrt1'] = fib1_extra['gt_cOrt'].iloc[0]
                cResult['gt_cOrt2'] = fib1_extra['gt_cOrt'].iloc[0]
                
                cResult['signal'] = crossSig
                
                
                #búsqueda
                print('Searching', gt_cd, gt_icvfLvl, gt_rotAng, gt_alpha)
                mIcvf, mRotAng, mAlpha, mCOrt, ds =  gridSearch_xing_4dof(crossSig,
                                                                  shell1Schm,
                                                                  cResult,
                                                                  0.999,
                                                                  50,
                                                                  bValue_desired,
                                                                  cNorm,
                                                                  float(gt_cd),
                                                                  True
                                                                  )
                
                
                cResult['alpha'] = mAlpha
                cResult['rotAng'] = mRotAng
                cResult['icvf'] = mIcvf
                cResult['cOrt1'] = cResult['cOrt2'] = mCOrt
                cResult['distanceSurface'] = ds
                
                outFname = '../results/crossings/4DOF_zs/' + gt_icvfLvl +  '_' + gt_rotAng + '_' + str(gt_alpha) + '.pickle' #'.json'
                
                #with open(outFname, 'w') as ofile:
                #    pd.Series(your_array).to_json(orient='values')
                #    json.dump(cResult, ofile)
                with open(outFname, 'wb') as ofile:
                    pickle.dump(cResult, ofile, pickle.HIGHEST_PROTOCOL)
                
                for k, v in cResult.items(): #agregar a df principal
                    dofDF[k].append(v)
                
dofDF =  pd.DataFrame(dofDF).set_index('gt_icvfLvl')

dofDF['rotAng_err'] = 100*(dofDF['rotAng']/dofDF['gt_rotAng'] - 1)
dofDF['cOrt_err'] = 100*(dofDF['cOrt1']/dofDF['gt_cOrt1'] - 1)
dofDF['icvf_err'] = 100*(dofDF['icvf']/dofDF['gt_icvf'] - 1)
dofDF['alpha_err'] = 100*(dofDF['alpha']/dofDF['gt_alpha'] - 1)

outFname = '../results/crossings/4DOF_zs/' + 'final.pickle' 

with open(outFname, 'wb') as ofile:
    pickle.dump(dofDF, ofile, pickle.HIGHEST_PROTOCOL)

##############################################################
#
#  4 DOF, Zeppelin Stick 4 shell
#
##############################################################

#NOTA: no se puede ejecutar a menos que se carguen las señales de 4 shells

cNorm = 1

dofDF = {'gt_cOrt1' : [],
         'gt_cOrt2' : [],
         'gt_icvfLvl' : [],
         'gt_icvf' : [],
         'gt_rotAng' : [],
         'gt_alpha' : [],
         'cOrt1' : [],
         'cOrt2' : [],
         'alpha' : [],
         'rotAng': [],
         'icvf' : [],
         'signal' : [],
         'distanceSurface' : []
        }

for gt_cd in parCDs:
    for gt_icvfLvl, gt_icvf in icvfs.items():
        for gt_rotAng in rotAngs:
            for gt_alpha in alphas:
                cResult = {}
            
                cData = inputData.query('gt_cPar==\'{}\' & gt_icvfLvl==\'{}\' & (gt_rotAng==\'0\' | gt_rotAng==\'{}\')'.format(gt_cd, gt_icvfLvl, gt_rotAng))
                
                
                cResult['gt_icvfLvl'] = gt_icvfLvl
                cResult['gt_icvf'] = gt_icvf
                cResult['gt_rotAng'] = float(gt_rotAng)
                cResult['gt_alpha'] = gt_alpha
                
                crossSig, cort1, cort2 = makeTotalSignal(cData, gt_icvf, gt_alpha)
                
                #en zs, el gt es el extra
                fib1_extra = cData.query('gt_rotAng == \'0\' & gt_iniPos == \'extra\'')
                cResult['gt_cOrt1'] = fib1_extra['gt_cOrt'].iloc[0]
                cResult['gt_cOrt2'] = fib1_extra['gt_cOrt'].iloc[0]
                
                cResult['signal'] = crossSig
                
                
                #búsqueda
                print('Searching', gt_cd, gt_icvfLvl, gt_rotAng, gt_alpha)
                mIcvf, mRotAng, mAlpha, mCOrt, ds =  gridSearch_xing_4dof(crossSig,
                                                                  shells4Schm,
                                                                  cResult,
                                                                  0.999,
                                                                  50,
                                                                  bValVect,
                                                                  cNorm,
                                                                  float(gt_cd),
                                                                  True
                                                                  )
                
                
                cResult['alpha'] = mAlpha
                cResult['rotAng'] = mRotAng
                cResult['icvf'] = mIcvf
                cResult['cOrt1'] = cResult['cOrt2'] = mCOrt
                cResult['distanceSurface'] = ds
                
                outFname = '../results/crossings/4DOF_zs_4shell/' + gt_icvfLvl +  '_' + gt_rotAng + '_' + str(gt_alpha) + '.pickle' #'.json'
                
                #with open(outFname, 'w') as ofile:
                #    pd.Series(your_array).to_json(orient='values')
                #    json.dump(cResult, ofile)
                with open(outFname, 'wb') as ofile:
                    pickle.dump(cResult, ofile, pickle.HIGHEST_PROTOCOL)
                
                for k, v in cResult.items(): #agregar a df principal
                    dofDF[k].append(v)
                
dofDF =  pd.DataFrame(dofDF).set_index('gt_icvfLvl')

dofDF['rotAng_err'] = 100*(dofDF['rotAng']/dofDF['gt_rotAng'] - 1)
dofDF['cOrt_err'] = 100*(dofDF['cOrt1']/dofDF['gt_cOrt1'] - 1)
dofDF['icvf_err'] = 100*(dofDF['icvf']/dofDF['gt_icvf'] - 1)
dofDF['alpha_err'] = 100*(dofDF['alpha']/dofDF['gt_alpha'] - 1)

outFname = '../results/crossings/4DOF_zs_4shell/' + 'final.pickle' 

with open(outFname, 'wb') as ofile:
    pickle.dump(dofDF, ofile, pickle.HIGHEST_PROTOCOL)
