#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 17:11:55 2019

@author: ponco
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D

import seaborn as sns
sns.set()

#%% funciones
def getMinIdx(a):
    return np.unravel_index(np.argmin(a, axis=None), a.shape)

def getGradientMagnitude(bValue, tEcho, sDelta, bDelta):
    gmr = 2*np.pi*42.576*1e6 #de MHZ/T ->  rad/(sec T)
    #bval = gmr**2 * G_desired**2 * delta**2 * (bDelta_desired-delta/3) # en s/m^2
    return np.sqrt(bValue/( (gmr**2)*(sDelta**2)*(bDelta - sDelta/3) ))

def getNeighborhood(center, relErr, steps):
    return np.arange(center*(1-relErr), center*(1+relErr), center*2*relErr/steps)
    
def readScheme(fpath):
    idata = pd.read_csv(fpath, 
                        delim_whitespace=True, 
                        skiprows=1, 
                        header=None, 
                        names=['g_x',
                               'g_y',
                               'g_z',
                               'G',
                               'bDelta',
                               'sDelta',
                               'tEcho'
                               ])
    return idata

def gridSearch(y, scheme, ortDiff, parDiffCoef, bValue, lMetric, cicvf = None):
    #direcciones del gradiente magnético
    dirCols= ['g_x','g_y','g_z']
    G = scheme[dirCols]#.mul(scheme['G'], axis=0)
    G = G.to_numpy()#[1:]
    
    #obtener S_0
    S_0 = y[0]
    
    y_stick = np.exp(-bValue * parDiffCoef * (G @ np.array([0, 0, 1]))**2)
    
    #tensor de difusión
    D_t = np.zeros((3,3))
    D_t[2,2] = parDiffCoef
    
    distances = np.zeros(ortDiff.shape)
    for i, currOrt in enumerate(ortDiff):
        #setear tensor
        D_t[0,0] = D_t[1,1] = currOrt
        
        #calcular la señal analítica que corresponde a ese tensor gaussiano
        if cicvf is None:
            y_est = S_0 * np.exp(-bValue * np.einsum('ij,ji->i', G, D_t @ G.transpose()))
        else:
            y_est = cicvf* y_stick #stick
            y_est += (1-cicvf)*np.exp(-bValue * np.einsum('ij,ji->i', G, D_t @ G.transpose())) #zeppelin
            y_est *= S_0 
        
        distances[i] = np.linalg.norm((y_est-y), ord=lMetric)
        
    return (ortDiff[getMinIdx(distances)], distances)


def readCaminoXS(fpath):
    idata = np.fromfile(fpath, dtype='double')
    N = idata[0]
    T = idata[1]
    
    return ( N, T, idata[2:].reshape((-1,3)) )

#icvfs = {'low' : 0.37179500637303714,
#         'mid' : 0.5228519024512746,
#         'high' : 0.7207069831677195
#         }


icvfs = {'lvl2' : 0.20284519076726346,
         'lvl3' : 0.30812232342739665,
         'lvl4' : 0.4014456195874796,
         'lvl5' : 0.5016271405754553,
         'lvl6' : 0.6076080511924874,
         'lvl7' : 0.7064550523980454
         }
#%% calcular 1shell de Dyrby con b definido
    
bValue_desired = 1000 * 1e6 # de 1000

bValues = [1000 * 1e6,
           1500 * 1e6,
           2000 * 1e6,
           2500 * 1e6
           ]

sDelta_desired = 0.0005
#timeSteps = 4000

    
dyrbySchm = readScheme('schemes/Scheme_shells3_Dyrby.txt')

shell1Schm = dyrbySchm.iloc[:91,:].copy().reset_index(drop=True)
shell2Schm = dyrbySchm.iloc[91:182,:].copy().reset_index(drop=True)
shell3Schm = dyrbySchm.iloc[182:,:].copy().reset_index(drop=True)
shell4Schm = dyrbySchm.iloc[182:,:].copy().reset_index(drop=True)

#generar esquema nuevo con la información necesaria
bDelta_desired = shell1Schm['tEcho'][0] - sDelta_desired

for shell, bVal in zip ([shell1Schm, shell2Schm, shell3Schm, shell4Schm],
                  bValues):
    
    shell.G = getGradientMagnitude(
            bVal, 
            shell1Schm['tEcho'][0],
            sDelta_desired,
            bDelta_desired
            )
    shell.G[0] = 0
    
    shell.bDelta = bDelta_desired
    shell.bDelta[0] = 0
    
    shell.sDelta = sDelta_desired
    shell.sDelta[0] = 0

with open('schemes/moddedDyrby_1shell_b{}.txt'.format(bValue_desired/1e6), 'w') as ofile:
    ofile.write('VERSION: STEJSKALTANNER\n')
    shell1Schm.to_csv(ofile, sep=' ', header=None, index=False)

with open('schemes/moddedDyrby_4shell.txt', 'w') as ofile:
    ofile.write('VERSION: STEJSKALTANNER\n')
    shell1Schm.to_csv(ofile, sep=' ', header=None, index=False)
    shell2Schm.to_csv(ofile, sep=' ', header=None, index=False)
    shell3Schm.to_csv(ofile, sep=' ', header=None, index=False)
    shell4Schm.to_csv(ofile, sep=' ', header=None, index=False)

#%% hacer análisis
cNorm = 1
    
results = {'gt_cPar': [],
           'gt_cOrt' : [],
           'gt_icvfLvl' : [],
           'gt_iniPos' : [],
           'cOrt' : [],
           'cOrt_partition' : [],
           'distanceSurface' : [],
           'signal' : []
           }
    
dataPath = 'crossings_camino/'
shellpath = '1shell'
for gt_cd in ['1.9e-9', '2e-9', '2.1e-9']:
    for gt_icvfLvl in icvfs:
        for gt_iniPos in ['intra', 'extra']:
            mriData = None
            try:
                
                mriData = np.fromfile(dataPath + shellpath  + '/' + 
                                    gt_icvfLvl + 'ICVF_cd' + 
                                    gt_cd + '_gammac_' + 
                                    gt_iniPos + '.traj.bfloat', 
                                    dtype='>f')
                
                N, T, xs = readCaminoXS(dataPath + '/' + 
                                    gt_icvfLvl + 'ICVF_cd' + 
                                    gt_cd + '_gammac_' + 
                                    gt_iniPos + '.traj_'+
                                    'delta' + str(sDelta_desired) + '.xs')
                                    #'delta' + str(0) + '.xs')
            except:
                print('No existe el archivo')
                continue
            
            print('Fitting', gt_cd, gt_icvfLvl, gt_iniPos)
            results['gt_cPar'].append(gt_cd)
            results['gt_icvfLvl'].append(gt_icvfLvl)
            results['gt_iniPos'].append(gt_iniPos)
            results['signal'].append(mriData)
            
            #calcular coeficientes ortogonales ground truth
            #MD = np.mean( (xs*1e-6), axis=0)
            MSD = np.mean( (xs*1e-6)**2, axis=0)
            estimated_cdiffus = MSD/(2*(bDelta_desired))
            gt_cOrt = estimated_cdiffus[:2].mean()
            results['gt_cOrt'].append(gt_cOrt)
            
            #definir vecindad de búsqueda
            ortDiff = getNeighborhood(gt_cOrt, 0.999, 200001)
            results['cOrt_partition'].append(ortDiff)
            
            cOrt, ds = gridSearch(mriData, shell1Schm, ortDiff,
                                  float(gt_cd), bValue_desired, cNorm)
            results['cOrt'].append(cOrt)
            results['distanceSurface'].append(ds)
            
            
results =  pd.DataFrame(results)

#%% añadir total
for gt_cd in ['1.9e-9', '2e-9', '2.1e-9']:
    for gt_icvfLvl, gt_icvf in icvfs.items():
        for gt_iniPos in ['bundle', 'bundleStick']:
            subFrame = results.query('gt_cPar==\'{}\' & gt_icvfLvl==\'{}\''.format(gt_cd, gt_icvfLvl))
            if(subFrame.shape[0] == 0): continue
        
            print('Fitting', gt_cd, gt_icvfLvl, gt_iniPos)
            sf_intra = subFrame[subFrame.gt_iniPos == 'intra']
            sf_extra = subFrame[subFrame.gt_iniPos == 'extra']
            sf_total = subFrame[subFrame.gt_iniPos == 'intra'].copy()
            
            #ortDiff = np.arange(gt_cOrt*0.001, gt_cOrt*1.999, (gt_cOrt*1.998)/200000)
            sf_total['signal'].iloc[0] = gt_icvf*sf_intra.signal.iloc[0] + (1-gt_icvf)*sf_extra.signal.iloc[0]
            sf_total['gt_iniPos'].iloc[0] = gt_iniPos
            
            if gt_iniPos == 'bundle':
                gt_cOrt = gt_icvf*sf_intra.gt_cOrt.iloc[0] + (1-gt_icvf)*sf_extra.gt_cOrt.iloc[0]
                ortDiff = getNeighborhood(gt_cOrt, 0.999, 200001)
                
                cOrt, ds = gridSearch(sf_total.signal.iloc[0], shell1Schm, ortDiff,
                                      float(gt_cd), bValue_desired, cNorm)
            else:
                #probar cOrt extra con modelo stick
                gt_cOrt = sf_extra.gt_cOrt.iloc[0]
                ortDiff = getNeighborhood(gt_cOrt, 0.999, 200001)
                 
                cOrt, ds = gridSearch(sf_total.signal.iloc[0], shell1Schm, ortDiff,
                                      float(gt_cd), bValue_desired, cNorm,
                                      gt_icvf)
                
            #combinaciones convexas
            sf_total['gt_cOrt'].iloc[0] = gt_cOrt
            sf_total['cOrt_partition'].iloc[0] = ortDiff
            sf_total['cOrt'].iloc[0] = cOrt 
            sf_total['distanceSurface'].iloc[0] = ds
            
            results = results.append(sf_total)

results['relError'] = 100*(results.cOrt-results.gt_cOrt)/results.gt_cOrt

#%% graficar


#plotear señal por su ortogonalidad a z
dirCols= ['g_x','g_y','g_z']

dotProdG = np.abs(shell1Schm[dirCols]@ np.array([0, 0, 1])).to_numpy()
plotIdx = np.argsort(dotProdG)[1:] #np.flip( np.argsort(dotProdG), axis=0)
#S_0_Idx = plotIdx.argmin()


for idx, row in results.iterrows():
    
    fig = plt.figure()
    #fig.suptitle(', '.join([row.gt_cPar, row.gt_icvfLvl, row.gt_iniPos]))
    fig.suptitle( '$f=$ {}, Position: {}'.format(row.gt_icvfLvl, row.gt_iniPos) )
    
#    G = shell1Schm[dirCols].mul(row.signal, axis=0)
#    G = G.to_numpy()[plotIdx]#[1:]
#    
#    X, Y, Z = map(np.array, zip(*G))
#    U, V, W = map(np.array, zip(*-G))
    
    
#    ax1 = fig.add_subplot(121, projection='3d')
#    ax1.set_aspect('equal')
#    
#    ax1.quiver(X, Y, Z, U, V, W)
#    ax1.set_xticks([])
#    ax1.set_yticks([])
#    ax1.set_zticks([])
#    ax1.set_xlabel('x')
#    ax1.set_ylabel('y')
#    ax1.set_zlabel('z')
    
#    max_range = np.array([X.max()-X.min(), Y.max()-Y.min(), Z.max()-Z.min()]).max() / 2.0
#    mid_x = (X.max()+X.min()) * 0.5
#    mid_y = (Y.max()+Y.min()) * 0.5
#    mid_z = (Z.max()+Z.min()) * 0.5
#    ax1.set_xlim(mid_x - max_range, mid_x + max_range)
#    ax1.set_ylim(mid_y - max_range, mid_y + max_range)
#    ax1.set_zlim(mid_z - max_range, mid_z + max_range)
    
    ax1 = fig.add_subplot(211)
    #ax1.plot(row.signal[plotIdx]/row.signal[0], '.')
    ax1.scatter(x=dotProdG[plotIdx], y=row.signal[plotIdx]/row.signal[0], s=1)
    #ax1.scatter(x=S_0_Idx, y=row.signal[plotIdx[S_0_Idx]]/row.signal[0], c='r')
    #ax1.plot(x=dotProdG[plotIdx[S_0_Idx]], y=row.signal[plotIdx[S_0_Idx]]/row.signal[0], c='r')
    ax1.set_title('Signal from Montecarlo Simulator')
    ax1.set_xlabel('$|g^T e_z|$')# ($S_0$ in red)')
    ax1.set_ylabel('$S_{sim}/S_0$')
    #ax1.set_xticks([])
    
    ax1.set_ylim(0,1)
        
    
    
    annSS = '$\hat{D}_{ort} = %.3g$ \n $ \epsilon = %.2f$%%' % (row.cOrt, 100*(row.cOrt-row.gt_cOrt)/row.gt_cOrt)
    ax2 = fig.add_subplot(212)
    ax2.set_title('Search')
    ax2.plot(row.cOrt_partition, row.distanceSurface)
    ax2.axvline(row.cOrt_partition[len(row.cOrt_partition)//2], c='r')
    ax2.annotate(annSS, 
                 xy = (row.cOrt, row.distanceSurface.min()),
                 arrowprops = dict(facecolor='black', shrink=0.05)
                 )
    #ax2.set_yticks([])
    ax2.set_xlabel('$\hat{D}_{ort}$ tried')
    ax2.set_ylabel('$||S_{sim} - S_{model}||_' + str(cNorm) + '$')
    
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    
    plt.savefig('../results/1d_mriFit/' + '_'.join([row.gt_cPar, row.gt_icvfLvl, row.gt_iniPos]) +'.eps', 
                format='eps', dpi=1000, bbox_inches='tight', pad_inches=0)
    
    plt.plot()
    
    
#%% generar tablas
errRes = results.pivot(index='gt_icvfLvl', columns='gt_iniPos', values='relError')
errRes.to_latex('../results/1d_mriFit/exp2_mriFit.tex', float_format='%.2f%%')
            
#%% guardar señales en txt
#cquery_gt_cPar = '2.1e-9'
#cquery_gt_icvfLvl = 'mid'
#cquery_gt_iniPos = 'total'
#
#cquery = 'gt_cPar==\'{}\' & gt_icvfLvl==\'{}\' & gt_iniPos==\'{}\''.format(
#        cquery_gt_cPar,
#        cquery_gt_icvfLvl,
#        cquery_gt_iniPos
#                     )
#osig = results.query(cquery)['signal'].iloc[0]
##np.savetxt(cquery_gt_icvfLvl + 'ICVF_cd' + 
##           cquery_gt_cPar + '_gammac_' + 
##           ('totalsum' if cquery_gt_iniPos == 'total' else cquery_gt_iniPos) + 
##           '.DWI.txt', 
##           osig, 
##           fmt='%.6E')

#%% ploter direcciones

fig = plt.figure()

G = shell1Schm[dirCols]
G = G.to_numpy()

X, Y, Z = map(np.array, zip(*(0*G)))
U, V, W = map(np.array, zip(*G))

ax1 = fig.add_subplot(111, projection='3d')
ax1.set_aspect('equal')

#ax1.quiver(X, Y, Z, U, V, W, arrow_length_ratio=0.2)
ax1.scatter(U, V, W, c='r', marker="x", depthshade=True)

ax1.set_xticklabels([])
ax1.set_yticklabels([])
ax1.set_zticklabels([])
ax1.set_xlabel('x')
ax1.set_ylabel('y')
ax1.set_zlabel('z')


X, Y, Z = map(np.array, zip(*G))
max_range = np.array([X.max()-X.min(), Y.max()-Y.min(), Z.max()-Z.min()]).max() / 2.0
mid_x = (X.max()+X.min()) * 0.5
mid_y = (Y.max()+Y.min()) * 0.5
mid_z = (Z.max()+Z.min()) * 0.5
ax1.set_xlim(mid_x - max_range, mid_x + max_range)
ax1.set_ylim(mid_y - max_range, mid_y + max_range)
ax1.set_zlim(mid_z - max_range, mid_z + max_range)

plt.savefig('../results/' + 'schemeDirs.eps', 
            format='eps', dpi=1000, bbox_inches='tight', pad_inches=0)

plt.show()