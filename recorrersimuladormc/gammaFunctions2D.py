##includes
import numpy as np
import matplotlib.pyplot as plt
import random as rn
import collections
import math

def cylinderCollision(cyl,cyl2):
    dif = [a-b for a,b in zip(cyl[0:3],cyl2[0:3])];
    dist = np.linalg.norm(dif) - float(cyl[3]) - float(cyl2[3]);
    if(dist <= 1e-10):
        return True;

    return False;


def checkBoundaryCollition(cyl2,voxel_limits):
    cyl_ = [];
    cyl_.append(cyl2);

    for i in range(2):
        temp = list(cyl2);
        rad = cyl2[3];
        #x,y,z + rad
        temp[i]+=rad
        if (temp[i]  >= voxel_limits[i][1]):
            temp = list(cyl2);
            temp[i] = temp[i]-voxel_limits[i][1]+voxel_limits[i][0];
            cyl_.append(temp)
        #x - rad
        temp = list(cyl2);
        temp[i]-=rad
        if (temp[i]  <= voxel_limits[i][0]):
            temp = list(cyl2);
            temp[i] = temp[i]-voxel_limits[i][0]+voxel_limits[i][1];
            cyl_.append(temp)

    return cyl_
def checkForNOCylinderColition(cylinders,cyl2,voxel_limits):

    collition = collections.namedtuple('no_collition_flag','cylinders');
    collition.no_collition_flag = False;
    cyl22 = checkBoundaryCollition(cyl2,voxel_limits)

    for cyl2_ in cyl22:
        dist_ = [(np.linalg.norm([cyl2_[0]-cyl[0],
                                  cyl2_[1]-cyl[1],
                                  cyl2_[2]-cyl[2]
                                  ]) - cyl2_[3]-cyl[3]) <= 0 for cyl in cylinders];
        if(True in dist_):
            collition.cylinders = [];
            collition.no_collition_flag = False;
            return collition;

    collition.no_collition_flag = True;
    collition.cylinders = list(cyl22);
    return collition

def computeICVF(cylinders,voxel_limits):

    if len(cylinders) == 0:
        return 0

    AreaV = (voxel_limits[0][1] - voxel_limits[0][0]) 
    AreaV *= (voxel_limits[1][1] - voxel_limits[1][0]);
    
    radii = [cyl[3] for cyl in cylinders];
    AreaC = np.sum([math.pi*r*r for r in radii ]);
    return AreaC/AreaV;


def gammaDistributedCylinders(radius_list,
                              max_num_cylinders, 
                              voxel_size, 
                              max_icfv = 1,
                              plot = False,
                              output_folder = None):
    #Gamma shape and scale parameters
    # GAMA=4.8184
    # GAMB=1.3008E-7

    radiis =  radius_list;

    substrate = collections.namedtuple('icvf','cylinders');

    voxel_x_limits = [0,voxel_size]
    voxel_y_limits = [0,voxel_size]
    voxel_z_limits = [-voxel_size,voxel_size]
    voxel_limits = [voxel_x_limits, voxel_y_limits,voxel_z_limits]

    ## If plot_gamma_dist
    if plot:
        count, bins, ignored = plt.hist(radiis, 50, normed=False)

        plt.show(False)

    ## Until all radiis are used or icvf achieved we sample
    cylinders  = [];
    cylinders_no_rep = [];
    perc_display = 0.09
    rn.seed();
    for rad in radiis:
        stuck = 0 ;
        while(stuck <= 50):
            t = rn.random();
            #x = (t*voxel_limits[0][1] + (1-t)+voxel_limits[0][0]);
            x = (voxel_limits[0][1]-voxel_limits[0][0])*t + voxel_limits[0][0];
            t = rn.random();
            #y = (t*voxel_limits[1][1] + (1-t)+voxel_limits[1][0]);
            y = (voxel_limits[1][1]-voxel_limits[1][0])*t + voxel_limits[1][0];
            z = 0;

            cyl = [x,y,z,rad];
            collition = checkForNOCylinderColition(cylinders,cyl,voxel_limits)

            if( collition.no_collition_flag == True):
                cylinders_no_rep.append(collition.cylinders[0]);
                for cyl_ in collition.cylinders:
                    cylinders.append(cyl_)
                break;
            else:
                stuck+=1;

        perc_ = float(len(cylinders_no_rep))/len(radiis);

        icvf = computeICVF(cylinders_no_rep,voxel_limits);
        if( perc_ > perc_display ):
            perc_display+=0.05;
            print("Completed so far: " + 
                  str(perc_*100) + 
                  "%,\nICVF achieved: " + 
                  str(icvf) + "  (" +
                  str( int(100.0*icvf/max_icfv)) + 
                  "% of the desired icvf)\n");
        #print(icvf)
        if(icvf > max_icfv):
            break;

    ## Plot 2d "cylinders"
    if(plot):
        fig, ax = plt.subplots()
        X = [x[0] for x in cylinders ]
        Y = [x[1] for x in cylinders ]
        # ax.set_xlim((min(voxel_x_limits[0],min(X)),max(voxel_x_limits[1],max(X)) ))
        # ax.set_ylim((min(voxel_y_limits[0],min(Y)),max(voxel_y_limits[1],max(Y)) ))

        ax.set_xlim(voxel_x_limits[0],voxel_x_limits[1])
        ax.set_ylim(voxel_y_limits[0],voxel_y_limits[1])

        for cyl in cylinders:
            circle = plt.Circle((cyl[0], cyl[1]), cyl[3], color='k')
            ax.add_artist(circle)

        fig.show();

        fig2 = plt.figure()
        radiis = [x[3] for x in cylinders_no_rep];
        count, bins, ignored = plt.hist(radiis, 50, normed=False)
        #y = bins**(shape-1)*(np.exp(-bins/scale) /
                             ##(sps.gamma(shape)*scale**shape))
        #plt.plot(bins, y, linewidth=2, color='r')

        plt.show(False)

    else:
        pass
        #fig.clf()

    substrate.icvf =  computeICVF(cylinders_no_rep,voxel_limits);
    substrate.cylinders = cylinders;
    
    outPath = "{}Cilindros_gamma_{}-cylinders_{}.txt"
    
    if not (output_folder is None):
        np.savetxt(outPath.format(output_folder,
                                  len(cylinders), 
                                  substrate.icvf ), 
                cylinders)

    return substrate
