#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 27 17:54:02 2019

@author: alfonso
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import seaborn as sns
sns.set()

#%%

icvfs = {'lvl2' : 0.20284519076726346,
         'lvl3' : 0.30812232342739665,
         'lvl4' : 0.4014456195874796,
         'lvl5' : 0.5016271405754553,
         'lvl6' : 0.6076080511924874,
         'lvl7' : 0.7064550523980454
         }

for cicvfLvl in icvfs:
    cCircPD = pd.read_csv('crossings_camino/' + cicvfLvl +'ICVF_cd2.1e-9_gammac_extra.centers.txt', 
                          skiprows=1,
                          names=['r', 'x', 'y', 'z'])
    
    fig, ax = plt.subplots()
    ax.set_aspect('equal', 'box')
    ax.set_xlim([cCircPD['x'].min(), cCircPD['x'].max()])
    ax.set_ylim([cCircPD['y'].min(), cCircPD['y'].max()])
    
    for idx, row in cCircPD.iterrows():
        cCirc = plt.Circle( (row['x'] , row['y']), row['r'], color='black')
        #cCirc = plt.Circle((0.5, 0.5), 0.5)
        ax.add_artist(cCirc)
        
    ax.set_title(cicvfLvl)
    
    plt.savefig('../results/substrate/' + cicvfLvl +'.eps', 
                format='eps', dpi=1000, bbox_inches='tight', pad_inches=0)
    plt.show()
    