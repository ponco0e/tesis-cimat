#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  4 21:02:21 2018

@author: ponco
"""

import sys, os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from gammaFunctions2D import *

#constantes de la distribución gamma
shape, scale = 6.27, 4.90E-1/8.0

#función para escribir archivo de configuración para el simulador montecarlo
def writeConfFile(outPath, #dirección donde escribir
                  N, #número de moléculas
                  T, #pasos de simulación (el simulador hace T+1 pasos)
                  duration, #duración de la dinámica (vienen del esquema)
                  writeTxt = False, #escribir a texto
                  writeBin = False, #escr4ibir binario
                  scale = 1, #scale_from_stu (?)
                  threads = 1, #número de procesos paralelos
                  xLims = (0, 1), #dimensiones del voxel
                  yLims = (0, 1),
                  zLims = (0, 1),
                  schema = "2Shells_human_1_5k_2_5k_scheme_64o.txt", #proto. MRI
                  loc = "default" #"intra" o "extra"
                  ):
    with open(outPath + "/sim.conf", "w") as ofile:
        lineBuff = []
        
        lineBuff.append("N " + str(N))
        
        lineBuff.append("T " + str(T))
        
        lineBuff.append("duration " + str(duration))
        
        lineBuff.append("diffusivity 2.1E-9")
        
        lineBuff.append("out_traj_file_index outputs/bundle_z_{}_{}N_{}T".format(loc, N, T))
        
        lineBuff.append("scheme_file " + schema)
        
        lineBuff.append("write_txt " + ("1" if writeTxt else "0"))
        
        lineBuff.append("write_bin " + ("1" if writeBin else "0"))
        
        lineBuff.append("scale_from_stu " + str(scale))
        
        lineBuff.append("<obstacle>")
        lineBuff.append("cylinders_list centers.txt")
        lineBuff.append("</obstacle>")
        
        lineBuff.append("<voxels>")
        lineBuff.append( "{} {} {}".format(xLims[0], yLims[0], zLims[0]) )
        lineBuff.append( "{} {} {}".format(xLims[1], yLims[1], zLims[1]) )
        lineBuff.append("</voxels>")
        
        lineBuff.append("num_process " + str(threads))
        
        lineBuff.append("ini_walkers_file iniWalkers.txt")
        
        lineBuff.append("<END>")
        
        ofile.write( "\n".join(lineBuff) )
    



# leer argumentos
icvf = 0.0
maxCyl = 0
voxSize = 0.0
nWalkers = 0
outFolderPrefix = ""
simT = 100
simDur = 0.047

errorParams = True
if len(sys.argv) == 8:
    try:
        icvf = float(sys.argv[1])
        maxCyl = int(sys.argv[2])
        voxSize = float(sys.argv[3]) * 1E-3 #convertir a milímetros
        nWalkers = int(sys.argv[4])
        outFolderPrefix = sys.argv[5]
        simDur = float(sys.argv[6])
        simT = int(sys.argv[7])
        
        errorParams = False
    except ValueError as e:
        pass
       
if errorParams:
    print("Usage:" + sys.argv[0] + " ICVF max_no_of_cyllinders voxel_size[microns] nMolecules output_folder_prefix simT[sec] simSteps")
    sys.exit(-1)
    

print("Solving for {} max cyllinders, target icvf of {} and voxSize of {} mm".format(maxCyl, icvf, voxSize))

#generar y ordenar radios con distribución gamma
radiis = np.random.gamma(shape, scale, maxCyl);
radiis = (np.sort(radiis))[::-1]

#generar la lista de los cilindros
substrate = gammaDistributedCylinders(radius_list = radiis,
                                      max_num_cylinders = maxCyl, 
                                      voxel_size = voxSize * 1E3, #tiene que ser en micrómetros 
                                      max_icfv = icvf,
                                      plot = False) #,output_folder='')
#pasarlos a dataframe para manipularlos más facil y reescalar a milímetros
cyls = pd.DataFrame(substrate.cylinders, columns=["x", "y", "z", "r"]) * 1E-3


#varianzas (asumiendo voxel cuadrado)
svx = voxSize
svy = voxSize

#generar posiciones aleatorias de los caminantes
walkers = pd.DataFrame(np.random.uniform(size=(nWalkers, 2)), columns=["x", "y"])
walkers["x"] *= svx
walkers["y"] *= svy
walkers["z"] = 0

#clasificar caminantes por posición interna o externa
isInsideAxon = np.array( [ False for _ in range(walkers.shape[0]) ] )
for _, cyl in cyls.iterrows():
    isInsideAxon |= ( (walkers["x"]-cyl["x"])**2 + (walkers["y"]-cyl["y"])**2 ) < cyl["r"]**2

#separar intra y extra
for pos, dat in {"intra" : walkers[isInsideAxon], 
                 "extra" : walkers[isInsideAxon==False]
                 }.items():
    #crear carpetas
    if not os.path.exists(outFolderPrefix + "_"+pos+"/"):
        os.makedirs(outFolderPrefix + "_"+pos+"/outputs/")
    #crear archivos de cilindros
    with open(outFolderPrefix+"_"+pos+"/centers.txt", "w") as ofile:
        ofile.write("1\n")
        cyls.to_csv(ofile, sep=" ", header=False, index=False)
    writeConfFile(outFolderPrefix + "_"+pos+"/",
                  N=dat.shape[0],
                  T=simT,
                  duration=simDur,
                  xLims=(0, voxSize),
                  yLims=(0, voxSize),
                  zLims=(-10,10),
                  loc=pos,
                  writeBin = True
                  )
    #crear archivos de moléculas
    #datScaled = dat * 1E-3 #escribir en mm
    dat.to_csv(outFolderPrefix+"_"+pos+"/iniWalkers.txt", 
                    sep=" ", header=False, index=False)
    #escribir el icvf conseguido por si se ocupa
    with open(outFolderPrefix+"_"+pos+"/icvf.txt", "w") as ofile:
        ofile.write(str(substrate.icvf))



#graficar
plt.figure()
cColorMap = ["red" if x else "blue" for x in isInsideAxon]
walkers.plot.scatter(x="x", y="y", s=0.01, c=cColorMap)
plt.gca().set_aspect('equal', adjustable='box')
plt.savefig("walkers.png", dpi=360)

    
print("Finished with ICFV: " + 
      str(substrate.icvf) + " and " + 
      str(len(substrate.cylinders)) + 
      " cylinders")
